import React from "react";
import {withRouter} from "react-router-dom";
import NetworkController from "controllers/Network";
import AsyncStateComponent from "contexts/common/AsyncStateComponent";
import routes, {getRoute} from "consts/routes";
import {withContextConsumer} from "utils/contexts";
import NotificationContext from "contexts/Notifications";
import CurrentUser from "./CurrentUser";

const ArticlesContext = React.createContext("articles");

const validateArticleData = data => {
    const isExistsTitle = data.title && data.title.trim() !== "";
    const isExistsDescription = data.description && data.description.trim() !== "";
    const isExistsContent = data.content && data.content.trim() !== "";

    const errors = {};
    if (!isExistsTitle) errors.title = "Введите название";
    if (!isExistsDescription) errors.description = "Введите краткое описание";
    if (!isExistsContent) errors.content = "Напишите статью";

    return Object.keys(errors).length > 0 && errors;
};

@withContextConsumer(CurrentUser.Consumer)
@withContextConsumer(NotificationContext.Consumer)
class ArticlesProvider extends AsyncStateComponent {
    constructor(props) {
        super(props);
        this.state = {
            currentArticle: null,
            isLoading: false,
            articleErrors: {},
            articles: [],
            total: 0,
            articlesMeta: null,
            clearArticles: () => this.setState({articles: [], total: 0}),
            clearArticleErrors: () => this.setState({articleErrors: {}}),

            createArticle: this.createArticle.bind(this),
            updateArticle: this.updateArticle.bind(this),
            deleteArticle: this.deleteArticle.bind(this),

            getArticleById: this.getArticleById.bind(this),
            getArticles: this.getArticles.bind(this),

            onChangeArticleRate: this.onChangeArticleRate.bind(this),

            comments: [],
            totalComments: 0,
            createComment: this.createComment.bind(this),
            createCommentErrors: {},

            getCommentsByArticleId: this.getCommentsByArticleId.bind(this),
            loadCommentsErrors: {},

            deleteComment: this.deleteComment.bind(this),
            deleteCommentErrors: {},

            editComment: this.editComment.bind(this)
        };
    }

    createArticle = async data => {
        const {history} = this.props;
        this.setState({isLoading: true});
        const errors = validateArticleData(data);
        if (errors) {
            return this.setState({articleErrors: errors, isLoading: false});
        }
        const {error, response} = await NetworkController.post("/articles", data);
        if (error) {
            return this.setState({articleErrors: response, isLoading: false});
        }

        this.setState({isLoading: false}, history.push(getRoute(routes.article, {id: response.id})));
    };

    updateArticle = async (id, data) => {
        const {history} = this.props;
        this.setState({isLoading: true});
        const errors = validateArticleData(data);
        if (errors) {
            return this.setState({articleErrors: errors, isLoading: false});
        }
        const {error, response} = await NetworkController.put(`/articles/${id}`, data);
        if (error) {
            return this.setState({articleErrors: response, isLoading: false});
        }

        this.setState({isLoading: false}, history.push(getRoute(routes.article, {id: response.id})));
    };

    deleteArticle = async id => {
        const {history} = this.props;
        this.setState({isLoading: true});
        const {error, response} = await NetworkController.delete(`/articles/${id}`);
        if (error) {
            return this.setState({articleErrors: response, isLoading: false});
        }
        this.setState({isLoading: false}, history.push("/"));
    };

    getArticleById = async id => {
        this.setState({isLoading: true});
        const {error, response} = await NetworkController.get(`/articles/${id}`);
        if (error) {
            this.props.createNotification({
                content: response.error,
                type: "error"
            });
            return this.setState({isLoading: false}, () => {
                this.props.history.push(routes.home.path);
            });
        }

        this.setState({isLoading: false, currentArticle: response});
    };

    getArticles = async (search, limit, offset, filters, sort) => {
        this.setState({isLoading: true});
        const {error, response} = await NetworkController.get(`/articles`, {
            filters: JSON.stringify(filters),
            sort: JSON.stringify(sort),
            search,
            limit,
            offset
        });

        if (error) {
            return this.setState({articleErrors: response, isLoading: false});
        }
        const {list, total, meta} = response;
        this.setState({isLoading: false, articles: list, articlesMeta: meta, total});
    };

    onChangeArticleRate = async isPositive => {
        const {currentArticle} = this.state;
        const {error, response} = await NetworkController.post(`/articles/${currentArticle.id}/rating`, {isPositive});
        if (!error) {
            currentArticle.rate = response.rate;
            currentArticle.currentUserRate = response.currentUserRate;
            await this.setState({currentArticle});
        }
    };

    createComment = async text => {
        await this.setStatePromise({createCommentErrors: {}});
        const request = {
            text: text,
            currentArticle: this.state.currentArticle,
            currentUser: this.props.currentUser
        };

        const {error, response} = await NetworkController.post(`/comments`, request);
        if (error) {
            return await this.setStatePromise({createCommentErrors: response});
        }
    };

    deleteComment = async comment => {
        await this.setStatePromise({deleteCommentErrors: {}});

        const {error, response} = await NetworkController.delete(`/comments/${comment.id}`);

        if (error) {
            return await this.setStatePromise({deleteCommentErrors: response});
        }
    };

    editComment = async (comment, text) => {
        await this.setStatePromise({createCommentErrors: {}});
        const request = {text: text};

        const {error, response} = await NetworkController.put(`/comments/${comment.id}`, request);

        if (error) {
            return await this.setStatePromise({createCommentErrors: response});
        }
    };

    getCommentsByArticleId = async (id, offset, limit) => {
        await this.setStatePromise({loadCommentsErrors: {}});
        const {error, response} = await NetworkController.get(`/comments/${id}`, {
            offset,
            limit
        });

        if (error) {
            return this.setStatePromise({loadCommentsErrors: response});
        }

        this.setState({comments: response.comments, totalComments: response.total});
    };

    render() {
        return <ArticlesContext.Provider value={this.state}>{this.props.children}</ArticlesContext.Provider>;
    }
}

export default {
    Provider: withRouter(ArticlesProvider),
    Consumer: ArticlesContext.Consumer
};
