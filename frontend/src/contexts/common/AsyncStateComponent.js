import React, {Component} from "react";

class AsyncStateComponent extends Component {
    setStatePromise(state) {
        return new Promise(resolve => this.setState(state, resolve));
    }
}

export default AsyncStateComponent;
