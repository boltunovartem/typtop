import React from "react";
import {withRouter} from "react-router-dom";
import AsyncStateComponent from "./common/AsyncStateComponent";
import NetworkController from "controllers/Network";

const TagsContext = React.createContext("tags");

class TagsProvider extends AsyncStateComponent {
    constructor(props) {
        super(props);
        this.state = {
            tags: [],
            getTags: this.getTags.bind(this)
        };
    }

    getTags = async (selectedTags = [], search = "") => {
        const {response, error} = await NetworkController.get("/tags", {search});
        if (!error) {
            const {tags} = response;
            const newTags = await Promise.all(tags.map(({name}) => ({name, value: name})));
            this.setState({tags: newTags});
        }

        return null;
    };

    render() {
        return <TagsContext.Provider value={this.state}>{this.props.children}</TagsContext.Provider>;
    }
}

export default {Provider: withRouter(TagsProvider), Consumer: TagsContext.Consumer};
