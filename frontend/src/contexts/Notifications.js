import React from "react";
import {withRouter} from "react-router-dom";
import AsyncStateComponent from "./common/AsyncStateComponent";
import {v4 as uuidv4} from "uuid";

const NotificationsContext = React.createContext("notifications");

class NotificationsProvider extends AsyncStateComponent {
    constructor(props) {
        super(props);

        this.state = {
            notifications: [],
            timers: [],
            createNotification: this.createNotification.bind(this),
            closeNotification: this.closeNotification.bind(this)
        };
    }

    componentWillUnmount() {
        const {timers} = this.state;
        timers.forEach(u => clearTimeout(u));
    }

    createNotification(notification) {
        const {notifications, timers} = this.state;
        const {type, content, timeOut} = notification;
        const id = uuidv4();

        this.setState({
            notifications: [...notifications, {id, content, type}],
            timers: [...timers, setTimeout(() => this.closeNotification(id), timeOut || 3000)]
        });
    }

    closeNotification(removedId) {
        const {notifications} = this.state;
        this.setState({notifications: notifications.filter(({id}) => id !== removedId)});
    }

    render() {
        return <NotificationsContext.Provider value={this.state}>{this.props.children}</NotificationsContext.Provider>;
    }
}

export default {Provider: withRouter(NotificationsProvider), Consumer: NotificationsContext.Consumer};
