import React from "react";
import {withRouter} from "react-router-dom";
import NetworkController from "controllers/Network";
import AsyncStateComponent from "contexts/common/AsyncStateComponent";
import routes from "consts/routes";
import {withContextConsumer} from "utils/contexts";
import NotificationsContext from "contexts/Notifications";

const CurrentUserContext = React.createContext("currentUser");

@withContextConsumer(NotificationsContext.Consumer)
class CurrentUserProvider extends AsyncStateComponent {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: null,
            isLoadingCurrentUser: true,

            registerErrors: {},
            clearRegisterErrors: () => this.setState({registerErrors: {}}),
            register: this.register.bind(this),

            loginErrors: {},
            clearLoginErrors: () => this.setState({loginErrors: {}}),
            login: this.login.bind(this),

            updateProfileErrors: {},
            clearUpdateProfileErrors: () => this.setState({updateProfileErrors: {}}),
            updateProfile: this.updateProfile.bind(this),

            deleteProfile: this.deleteProfile.bind(this),

            getUserById: this.getUserById.bind(this),
            logout: this.logout.bind(this),

            subscribeToUserErrors: {},
            clearSubscribeToUserErrors: () => this.setState({subscribeToUserErrors: {}}),
            subscribeToUser: this.subscribeToUser.bind(this),

            getSubscriptions: this.getSubscriptions.bind(this),
            followers: [],
            following: []
        };
    }

    async componentDidMount() {
        const {error, response} = await NetworkController.get("/users", {});
        if (error) {
            return this.setState({isLoadingCurrentUser: false});
        }
        this.setState({
            currentUser: response.user,
            isLoadingCurrentUser: false
        });
    }

    async logout() {
        this.setState({currentUser: null});
        await NetworkController.post("/users/logout");
        this.props.history.push("/");
    }

    async login(data) {
        const loginErrors = {};
        if (data.userName.trim() === "") {
            loginErrors.userName = "Введите логин";
        }
        if (data.password.trim() === "") {
            loginErrors.password = "Введите пароль";
        }
        if (Object.keys(loginErrors).length > 0) {
            return this.setState({loginErrors});
        }

        const {error, response} = await NetworkController.post("/users/login", data);
        if (error) {
            return this.setState({loginErrors: response});
        }
        const {user: currentUser} = response;
        this.setState({loginErrors: {}});
        await this.setStatePromise({currentUser});
        this.props.history.push(routes.home.path);
    }

    async register(data) {
        await this.setStatePromise({registerErrors: {}});
        if (data.password !== "" && data.confirmPassword !== data.password) {
            return this.setState({
                registerErrors: {
                    confirmPassword: "Пароли должны совпадать",
                    password: "Пароли должны совпадать"
                }
            });
        }
        const request = {...data};
        delete request.confirmPassword;
        const {error, response} = await NetworkController.post("/users/register", request);
        if (error) {
            return this.setState({registerErrors: response});
        }
        await this.setStatePromise({currentUser: response.user});
        this.props.history.push(routes.home.path);
    }

    async updateProfile(data) {
        await this.setStatePromise({updateProfileErrors: {}});
        if (data.password !== "" && data.confirmPassword !== data.password) {
            return this.setState({
                updateProfileErrors: {
                    confirmPassword: "Пароли должны совпадать",
                    password: "Пароли должны совпадать"
                }
            });
        }
        const request = {...data};
        delete request.confirmPassword;
        const {error, response} = await NetworkController.put(`/users`, request);
        if (error) {
            return this.setState({updateProfileErrors: response});
        }
        await this.setStatePromise({currentUser: response.user});
    }

    async deleteProfile() {
        this.setState({currentUser: null});
        await NetworkController.delete(`/users`);
        this.props.history.push("/");
    }

    async getUserById(id) {
        this.setState({isLoading: true});
        const {error, response} = await NetworkController.get(`/users/${id}`);
        if (error) {
            this.props.createNotification({
                content: response.error || response.profileId,
                type: "error"
            });
            return this.setState({isLoading: false}, () => {
                this.props.history.push(routes.home.path);
            });
        }
        return response.user;
    }

    async subscribeToUser(userId) {
        console.log(this.state.currentUser.id, userId);
        const {error, response} = await NetworkController.post(`/users/subscribe`, {userId: userId});
        if (error) {
            return this.setState({subscribeToUserErrors: response});
        }
        return response;
    }

    async getSubscriptions(profileId) {
        const {currentUser} = this.state;
        const id = profileId || currentUser.id;
        const {error, response} = await NetworkController.get(`/users/${id}/subscriptions`);
        if (error) {
            this.props.createNotification({
                content: response.error,
                type: "error"
            });
            return;
        }
        const {followers, following} = response;
        this.setState({followers, following});
    }

    render() {
        const {isLoadingCurrentUser} = this.state;
        return (
            <CurrentUserContext.Provider value={this.state}>
                {!isLoadingCurrentUser && this.props.children}
            </CurrentUserContext.Provider>
        );
    }
}

export default {
    Provider: withRouter(CurrentUserProvider),
    Consumer: CurrentUserContext.Consumer
};
