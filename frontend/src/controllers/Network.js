const NETWORK_ERROR = "NETWORK_ERROR";

class Network {
    constructor() {
        this.apiRoot = this.getApiRoot();
    }

    async get(apiMethod, qs) {
        return await this.sendSaveRequest(apiMethod, "json", "GET", {qs});
    }

    async post(apiMethod, body) {
        return await this.sendSaveRequest(apiMethod, "json", "POST", {body});
    }

    async put(apiMethod, body) {
        return await this.sendSaveRequest(apiMethod, "json", "PUT", {body});
    }

    async delete(apiMethod, body) {
        return await this.sendSaveRequest(apiMethod, "json", "DELETE", {body});
    }

    async formData(apiMethod, form, files) {
        return await this.sendSaveRequest(apiMethod, "formData", "POST", {form, files});
    }

    async sendSaveRequest(apiMethod, type, method, {body, qs, form, files}) {
        try {
            let url = new URL(`${this.apiRoot}${apiMethod}`);
            if (qs) {
                Object.keys(qs).forEach(key => url.searchParams.append(key, qs[key]));
            }
            body = this.transformBody(body);
            const response =
                type === "json"
                    ? await this.sendJsonRequest(url, method, body)
                    : await this.sendFormRequest(url, method, form, files);

            return {
                error: response.status !== 200,
                response: response.json ? await response.json() : response.data
            };
        } catch (e) {
            console.log(NETWORK_ERROR);
            return e;
        }
    }

    async sendJsonRequest(url, method, body) {
        return await fetch(url, {
            method,
            body: body && JSON.stringify(body),
            credentials: "include",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            }
        });
    }

    async sendFormRequest(url, method, form, files) {
        const data = new FormData();
        data.append("data", JSON.stringify(form));
        Object.keys(files).forEach(f => data.append(f, files[f]));
        return await fetch(url, {
            method,
            body: data,
            credentials: "include"
        });
    }

    transformBody(body) {
        if (!body) {
            return body;
        }
        Object.keys(body).forEach(k => {
            if (body[k] === undefined) body[k] = null;
        });
        return body;
    }

    getApiRoot() {
        return process.env.NODE_ENV === "development"
            ? `${location.protocol}//${location.hostname}:8889/api/v1`
            : `${location.origin}/api/v1`;
    }
}

export default new Network();
