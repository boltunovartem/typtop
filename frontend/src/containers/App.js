import React from "react";
import Router from "./Router";
import moment from "moment";
import "moment/locale/ru";
moment.locale("ru");

function App() {
    return (
        <div>
            <Router />
        </div>
    );
}

export default App;
