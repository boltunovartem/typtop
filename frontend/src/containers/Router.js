import React, {Component} from "react";
import {BrowserRouter, Switch, Route, Redirect} from "react-router-dom";
import CurrentUserContext from "contexts/CurrentUser";
import ArticlesContext from "contexts/Articles";
import AppContext from "contexts/App";
import routes from "consts/routes";
import PageContainer from "./contents/PageContainer";
import Notifications from "contexts/Notifications";

class Router extends Component {
    render() {
        return (
            <BrowserRouter>
                <AppContext.Provider>
                    <Notifications.Provider>
                        <CurrentUserContext.Provider>
                            <ArticlesContext.Provider>
                                <Switch>
                                    <Route path={routes.home.path} component={props => <PageContainer {...props} />} />
                                    <Route component={() => <Redirect to={routes.home.path} />} />
                                </Switch>
                            </ArticlesContext.Provider>
                        </CurrentUserContext.Provider>
                    </Notifications.Provider>
                </AppContext.Provider>
            </BrowserRouter>
        );
    }
}

export default Router;
