import React, {Component} from "react";
import PageWrapper from "./PageWrapper";
import {withContextConsumer} from "utils/contexts";
import CurrentUser from "contexts/CurrentUser";
import ArticlesList from "components/articles/articlesList/ArticlesList";
import Button from "components/common/form/Button";
import Add from "components/common/icons/Add";
import routes from "consts/routes";
import {Row, Column} from "components/articles/articlesList/styled";
import {withRouter} from "react-router-dom";

@withRouter
@withContextConsumer(CurrentUser.Consumer)
class Index extends Component {
    render() {
        const {history, currentUser} = this.props;
        return (
            <PageWrapper>
                <Column>
                    <Row>
                        <h2>Typical topics - статьи для всех</h2>
                        <Button
                            borderColor={"#050558"}
                            icon={<Add width={"1rem"} height={"1rem"} />}
                            style={{width: "10rem"}}
                            onClick={() => history.push(routes.createArticle.path)}>
                            Создать статью
                        </Button>
                    </Row>
                    <ArticlesList currentUser={currentUser} />
                </Column>
            </PageWrapper>
        );
    }
}

export default Index;
