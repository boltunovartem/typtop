import styled from "styled-components";

const Content = styled.div`
    position: relative;
    width: 100%;
    display: flex;
    justify-content: center;
    margin-top: 1rem;
    ${({fullHeight}) =>
        fullHeight &&
        `
        height: -webkit-fill-available;
        height: -moz-available;
        height: 100%;
    `}
`;

const UserItem = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 0.5rem;
    border-radius: 5px;
    transition: 0.5s;

    button {
        opacity: 0;
        transition: 0.5s;
    }

    :hover {
        background: rgba(200, 200, 200, 0.3);
        button {
            opacity: 1;
        }
    }
`;

export {Content, UserItem};
