import React, {Component} from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import routes from "consts/routes";
import PageWrapper from "containers/contents/PageWrapper";
import AnotherProfile from "./AnotherProfile";
import MyProfile from "./MyProfile";

class Profile extends Component {
    render() {
        return (
            <PageWrapper>
                <Switch>
                    <Route path={routes.profileById.path} component={props => <AnotherProfile {...props} />} exact />
                    <Route path={routes.profile.path} component={props => <MyProfile {...props} />} exact />
                    <Route component={() => <Redirect to={routes.home.path} />} />
                </Switch>
            </PageWrapper>
        );
    }
}

export default Profile;
