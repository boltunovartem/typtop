import React, {Component} from "react";

import PageWrapper from "containers/contents/PageWrapper";
import CurrentUser from "contexts/CurrentUser";
import {withContextConsumer} from "utils/contexts";
import ProfileForm from "components/profile/ProfileForm";
import ArticlesList from "components/articles/articlesList/ArticlesList";
import Tabs from "components/common/Tabs";
import {Column} from "components/articles/articlesList/styled";
import {Content} from "containers/contents/Profile/styled";

const tabs = [
    {
        id: "profile",
        name: "Профиль",
        renderContent: props => <ProfileForm {...props} />
    },
    {
        id: "articles",
        name: "Статьи",
        renderContent: props => <ArticlesList withoutFavorites {...props} />
    }
];

const fieldsKeys = {
    firstName: "firstName",
    lastName: "lastName",
    middleName: "middleName",
    userName: "userName",
    phoneNumber: "phoneNumber",
    email: "email",
    password: "password",
    confirmPassword: "confirmPassword",
    gender: "gender",
    dateOfBirth: "dateOfBirth",
    workPlace: "workPlace"
};

@withContextConsumer(CurrentUser.Consumer)
class MyProfile extends Component {
    constructor(props) {
        super(props);
        const {currentUser} = props;
        const currentUserCopy = {...currentUser};

        this.state = {
            firstName: currentUser.firstName,
            lastName: currentUser.lastName,
            middleName: currentUser.middleName,
            userName: currentUser.userName,
            phoneNumber: currentUser.phoneNumber,
            email: currentUser.email,
            password: "",
            confirmPassword: "",
            gender: currentUser.gender,
            dateOfBirth: currentUser.dateOfBirth,
            workPlace: currentUser.workPlace,
            isSent: false,
            waitResponse: false,
            currentUserCopy,
            currentTab: tabs[0],
            showModal: false,
            isOpenFollowersModal: false
        };
    }

    componentDidMount() {
        this.props.clearUpdateProfileErrors();
    }

    onChangeField = (field, value) => {
        this.setState({[field]: value});
    };

    submitFrom = async () => {
        await this.setState({isSent: true, waitResponse: true});
        const {updateProfile} = this.props;
        const {isSent, waitResponse, currentUserCopy, showModal, currentTab, ...user} = this.state;

        await updateProfile(user);

        await this.setState({
            waitResponse: false,
            currentUserCopy: {...this.props.currentUser}
        });
    };

    resetForm = () => {
        const {currentUserCopy} = this.state;
        for (const [key, value] of Object.entries(currentUserCopy)) {
            this.onChangeField(key, value);
        }
    };

    changeTab = tab => {
        this.setState({currentTab: tab});
    };

    onClickDeleteProfileButton = () => {
        this.setState({showModal: true});
    };

    modalSubmit = async () => {
        const {deleteProfile} = this.props;
        await deleteProfile();
    };

    modalClose = () => {
        this.setState({showModal: false});
    };

    openFollowersModal = async () => {
        await this.props.getSubscriptions();
        this.setState({isOpenFollowersModal: true});
    };

    closeFollowersModal = () => {
        this.setState({isOpenFollowersModal: false});
    };

    renderContent = () => {
        const {updateProfileErrors, currentUser, getSubscriptions, subscribeToUser, followers, following} = this.props;
        const {currentTab, showModal, isOpenFollowersModal, ...rest} = this.state;
        const props = {
            fieldsValues: rest,
            owner: currentUser,
            onChangeField: this.onChangeField.bind(this),
            submitForm: this.submitFrom.bind(this),
            resetForm: this.resetForm.bind(this),
            deleteProfile: this.onClickDeleteProfileButton.bind(this),
            modalSubmit: this.modalSubmit.bind(this),
            modalClose: this.modalClose.bind(this),
            closeFollowersModal: this.closeFollowersModal.bind(this),
            openFollowersModal: this.openFollowersModal.bind(this),
            errors: updateProfileErrors,
            hasErrors: Object.keys(updateProfileErrors).length !== 0,
            isOpenFollowersModal,
            updateProfileErrors,
            getSubscriptions,
            subscribeToUser,
            tabs,
            currentUser,
            showModal,
            followers,
            following,
            fieldsKeys
        };

        return <Content fullHeight={currentTab.id === tabs[1].id}>{currentTab.renderContent(props)}</Content>;
    };

    render() {
        const {currentTab} = this.state;
        return (
            <PageWrapper>
                <Column>
                    <Tabs currentTab={currentTab} tabs={tabs} changeTab={tab => this.changeTab(tab)} />
                    {this.renderContent()}
                </Column>
            </PageWrapper>
        );
    }
}

export default MyProfile;
