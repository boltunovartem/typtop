import React, {Component} from "react";
import {withContextConsumer} from "utils/contexts";
import CurrentUser from "contexts/CurrentUser";
import ArticlesList from "components/articles/articlesList/ArticlesList";
import Tabs from "components/common/Tabs";
import PageWrapper from "containers/contents/PageWrapper";
import {Column} from "components/articles/articlesList/styled";
import ShowProfile from "components/profile/ShowProfile";
import Loading from "containers/contents/Loading";
import {Content} from "containers/contents/Profile/styled";
import routes from "consts/routes";

const tabs = [
    {
        id: "profile",
        name: "Профиль",
        renderContent: props => <ShowProfile {...props} />
    },
    {
        id: "articles",
        name: "Статьи",
        renderContent: props => <ArticlesList withoutFavorites {...props} />
    }
];

@withContextConsumer(CurrentUser.Consumer)
class AnotherProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profile: null,
            currentTab: tabs[0],
            isOpenFollowersModal: false,
            subscribeToUser: this.subscribeToUser.bind(this)
        };
    }

    componentDidMount() {
        this.props.clearSubscribeToUserErrors();
        const {match, currentUser} = this.props;
        if (currentUser && match.params.id === currentUser.id) {
            return this.props.history.push(routes.profile.path);
        }
        this.loadUser(match.params.id);
    }

    loadUser = async id => {
        const profile = await this.props.getUserById(id);
        this.setState({profile});
    };

    changeTab = tab => {
        this.setState({currentTab: tab});
    };

    subscribeToUser = async () => {
        const {subscribeToUser} = this.props;
        const {profile} = this.state;
        const response = await subscribeToUser(profile.id);
        const {subscribeToUserErrors} = this.props;
        if (Object.keys(subscribeToUserErrors).length === 0) {
            profile.isSubscribed = response.isSubscribed;
            this.setState({profile: profile});
        }
    };

    openFollowersModal = async () => {
        const {
            profile: {id}
        } = this.state;
        await this.props.getSubscriptions(id);
        this.setState({isOpenFollowersModal: true});
    };

    closeFollowersModal = () => {
        this.setState({isOpenFollowersModal: false});
    };

    renderContent = () => {
        const {currentUser, subscribeToUserErrors, followers, following, getSubscriptions} = this.props;
        const {currentTab, profile, subscribeToUser, isOpenFollowersModal} = this.state;
        const isProfilePage = currentTab.id === "profile";
        let props = {currentUser, profile};

        if (isProfilePage) {
            props = {
                ...props,
                followers,
                following,
                subscribeToUser,
                getSubscriptions,
                isOpenFollowersModal,
                subscribeToUserErrors,
                openFollowersModal: this.openFollowersModal.bind(this),
                closeFollowersModal: this.closeFollowersModal.bind(this)
            };
        } else {
            props = {...props, owner: profile};
        }

        return <Content fullHeight={!isProfilePage}>{currentTab.renderContent(props)}</Content>;
    };

    render() {
        const {currentTab, profile} = this.state;

        if (!profile) {
            return <Loading />;
        }

        return (
            <PageWrapper>
                <Column>
                    <Tabs currentTab={currentTab} tabs={tabs} changeTab={tab => this.changeTab(tab)} />
                    {this.renderContent()}
                </Column>
            </PageWrapper>
        );
    }
}

export default AnotherProfile;
