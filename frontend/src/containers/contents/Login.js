import React, {Component} from "react";
import PageWrapper from "containers/contents/PageWrapper";
import LoginForm from "components/login/LoginForm";
import CurrentUser from "contexts/CurrentUser";
import {withContextConsumer} from "utils/contexts";

const fieldsKeys = {
    userName: "userName",
    password: "password"
};

@withContextConsumer(CurrentUser.Consumer)
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: "",
            password: ""
        };
    }

    componentDidMount() {
        this.props.clearLoginErrors();
    }

    onChangeField = (field, value) => {
        this.setState({[field]: value});
    };

    submitForm = async () => {
        const {login} = this.props;
        await login(this.state);
    };

    render() {
        const {loginErrors, history} = this.props;

        return (
            <PageWrapper>
                <LoginForm
                    history={history}
                    errors={loginErrors}
                    fieldsKeys={fieldsKeys}
                    onChangeField={this.onChangeField.bind(this)}
                    submitForm={this.submitForm.bind(this)}
                />
            </PageWrapper>
        );
    }
}

export default Login;
