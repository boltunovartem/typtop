import React, {Component} from "react";
import moment from "moment";

import PageWrapper from "containers/contents/PageWrapper";
import CurrentUser from "contexts/CurrentUser";
import RegisterForm from "components/register/RegisterForm";
import {withContextConsumer} from "utils/contexts";

const fieldsKeys = {
    firstName: "firstName",
    lastName: "lastName",
    middleName: "middleName",
    userName: "userName",
    phoneNumber: "phoneNumber",
    email: "email",
    password: "password",
    confirmPassword: "confirmPassword",
    gender: "gender",
    dateOfBirth: "dateOfBirth",
    workPlace: "workPlace"
};

@withContextConsumer(CurrentUser.Consumer)
class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: null,
            lastName: null,
            middleName: null,
            userName: null,
            phoneNumber: null,
            email: null,
            password: null,
            confirmPassword: null,
            workPlace: null,
            gender: "other",
            dateOfBirth: moment().add(-18, "y")
        };
    }

    componentDidMount() {
        this.props.clearRegisterErrors();
    }

    onChangeField = (field, value) => {
        this.setState({[field]: value});
    };

    submitForm = async () => {
        const {register} = this.props;
        await register(this.state);
    };

    render() {
        const {gender, dateOfBirth} = this.state;
        const {registerErrors, history} = this.props;
        return (
            <PageWrapper>
                <RegisterForm
                    fieldsKeys={fieldsKeys}
                    gender={gender}
                    dateOfBirth={dateOfBirth}
                    history={history}
                    errors={registerErrors}
                    onChangeField={this.onChangeField.bind(this)}
                    submitForm={this.submitForm.bind(this)}
                />
            </PageWrapper>
        );
    }
}

export default Register;
