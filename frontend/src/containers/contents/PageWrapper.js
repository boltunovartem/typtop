import styled from "styled-components";

export default styled.div`
    flex: 1;
    width: 1024px;
    max-width: 100%;

    display: flex;
    justify-content: center;
`;
