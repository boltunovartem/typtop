import React, {Component, Suspense} from "react";
import {Route, Switch} from "react-router-dom";
import styled from "styled-components";
import Footer from "./Footer";
import Header from "./Header";
import Loading from "./Loading";
import Scrollbars from "components/common/Scrollbars";
import routes from "consts/routes";
import Notifications from "containers/contents/Notifications";

const IndexLoadable = React.lazy(() => import("./Index"));
const RegisterLoadable = React.lazy(() => import("./Register"));
const LoginLoadable = React.lazy(() => import("./Login"));
const ProfileLoadable = React.lazy(() => import("./Profile"));
const ArticlesLoadable = React.lazy(() => import("./articles"));

const IndexLoader = props => (
    <Suspense fallback={<Loading />}>
        <IndexLoadable {...props} />
    </Suspense>
);
const RegisterLoader = props => (
    <Suspense fallback={<Loading />}>
        <RegisterLoadable {...props} />
    </Suspense>
);
const LoginLoader = props => (
    <Suspense fallback={<Loading />}>
        <LoginLoadable {...props} />
    </Suspense>
);
const ProfileLoader = props => (
    <Suspense fallback={<Loading />}>
        <ProfileLoadable {...props} />
    </Suspense>
);
const ArticlesLoader = props => (
    <Suspense fallback={<Loading />}>
        <ArticlesLoadable {...props} />
    </Suspense>
);

const ScrollbarContainer = styled.div`
    width: 100vw;
    height: calc(var(--vh, 1vh) * 100);
`;

const Container = styled.div`
    display: flex;
    flex-flow: column;
    align-items: center;

    min-height: 100vh;
    min-height: calc(var(--vh, 1vh) * 100);
`;

class PageContainer extends Component {
    constructor(props) {
        super(props);
        this.scrollbarRef = React.createRef();
    }

    onScroll(e) {
        if (this.content && this.content.onScroll) {
            this.content.onScroll(e);
        }
    }

    render() {
        const props = {
            scrollbarRef: this.scrollbarRef
        };

        return (
            <ScrollbarContainer>
                <Scrollbars scrollRef={this.scrollbarRef} onScroll={this.onScroll.bind(this)}>
                    <Container>
                        <Header scrollbar={this.content} {...props} />
                        <Switch>
                            <Route path={routes.home.path} component={() => <IndexLoader {...props} />} exact />
                            <Route path={routes.signUp.path} component={RegisterLoader} />
                            <Route path={routes.signIn.path} component={LoginLoader} />
                            <Route path={routes.profile.path} component={ProfileLoader} />
                            <Route path={routes.articles.path} component={ArticlesLoader} />
                        </Switch>
                        <Notifications />
                        <Footer {...props} />
                    </Container>
                </Scrollbars>
            </ScrollbarContainer>
        );
    }
}

export default PageContainer;
