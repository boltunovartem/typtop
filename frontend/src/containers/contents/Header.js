import React, {Component} from "react";
import HeaderWrapper from "components/header/HeaderWrapper";
import Button from "components/common/form/Button";
import routes from "consts/routes";
import styled from "styled-components";
import {withRouter} from "react-router-dom";
import {mediaQuery} from "components/common/styled/mediaQuery";
import {withContextConsumer} from "utils/contexts";
import CurrentUser from "contexts/CurrentUser";

const AuthControl = styled.div`
    display: flex;
    margin-right: 1rem;

    ${mediaQuery.phone`
        flex-direction: column;
    `}
`;

const Title = styled.div`
    font-size: 1.5rem;
    margin-left: 1rem;

    cursor: pointer;
`;

const BoldText = styled.span`
    font-weight: 700;
`;

@withRouter
@withContextConsumer(CurrentUser.Consumer)
class Header extends Component {
    renderRegisteredHeader() {
        const {history} = this.props;
        return (
            <>
                <AuthControl>
                    <Button type={"header"} onClick={() => history.push(routes.home.path)}>
                        Главная
                    </Button>
                    <Button type="header" onClick={() => history.push(routes.profile.path)}>
                        Профиль
                    </Button>
                    <Button type={"danger"} borderColor={"#ff1818"} onClick={this.props.logout}>
                        Выход
                    </Button>
                </AuthControl>
            </>
        );
    }

    renderUnregisteredHeader() {
        const {history} = this.props;
        return (
            <AuthControl>
                <Button type="header" onClick={() => history.push(routes.signIn.path)}>
                    Вход
                </Button>
                <Button type="header" onClick={() => history.push(routes.signUp.path)}>
                    Регистрация
                </Button>
            </AuthControl>
        );
    }

    render() {
        const {currentUser, history} = this.props;

        return (
            <HeaderWrapper>
                <Title onClick={() => history.push(routes.home.path)}>
                    <BoldText>Typical</BoldText>Topics
                </Title>
                {currentUser ? this.renderRegisteredHeader() : this.renderUnregisteredHeader()}
            </HeaderWrapper>
        );
    }
}

export default Header;
