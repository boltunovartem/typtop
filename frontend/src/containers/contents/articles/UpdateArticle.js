import React, {Component} from "react";
import {withContextConsumer} from "utils/contexts";
import ArticlesContext from "contexts/Articles";
import CurrentUserContext from "contexts/CurrentUser";
import {Redirect, withRouter} from "react-router-dom";
import "ckeditor5-build-classic-base64-upload-adapter/build/translations/ru";
import routes from "consts/routes";
import Loading from "../Loading";
import CreateArticleForm from "components/articles/articleCreate/CreateArticleForm";
import {Row} from "components/articles/articleCreate/styled";
import Button from "components/common/form/Button";
import {ArticlePageWrapper} from "components/articles/articlePage/styled";

const fieldsKeys = {
    title: "title",
    description: "description",
    content: "content",
    tags: "tags"
};

@withRouter
@withContextConsumer(CurrentUserContext.Consumer)
@withContextConsumer(ArticlesContext.Consumer)
class UpdateArticle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            content: "",
            tags: [],
            showModal: false
        };
    }

    componentDidMount() {
        this.props.clearArticleErrors();
        const {match} = this.props;
        this.getArticle(match.params.id);
    }

    onClickDeleteArticleButton = () => {
        this.setState({showModal: true});
    };

    modalSubmit = async () => {
        const {match, deleteArticle} = this.props;
        await deleteArticle(match.params.id);
    };

    modalClose = () => {
        this.setState({showModal: false});
    };

    async getArticle(id) {
        const {getArticleById} = this.props;
        await getArticleById(id);
        const {currentArticle} = this.props;
        await this.setState({
            title: currentArticle.title,
            description: currentArticle.description,
            content: currentArticle.content,
            tags: currentArticle.tags.map(tag => tag.name)
        });
    }

    onChangeField = (field, value) => {
        this.setState({[field]: value});
    };

    submitArticle = () => {
        const {match, updateArticle} = this.props;
        updateArticle(match.params.id, this.state);
    };

    render() {
        const {currentUser, currentArticle, articleErrors, isLoading, history} = this.props;
        const {title, description, content, tags, showModal} = this.state;
        const shouldRedirectToLogin = !currentUser;

        if (shouldRedirectToLogin) {
            return <Redirect to={{pathname: routes.signIn.path}} />;
        }

        if (isLoading || !currentArticle) {
            return <Loading />;
        }

        return (
            <ArticlePageWrapper>
                <CreateArticleForm
                    currentArticle={currentArticle}
                    isLoading={isLoading}
                    title={title}
                    description={description}
                    content={content}
                    tags={tags}
                    errors={articleErrors}
                    fieldsKeys={fieldsKeys}
                    history={history}
                    onChange={this.onChangeField.bind(this)}
                    submitArticle={this.submitArticle.bind(this)}
                    deleteArticle={this.onClickDeleteArticleButton.bind(this)}
                    modalSubmit={this.modalSubmit.bind(this)}
                    modalClose={this.modalClose.bind(this)}
                    showModal={showModal}
                />
                <Row>
                    <Button type={"danger"} borderColor={"#ff1818"} onClick={() => this.onClickDeleteArticleButton()}>
                        Удалить
                    </Button>
                </Row>
            </ArticlePageWrapper>
        );
    }
}

export default UpdateArticle;
