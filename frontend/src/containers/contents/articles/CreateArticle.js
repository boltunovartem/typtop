import React, {Component} from "react";

import CreateArticleForm from "components/articles/articleCreate/CreateArticleForm";
import {withContextConsumer, withContextProvider} from "utils/contexts";
import ArticlesContext from "contexts/Articles";
import CurrentUserContext from "contexts/CurrentUser";
import {Redirect, withRouter} from "react-router-dom";
import "ckeditor5-build-classic-base64-upload-adapter/build/translations/ru";
import routes from "consts/routes";
import TagsContext from "contexts/Tags";

const fieldsKeys = {
    title: "title",
    description: "description",
    content: "content",
    tags: "tags"
};

@withRouter
@withContextProvider(TagsContext.Provider)
@withContextConsumer(TagsContext.Consumer)
@withContextConsumer(CurrentUserContext.Consumer)
@withContextConsumer(ArticlesContext.Consumer)
class CreateArticle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            content: "",
            tags: []
        };
    }

    componentDidMount() {
        this.props.clearArticleErrors();
        this.props.getTags();
    }

    onChangeField = (field, value) => {
        this.setState({[field]: value});
    };

    submitArticle = () => {
        this.props.createArticle(this.state);
    };

    render() {
        const {currentUser, articleErrors, isLoading, history, tags: tagsFromDB} = this.props;
        const {title, description, content, tags} = this.state;
        const shouldRedirectToLogin = !currentUser;

        if (shouldRedirectToLogin) {
            return <Redirect to={{pathname: routes.signIn.path}} />;
        }

        return (
            <CreateArticleForm
                isLoading={isLoading}
                title={title}
                description={description}
                content={content}
                tags={tags}
                errors={articleErrors}
                fieldsKeys={fieldsKeys}
                history={history}
                suggestions={tagsFromDB.map(u => u.name)}
                onChange={this.onChangeField.bind(this)}
                submitArticle={this.submitArticle.bind(this)}
            />
        );
    }
}

export default CreateArticle;
