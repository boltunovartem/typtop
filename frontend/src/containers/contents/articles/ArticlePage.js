import React, {Component} from "react";
import {Redirect, withRouter} from "react-router-dom";
import Loading from "containers/contents/Loading";
import {withContextConsumer} from "utils/contexts";
import ArticlesContext from "contexts/Articles";
import CurrentUserContext from "contexts/CurrentUser";
import ArticlePageComponent from "components/articles/articlePage/ArticlePageComponent";
import routes from "consts/routes";

@withRouter
@withContextConsumer(ArticlesContext.Consumer)
@withContextConsumer(CurrentUserContext.Consumer)
class ArticlePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            commentText: "",
            shouldRedirectToLogin: false,

            isEditing: false,
            editedComment: {},

            commentsOffset: 0,
            currentCommentsPage: 1,
            commentsLimit: 6
        };
    }

    componentDidMount() {
        const {match, getArticleById} = this.props;

        getArticleById(match.params.id);
        this.updateCommentsList();
    }

    updateCommentsList = () => {
        const {getCommentsByArticleId, match} = this.props;
        getCommentsByArticleId(match.params.id, this.state.commentsOffset, this.state.commentsLimit);
    };

    onChangeCommentText = value => {
        this.setState({commentText: value});
    };

    onComment = async () => {
        const {currentUser, createComment} = this.props;

        if (!currentUser) return this.setState({shouldRedirectToLogin: true});

        await createComment(this.state.commentText);

        const errors = this.props.createCommentErrors;

        if (Object.keys(errors).length === 0) this.setState({commentText: ""});
        this.updateCommentsList();
    };

    onStartEditComment = comment => {
        this.onChangeCommentText(comment.text);
        this.setState({
            editedComment: {...comment},
            isEditing: true
        });
    };

    onCancelEditComment = () => {
        this.setState({
            editedComment: {},
            commentText: "",
            isEditing: false
        });
    };

    onSubmitEditComment = async () => {
        const {editComment} = this.props;
        const {commentText, editedComment} = this.state;
        await editComment(editedComment, commentText);

        const {createCommentErrors} = this.props;
        if (Object.keys(createCommentErrors).length === 0) {
            this.setState({
                editedComment: {},
                commentText: "",
                onEditComment: false
            });
        }
        this.updateCommentsList();
    };

    onCLickDeleteComment = async comment => {
        const {deleteComment} = this.props;
        this.checkEditedComment(comment);
        await deleteComment(comment);
        this.updateCommentsList();
    };

    checkEditedComment = comment => {
        const {editedComment} = this.state;
        if (editedComment.id === comment.id) {
            this.setState({
                commentText: comment.text,
                editedComment: {},
                isEditing: false
            });
        }
    };

    onChangeCommentsPage = async pageNum => {
        await this.setState({
            currentCommentsPage: pageNum,
            commentsOffset: (pageNum - 1) * this.state.commentsLimit
        });
        this.updateCommentsList();
    };

    render() {
        const {
            isLoading,
            currentArticle,
            currentUser,
            onChangeArticleRate,
            history,
            createCommentErrors,
            comments,
            totalComments
        } = this.props;
        const {commentText, shouldRedirectToLogin, isEditing, commentsOffset, currentCommentsPage, commentsLimit} = this.state;

        if (isLoading || !currentArticle) {
            return <Loading />;
        }

        if (shouldRedirectToLogin) {
            return <Redirect to={{pathname: routes.signIn.path}} />;
        }

        return (
            <ArticlePageComponent
                currentUser={currentUser}
                currentArticle={currentArticle}
                history={history}
                commentText={commentText}
                onChangeArticleRate={onChangeArticleRate}
                onChangeComment={this.onChangeCommentText.bind(this)}
                onComment={this.onComment.bind(this)}
                onClickDeleteComment={this.onCLickDeleteComment.bind(this)}
                isEditing={isEditing}
                onStartEditComment={this.onStartEditComment.bind(this)}
                onCancelEditComment={this.onCancelEditComment.bind(this)}
                onSubmitEditComment={this.onSubmitEditComment.bind(this)}
                onChangeCommentsPage={this.onChangeCommentsPage.bind(this)}
                errors={createCommentErrors}
                comments={comments}
                totalComments={totalComments}
                commentsOffset={commentsOffset}
                commentsLimit={commentsLimit}
                currentCommentsPage={currentCommentsPage}
            />
        );
    }
}

export default ArticlePage;
