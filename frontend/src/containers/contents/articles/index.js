import React, {Component} from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import routes from "consts/routes";
import PageWrapper from "containers/contents/PageWrapper";
import CreateArticle from "./CreateArticle";
import ArticlePage from "containers/contents/articles/ArticlePage";
import UpdateArticle from "./UpdateArticle";

class Articles extends Component {
    render() {
        return (
            <PageWrapper>
                <Switch>
                    <Route path={routes.createArticle.path} component={props => <CreateArticle {...props} />} exact />
                    <Route path={routes.updateArticle.path} component={props => <UpdateArticle {...props} />} exact />
                    <Route path={routes.article.path} component={props => <ArticlePage {...props} />} exact />
                    <Route component={() => <Redirect to={routes.home.path} />} />
                </Switch>
            </PageWrapper>
        );
    }
}

export default Articles;
