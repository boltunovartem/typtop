import React, {Component} from "react";
import {withContextConsumer} from "utils/contexts";
import NotificationsContext from "contexts/Notifications";
import styled from "styled-components";

const NotificationsWrapper = styled.div`
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 9999;

    .notification {
        display: flex;
        width: calc(100% - 1.4rem);
        border-radius: 5px;
        padding: 1rem;
        margin: 0.4rem;
        color: #fff;
        justify-content: center;
        box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.5);
    }

    .notification-success {
        background: #22bb22;
    }

    .notification-error {
        background-color: #bb2222;
    }
`;

const NotificationItem = styled.div``;

@withContextConsumer(NotificationsContext.Consumer)
class Notifications extends Component {
    render() {
        const {notifications} = this.props;
        return (
            <NotificationsWrapper>
                {notifications.map(props => (
                    <NotificationElem key={props.id} {...props} {...this.props} />
                ))}
            </NotificationsWrapper>
        );
    }
}

function NotificationElem(props) {
    const {type, content, id, closeNotification} = props;

    return (
        <NotificationItem className={`notification notification-${type}`} onClick={() => closeNotification(id)}>
            {content}
        </NotificationItem>
    );
}

export default Notifications;
