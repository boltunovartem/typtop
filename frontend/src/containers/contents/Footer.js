import React, {Component} from "react";
import styled from "styled-components";
import {mediaQuery} from "components/common/styled/mediaQuery";

const FooterWrapper = styled.div`
    position: relative;

    background-color: var(--white);

    display: flex;
    align-items: center;
    justify-content: center;

    padding: 0 1rem;

    width: 100%;
    height: 4rem;
    box-shadow: 0px 0px 6px rgba(0, 0, 0, 0.55);
    margin-top: 1rem;

    color: #323232;

    ${mediaQuery.phone`
        display: block;
        color: #8b8b8b;
        box-shadow: none;
        background-color: #f8f8fa;
        padding: 1.2rem 1rem;
        height: 8rem;
        margin-top: 0;
    `}
`;

class Footer extends Component {
    render() {
        return <FooterWrapper>© Dream Team, 2020</FooterWrapper>;
    }
}

export default Footer;
