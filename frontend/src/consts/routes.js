const routes = {
    home: {path: "/"},
    articles: {path: "/articles"},
    article: {path: "/articles/:id"},
    createArticle: {path: "/articles/create"},
    updateArticle: {path: "/articles/update/:id"},
    signUp: {path: "/sign-up"},
    signIn: {path: "/sign-in"},
    profile: {path: "/profile"},
    profileById: {path: "/profile/:id"}
};

const getRoute = (route, params = {}) => {
    const routePath = typeof route === "object" ? route.path : route;
    return Object.keys(params).reduce((acc, key) => acc.replace(`:${key}`, params[key]), routePath);
};

export {getRoute};
export default routes;
