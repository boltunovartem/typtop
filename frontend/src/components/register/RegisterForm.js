import React, {Component} from "react";
import {DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import styled from "styled-components";

import {mediaQuery} from "components/common/styled/mediaQuery";
import Input from "components/common/form/Input";
import Button from "components/common/form/Button";
import routes from "consts/routes";
import {Title} from "components/common/form/Title";

const Form = styled.div`
    display: flex;
    height: fit-content;
    flex-direction: column;
    width: 40rem;
    padding: 1rem;
    box-shadow: 0px 1px 4px 0px #999;
    min-width: fit-content;

    ${mediaQuery.phone`
        width: 100%;
        height: 100%;
        padding: 0 1rem;
    `}
`;

const Row = styled.div`
    display: flex;
    width: 100%;
    justify-content: space-around;
    min-width: fit-content;

    ${mediaQuery.phone`
        flex-direction: ${p => (p.onlyRow ? "row" : "column")};
    `}
`;

const GenderWrapper = styled.div`
    display: flex;
    padding: 1rem;
    align-items: center;
    justify-content: space-between;
    width: 60%;
    min-width: 300px;
    max-width: 350px;

    ${mediaQuery.phone`
        flex-direction: column;
        align-items: baseline;
    `}
`;

class RegisterForm extends Component {
    render() {
        const {fieldsKeys, dateOfBirth, gender, history, errors, onChangeField, submitForm} = this.props;

        return (
            <Form>
                <Title>Регистрация</Title>
                <Row>
                    <Input placeholder={"Имя*"} onChange={v => onChangeField(fieldsKeys.firstName, v)} error={errors.firstName} />
                    <Input
                        placeholder={"Фамилия*"}
                        onChange={v => onChangeField(fieldsKeys.lastName, v)}
                        error={errors.lastName}
                    />
                    <Input placeholder={"Отчество"} onChange={v => onChangeField(fieldsKeys.middleName, v)} />
                </Row>

                <MuiPickersUtilsProvider utils={MomentUtils} locale={"ru"}>
                    <DatePicker
                        style={{padding: "0.5rem"}}
                        variant="inline"
                        format={"Дата рождения DD.MM.yyyy"}
                        inputVariant="outlined"
                        animateYearScrolling
                        value={dateOfBirth}
                        onChange={v => onChangeField(fieldsKeys.dateOfBirth, v)}
                    />
                </MuiPickersUtilsProvider>

                <GenderWrapper>
                    <b>Пол: </b>
                    <Button
                        selected={gender === "male"}
                        borderColor={"#050558"}
                        onClick={() => onChangeField(fieldsKeys.gender, "male")}>
                        Мужской
                    </Button>
                    <Button
                        selected={gender === "female"}
                        borderColor={"#050558"}
                        onClick={() => onChangeField(fieldsKeys.gender, "female")}>
                        Женский
                    </Button>
                    <Button
                        selected={gender === "other"}
                        borderColor={"#050558"}
                        onClick={() => onChangeField(fieldsKeys.gender, "other")}>
                        Другой
                    </Button>
                </GenderWrapper>

                <Input placeholder={"Место работы"} onChange={v => onChangeField(fieldsKeys.workPlace, v)} />
                <Row>
                    <Input
                        placeholder={"Номер телефона +7 (xxx) xxx-xx-xx"}
                        type="tel"
                        formatOptions="+7 (###) ###-##-##"
                        onChange={v => onChangeField(fieldsKeys.phoneNumber, v)}
                        error={errors.phoneNumber}
                    />
                    <Input
                        placeholder={"E-mail*"}
                        type={fieldsKeys.email}
                        onChange={v => onChangeField(fieldsKeys.email, v)}
                        error={errors.email}
                    />
                </Row>
                <Input
                    placeholder={"Логин*"}
                    type={fieldsKeys.userName}
                    onChange={v => onChangeField(fieldsKeys.userName, v)}
                    error={errors.userName}
                />
                <Input
                    placeholder={"Пароль*"}
                    type={fieldsKeys.password}
                    onChange={v => onChangeField(fieldsKeys.password, v)}
                    error={errors.password}
                />
                <Input
                    placeholder={"Подтвердите пароль*"}
                    type={fieldsKeys.password}
                    onChange={v => onChangeField(fieldsKeys.confirmPassword, v)}
                    error={errors.confirmPassword}
                />
                <Button style={{width: "100%"}} borderColor={"#050558"} onClick={submitForm}>
                    Регистрация
                </Button>
                <Button
                    style={{width: "100%", fontSize: 13, color: "#696969", textDecoration: "underline"}}
                    onClick={() => history.push(routes.signIn.path)}>
                    Уже есть аккаунт? Выполните вход
                </Button>
            </Form>
        );
    }
}

export default RegisterForm;
