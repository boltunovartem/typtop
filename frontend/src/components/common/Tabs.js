import React, {Component} from "react";
import styled from "styled-components";

const TabsWrapper = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-around;
    border: 1px solid #050558;
    height: fit-content;
    border-radius: 5px;
`;

const TabItemWrapper = styled.div`
    width: 100%;
    height: fit-content;
    padding: 1rem;
    display: flex;
    justify-content: center;

    transition: 0.6s;
    transition-timing-function: cubic-bezier;

    ${p =>
        p.isSelected &&
        `
        color: #FFF;
        background: #050558;
    `}

    :hover {
        color: #fff;
        background: #050558;
    }
`;

class Tabs extends Component {
    render() {
        const {tabs, currentTab, changeTab} = this.props;
        return (
            <TabsWrapper>
                {tabs.map(tab => (
                    <TabItem key={tab.id} tab={tab} changeTab={changeTab} isSelected={currentTab.id === tab.id} />
                ))}
            </TabsWrapper>
        );
    }
}

class TabItem extends Component {
    render() {
        const {tab, changeTab, isSelected} = this.props;
        return (
            <TabItemWrapper onClick={() => changeTab(tab)} isSelected={isSelected}>
                {tab.name}
            </TabItemWrapper>
        );
    }
}

export default Tabs;
