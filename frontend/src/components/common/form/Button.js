import React, {Component} from "react";
import styled from "styled-components";

const getStyleByType = (type, selected) => {
    let background,
        color,
        hover = "{ background: #000}";
    switch (type) {
        case "danger":
            background = "#00000000";
            color = "#ff1818";
            break;
        case "header":
            background = "#00000000";
            color = "#FAFAFA";
            break;
        default:
            background = selected ? "#05055811" : "#00000000";
            color = "#050558";
    }

    return `
        background: ${background};
        color: ${color};
        :hover: ${hover};
    `;
};

const ButtonWrapper = styled.button`
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0.3rem;
    padding: 0.6rem;
    width: 8rem;
    ${p => getStyleByType(p.type, p.selected)};
    border: "none";
    font-size: 1rem;
    cursor: pointer;
    ${p => p.disabled && "opacity: .3;"}

    :focus {
        outline: none;
    }
`;

const BlockWrapper = styled.div`
    .forwardBorderTrain {
        :before,
        :after {
            border-top: 1px solid ${p => p.borderColor || "white"} !important;
            border-bottom: 1px solid ${p => p.borderColor || "white"} !important;
        }
    }

    .ui-border-element {
        :before,
        :after {
            border-left: 1px solid ${p => p.borderColor || "white"} !important;
            border-right: 1px solid ${p => p.borderColor || "white"} !important;
        }
    }
`;

const IconWrapper = styled.div`
    display: inline;
    margin-right: 0.5rem;
`;

class Button extends Component {
    render() {
        const {children, type, borderColor, selected, icon, ...rest} = this.props;
        return (
            <BlockWrapper borderColor={borderColor} selected={selected}>
                <ButtonWrapper className="ui-box forwardBorderTrain" selected={selected} type={type} {...rest}>
                    {icon && <IconWrapper>{icon}</IconWrapper>}
                    <span className="ui-border-element">{children}</span>
                </ButtonWrapper>
            </BlockWrapper>
        );
    }
}

export default Button;
