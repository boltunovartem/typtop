import React, {createRef, PureComponent} from "react";
import {mediaQuery} from "components/common/styled/mediaQuery";
import styled, {css} from "styled-components";
import TextareaAutosize from "react-textarea-autosize";
import Cleave from "cleave.js/react";
import "cleave.js/dist/addons/cleave-phone.ru";
import NumberFormatElem from "react-number-format";

const InputStyle = css`
    margin: 0.3rem;
    padding: 1rem;
    border: none;
    font-size: 0.9rem;
    box-shadow: 0 0.0925rem 0.175rem rgba(0, 0, 0, 0.4);
    width: -webkit-fill-available;
    width: -moz-available;

    :focus {
        background: #fff;
        box-shadow: 0 0.0925rem 0.225rem #050558;
        outline: none;
    }
`;

const InputElement = styled.input`
    ${InputStyle}
`;

const InputWrapper = styled.div`
    width: -webkit-fill-available;
    width: -moz-available;
    margin-bottom: 1rem;
`;

const InputTitle = styled.div`
    color: #9e9e9e;
    font-size: 0.9rem;
    ${mediaQuery.phone`margin-bottom: 0;`}
`;

const CleaveElement = styled(Cleave)`
    ${InputStyle}
`;

const TextArea = styled(TextareaAutosize)`
    min-height: 2.75rem;
    ${InputStyle}
`;

const NumberFormat = styled(NumberFormatElem)`
    ${InputStyle}
`;

const Error = styled.div`
    padding: 0 1rem;
    color: #c00;
    font-size: 0.9rem;
`;

class Input extends PureComponent {
    constructor(props) {
        super(props);
        this.inputRef = createRef();
    }

    getInnerRef() {
        return this.inputRef;
    }

    onKeyPressed(e) {
        const {onEnter} = this.props;
        if (onEnter && e.key === "Enter") {
            onEnter();
        }
    }

    onChange(e) {
        const {onChange, formatFunction} = this.props;
        if (formatFunction) {
            e.target.rawValue = formatFunction(e.target.value);
            e.target.value = e.target.rawValue;
        }
        onChange && onChange(e.target.value);
    }

    onChangePhone(v) {
        const {onChange} = this.props;
        onChange && onChange(v.formattedValue);
    }

    getInputElement() {
        const {
            id,
            formatOptions,
            error,
            type,
            multiline,
            placeholder,
            value,
            defaultValue,
            maxLength,
            onFocus,
            onBlur,
            pattern,
            max,
            minRows,
            maxRows,
            inputStyle,
            disabled
        } = this.props;

        const commonProps = {
            onFocus: e => onFocus && onFocus(e.target.value),
            onKeyPress: this.onKeyPressed.bind(this),
            style: inputStyle,
            id,
            disabled,
            error,
            type,
            placeholder,
            onBlur
        };
        if (!formatOptions && !multiline) {
            return (
                <InputElement
                    value={value}
                    defaultValue={defaultValue}
                    maxLength={maxLength}
                    onChange={this.onChange.bind(this)}
                    pattern={pattern}
                    max={max}
                    ref={this.inputRef}
                    {...commonProps}
                />
            );
        }
        if (multiline) {
            return (
                <TextArea
                    value={value}
                    defaultValue={defaultValue}
                    onChange={this.onChange.bind(this)}
                    minRows={minRows}
                    maxRows={maxRows || 4}
                    maxLength={maxLength}
                    inputRef={this.inputRef}
                    {...commonProps}
                />
            );
        }

        if (type === "tel") {
            return (
                <NumberFormat
                    format={formatOptions}
                    defaultValue={defaultValue}
                    placeholder={placeholder}
                    onValueChange={this.onChangePhone.bind(this)}
                    value={value}
                />
            );
        }
        return <CleaveElement options={formatOptions} value={defaultValue} onInput={this.onChange.bind(this)} {...commonProps} />;
    }

    getBottomElement() {
        const {error} = this.props;
        if (!error) {
            return;
        }
        return <Error>{error}</Error>;
    }

    render() {
        const {title, style, titleStyle} = this.props;

        return (
            <InputWrapper style={style}>
                {title && <InputTitle style={titleStyle}>{title}</InputTitle>}
                {this.getInputElement()}
                {this.getBottomElement()}
            </InputWrapper>
        );
    }
}

export default Input;
