import React, {Component} from "react";
import styled from "styled-components";
import Favorite from "@material-ui/icons/Favorite";
import FavoriteBorder from "@material-ui/icons/FavoriteBorder";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import SelectSearch from "react-select-search";
import "react-select-search/style.css";
import {Manager, Popper, Reference} from "react-popper";
import MomentUtils from "@date-io/moment";
import {DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import Button from "components/common/form/Button";
import {Row} from "components/articles/articlesList/styled";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import {throttle} from "lodash";

export const Wrapper = styled.div`
    display: flex;
    background-color: #FFF;
    margin: 0.5rem;
    padding: 0.5rem;
    height: ${p => p.height || "80%"};
    width: ${p => p.width || "100%"};
    justify-content: center;
    align-items: center;
    border-radius: 5px;
    
    ${p => p.type === "date" && "border: 1px solid #ebebeb;"}
    ${p =>
        p.withHover &&
        `
        :hover {
            box-shadow: 0px 0px 0px 1px #050558;
        }
    `}
    
    .select-search, .select-search__value, .select-search__input { 
        width: ${p => p.width || "unset"};
        ${p => p.height && `height: ${p.height};`}
    }
    .select-search__value::after {
        margin-left: 1rem;
    }
    .select-search__options { 
        padding: 0rem;
        margin-top: 0.4rem;
        margin-bottom: 0;
    }
    .select-search__option {
        padding: 1.4rem 1rem;
        display: flex;
        align-items: center;
    }
     
    .select-search__option.is-selected,
    .select-search__option:not(.is-selected):hover,
    .select-search__option.is-selected:hover {
        background: #050558;
        color: #FFF;
    }       
    .select-search:not(.select-search--multiple) .select-search__input:hover {
        border-color: #050558;
    }
    .MuiFormControlLabel-root {
        margin: 0;
    }
}
`;

const DateBlock = styled.div`
    background: #fff;
    box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.2);
    margin-top: 0.5rem;
    margin-left: -0.5rem;
    z-index: 100;
`;

class FilterItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpenDate: false,
            startDate: null,
            endDate: null
        };
    }

    toggleDate() {
        this.setState({isOpenDate: !this.state.isOpenDate});
    }

    toggleRate() {
        this.setState({isOpenRate: !this.state.isOpenRate});
    }

    getDateText = () => {
        const {filters, name} = this.props;
        const {date} = filters;
        if (!date) return name;

        const {startDate, endDate} = date;
        if (!endDate) {
            return startDate ? `От ${new Date(startDate).toLocaleDateString()}` : name;
        }

        if (!startDate) {
            return endDate ? `До ${new Date(endDate).toLocaleDateString()}` : name;
        }

        return `${new Date(startDate).toLocaleDateString()} - ${new Date(endDate).toLocaleDateString()}`;
    };

    renderPopupDateElem = () => {
        const {id, onChange} = this.props;
        const {isOpenDate, startDate, endDate} = this.state;

        return (
            <Wrapper type={id} withHover>
                <Manager>
                    <Reference>
                        {({ref}) => (
                            <div onClick={() => this.toggleDate()} style={{width: "100%"}} ref={ref}>
                                {this.getDateText()}
                            </div>
                        )}
                    </Reference>
                    <Popper placement={"bottom-start"}>
                        {({ref, style}) =>
                            isOpenDate ? (
                                <DateBlock ref={ref} style={style}>
                                    <MuiPickersUtilsProvider utils={MomentUtils} locale={"ru"}>
                                        <DatePicker
                                            label="От"
                                            format={"DD.MM.yyyy"}
                                            inputVariant="outlined"
                                            style={{margin: "1rem"}}
                                            variant="inline"
                                            value={startDate}
                                            disableFuture
                                            onChange={startDate => this.setState({startDate})}
                                            autoOk
                                        />
                                        <DatePicker
                                            label="До"
                                            format={"DD.MM.yyyy"}
                                            inputVariant="outlined"
                                            style={{margin: "1rem"}}
                                            variant="inline"
                                            minDate={startDate || ""}
                                            value={endDate}
                                            onChange={endDate => this.setState({endDate})}
                                            autoOk
                                        />
                                    </MuiPickersUtilsProvider>
                                    <Row>
                                        <Button
                                            type={"danger"}
                                            onClick={() => {
                                                onChange({startDate: null, endDate: null});
                                                this.toggleDate();
                                            }}>
                                            Сбросить
                                        </Button>
                                        <Button
                                            onClick={() => {
                                                if (startDate.isAfter(endDate)) return;
                                                onChange({startDate, endDate});
                                                this.toggleDate();
                                            }}>
                                            Готово
                                        </Button>
                                    </Row>
                                </DateBlock>
                            ) : null
                        }
                    </Popper>
                </Manager>
            </Wrapper>
        );
    };

    renderSliderElem = () => {
        const {id, onChange, filters, min, max} = this.props;

        return (
            <Wrapper style={{display: "unset"}}>
                <Typography id="range-slider" gutterBottom>
                    Рейтинг
                </Typography>
                <Slider
                    value={filters[id] || [min, max]}
                    onChange={throttle((event, value) => onChange(value), 10100)}
                    valueLabelDisplay="auto"
                    min={min}
                    max={max}
                    aria-labelledby="range-slider"
                />
            </Wrapper>
        );
    };

    renderDropdownElem = () => {
        const {name, onChange, tags} = this.props;

        return (
            <Wrapper width={"15rem"} height={"5vh"}>
                <SelectSearch
                    options={tags}
                    onChange={value => onChange(value)}
                    placeholder={name}
                    printOptions={"on-focus"}
                    multiple
                    search
                />
            </Wrapper>
        );
    };

    renderCheckBoxElem = () => {
        const {name, onChange} = this.props;
        return (
            <Wrapper width={"fit-content"}>
                <FormControlLabel
                    control={
                        <Checkbox
                            icon={<FavoriteBorder />}
                            checkedIcon={<Favorite />}
                            onChange={e => onChange(e.target.checked)}
                        />
                    }
                    label={name}
                />
            </Wrapper>
        );
    };

    render() {
        const {elem} = this.props;

        switch (elem) {
            case "popup":
                return this.renderPopupDateElem();
            case "dropdown":
                return this.renderDropdownElem();
            case "slider":
                return this.renderSliderElem();
            case "checkBox":
                return this.renderCheckBoxElem();
            default:
                return null;
        }
    }
}

export default FilterItem;
