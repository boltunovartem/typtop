import React, {Component} from "react";
import {Scrollbars} from "react-custom-scrollbars-rtl";

export default class extends Component {
    render() {
        const {scrollRef, children, ...rest} = this.props;
        return (
            <Scrollbars ref={scrollRef} {...rest}>
                {children}
            </Scrollbars>
        );
    }
}
