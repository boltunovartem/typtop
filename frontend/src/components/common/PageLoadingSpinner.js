import React from "react";
import styled from "styled-components";
import Spinner from "./Spinner";

const SpinnerContainer = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
`;

export default props => (
    <SpinnerContainer>
        <Spinner height="7rem" stroke="#050558" {...props} />
    </SpinnerContainer>
);
