import React from "react";
import styled from "styled-components";
import {mediaQuery} from "components/common/styled/mediaQuery";

const Icon = props => (
    <svg viewBox="0 0 512 512" fill={"#FFF"} {...props}>
        <g>
            <g>
                <g>
                    <path
                        d="M492,127.5h-18v-18c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v18h-18c-11.046,0-20,8.954-20,20s8.954,20,20,20
                        h18v18c0,11.046,8.954,20,20,20c11.046,0,20-8.954,20-20v-18h18c11.046,0,20-8.954,20-20S503.046,127.5,492,127.5z"
                    />
                    <path
                        d="M315.409,249.231C345.854,225.711,365.5,188.86,365.5,147.5C365.5,76.645,307.855,19,237,19S108.5,76.645,108.5,147.5
                        c0,41.359,19.646,78.211,50.091,101.731C68.293,280.793,0,367.427,0,473c0,11.046,8.954,20,20,20h434c11.046,0,20-8.954,20-20
                        C474,367.401,405.656,280.775,315.409,249.231z M148.5,147.5c0-48.799,39.701-88.5,88.5-88.5s88.5,39.701,88.5,88.5
                        S285.799,236,237,236S148.5,196.299,148.5,147.5z M41.008,453C51.061,353.73,135.123,276,237,276s185.939,77.73,195.992,177
                        H41.008z"
                    />
                </g>
            </g>
        </g>
    </svg>
);

const FollowersWrapper = styled.div`
    position: absolute;
    right: -4rem;
    top: 1.5rem;

    background: #050558;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 3rem;
    height: 3rem;
    transition: 0.3s;

    :hover {
        transform: scale(1.07);
        box-shadow: 0px 7px 4px -3px rgba(0, 0, 0, 0.5);

        :after {
            opacity: 1;
        }
    }

    :after {
        content: "Подписчики";
        position: absolute;
        left: calc(100% + 0.5rem);
        transition: 0.3s;
        opacity: 0;
    }

    ${mediaQuery.phone`
        position: relative;
        left: 50%;
        top: 0;
        transform: translateX(-150%);
        margin-top: 1rem;
        margin-bottom: 1rem;
        
        :hover {
            transform: translateX(-150%) scale(1.07);
        }
        
        :after {
            opacity: 1;
        }
   `}
`;

export default ({onClick, ...props}) => (
    <FollowersWrapper onClick={onClick}>
        <Icon {...props} />
    </FollowersWrapper>
);
