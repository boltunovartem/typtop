import React from "react";
import styled from "styled-components";

const getColorByUserRate = (rate, isUpArrow) => {
    if (isUpArrow && rate === "positive") return "#0C0";

    if (!isUpArrow && rate === "negative") return "#C00";

    return "#898989";
};

export const SimpleArrow = ({currentUserRate, isUpArrow, ...props}) => (
    <svg viewBox="0 0 18 10" fill={getColorByUserRate(currentUserRate, isUpArrow)} {...props}>
        <path
            d="M17.85 9.65c.1-.1.15-.25.15-.35 0-.15-.05-.25-.15-.35L9.8.4C9.65.2 9.35.05 9 .05c-.35 0-.65.15-.8.35L.15 8.95a.6.6 0 000 .7c.15.2.5.35.8.35h16.1c.35 0 .65-.15.8-.35z"
            fillRule="evenodd"
        />
    </svg>
);

export default styled(SimpleArrow)`
    transition: 0.5s;
    :hover {
        fill: ${p => (p.isUpArrow ? "#0C0" : "#C00")};
    }
`;
