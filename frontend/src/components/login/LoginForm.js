import React, {Component} from "react";
import Button from "components/common/form/Button";
import routes from "consts/routes";
import styled from "styled-components";
import {mediaQuery} from "components/common/styled/mediaQuery";
import Input from "components/common/form/Input";
import {Title} from "components/common/form/Title";

const Form = styled.div`
    display: flex;
    height: fit-content;
    flex-direction: column;
    width: 40rem;
    padding: 1rem;
    box-shadow: 0px 1px 4px 0px #999;

    ${mediaQuery.phone`
        width: 100%;
        height: 100%;
        padding: 0 1rem;
    `}
`;

const Error = styled.div`
    padding: 0 1rem;
    color: #c00;
    font-size: 0.9rem;
`;

class LoginForm extends Component {
    render() {
        const {fieldsKeys, onChangeField, submitForm, errors, history} = this.props;

        return (
            <Form>
                <Title>Вход</Title>
                <Input
                    placeholder={"Логин*"}
                    type={fieldsKeys.userName}
                    error={errors.userName}
                    onChange={e => onChangeField(fieldsKeys.userName, e)}
                />
                <Input
                    placeholder={"Пароль*"}
                    type={fieldsKeys.password}
                    error={errors.password}
                    onChange={e => onChangeField(fieldsKeys.password, e)}
                />
                {errors.error && <Error>{errors.error}</Error>}
                <Button style={{width: "100%"}} borderColor={"#050558"} onClick={submitForm}>
                    Вход
                </Button>
                <Button
                    style={{width: "100%", fontSize: 13, color: "#696969", textDecoration: "underline"}}
                    onClick={() => history.push(routes.signUp.path)}>
                    Впервые на сайте? Пройдите регистрацию!
                </Button>
            </Form>
        );
    }
}

export default LoginForm;
