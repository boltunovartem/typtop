import React from "react";
import styled from "styled-components";
import {mediaQuery} from "components/common/styled/mediaQuery";

const HeaderWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-content: center;
    align-items: center;

    width: 100%;
    height: 4rem;

    background: #050558;
    color: #fafafa;
    box-shadow: 0px 0px 6px rgba(0, 0, 0, 0.55);
    margin-bottom: 1rem;

    ${mediaQuery.phone`
        flex-direction: column;
        height: fit-content;
        margin-bottom: 0;
    `}
`;

export default props => <HeaderWrapper>{props.children}</HeaderWrapper>;
