import React, {Component} from "react";
import styled from "styled-components";
import Button from "components/common/form/Button";
import Alert from "@material-ui/lab/Alert";
import Followers from "components/common/icons/Followers";
import Modal from "react-bootstrap/Modal";
import Tabs from "components/common/Tabs";
import routes, {getRoute} from "consts/routes";
import {UserItem} from "containers/contents/Profile/styled";
import {withRouter} from "react-router-dom";

const ProfileInfo = styled.div`
    display: flex;
    width: 100%;
    flex-direction: column;
    align-items: center;

    padding: 1rem;
    box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.5);
`;

const Name = styled.h2`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const Field = styled.div`
    font-size: 18px;
    margin-top: 1rem;
`;

const followerModalTabs = [
    {
        id: "following",
        name: "Подписки"
    },
    {
        id: "followers",
        name: "Подписчики"
    }
];

@withRouter
class ShowProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentTab: followerModalTabs[0]
        };
    }

    renderGender = gender => {
        switch (gender) {
            case "male":
                return "Мужской";
            case "female":
                return "Женский";
            default:
                return "Не указан";
        }
    };

    changeTab = async tab => {
        const {profile} = this.props;
        await this.props.getSubscriptions(profile.id);
        this.setState({currentTab: tab});
    };

    getUsersList = () => {
        const {followers, following} = this.props;
        const {currentTab} = this.state;

        const isFollowersTab = currentTab.id === "followers";
        const isFollowingTab = currentTab.id === "following";

        if (isFollowersTab) {
            if (!followers.length) return "Нет подписчиков";
            return followers.map(u => {
                const {id, firstName, lastName} = u;
                return (
                    <UserItem key={id} onClick={() => this.props.history.push(getRoute(routes.profileById, {id}))}>
                        {`${firstName} ${lastName}`}
                    </UserItem>
                );
            });
        }
        if (isFollowingTab) {
            if (!following.length) return "Нет подписок";
            return following.map(u => {
                const {id, firstName, lastName} = u;
                return (
                    <UserItem key={id} onClick={() => this.props.history.push(getRoute(routes.profileById, {id}))}>
                        {`${firstName} ${lastName}`}
                    </UserItem>
                );
            });
        }
    };

    render() {
        const {
            currentUser,
            profile,
            subscribeToUser,
            subscribeToUserErrors,
            openFollowersModal,
            closeFollowersModal,
            isOpenFollowersModal
        } = this.props;
        const {firstName, lastName, middleName, dateOfBirth, gender, workPlace} = profile;
        const {currentTab} = this.state;

        return (
            <ProfileInfo>
                {currentUser && <Followers width={25} height={25} onClick={() => openFollowersModal()} />}

                <Name>
                    {`${firstName} ${lastName}${middleName ? " " + middleName : ""}`}
                    {currentUser && currentUser.id !== profile.id ? (
                        <Button borderColor={"#050558"} style={{width: "10rem"}} onClick={() => subscribeToUser()}>
                            {profile.isSubscribed ? "Отписаться" : "Подписаться"}
                        </Button>
                    ) : (
                        <div style={{width: "10rem"}} />
                    )}
                </Name>
                <Field>Пол: {this.renderGender(gender)}</Field>
                {workPlace && <Field>Место работы: {workPlace}</Field>}
                <Field>Дата рождения: {new Date(dateOfBirth).toLocaleDateString()}</Field>
                {Object.keys(subscribeToUserErrors).length !== 0 && (
                    <Alert severity={"error"}>Вы не можете подписаться на этогопользователя</Alert>
                )}

                <Modal show={isOpenFollowersModal} onHide={() => closeFollowersModal()}>
                    <Modal.Header>
                        <Tabs currentTab={currentTab} tabs={followerModalTabs} changeTab={tab => this.changeTab(tab)} />
                    </Modal.Header>
                    <Modal.Body>{this.getUsersList()}</Modal.Body>
                    <Modal.Footer>
                        <Button onClick={() => closeFollowersModal()}>Закрыть</Button>
                    </Modal.Footer>
                </Modal>
            </ProfileInfo>
        );
    }
}

export default ShowProfile;
