import React, {Component} from "react";
import styled from "styled-components";
import Input from "components/common/form/Input";
import {mediaQuery} from "components/common/styled/mediaQuery";
import MomentUtils from "@date-io/moment";
import {DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import Button from "components/common/form/Button";
import Alert from "@material-ui/lab/Alert";
import Modal from "react-bootstrap/Modal";
import Followers from "components/common/icons/Followers";
import Tabs from "components/common/Tabs";
import routes, {getRoute} from "consts/routes";
import {withRouter} from "react-router-dom";
import {UserItem} from "containers/contents/Profile/styled";

const Form = styled.div`
    position: relative;
    width: 40rem;
    min-width: fit-content;
    display: flex;
    height: fit-content;
    flex-direction: column;
    padding: 1rem;
    box-shadow: 0px 1px 4px 0px #999;

    ${mediaQuery.phone`
        width: 100%;
        height: 100%;
        padding: 0 1rem;
    `}
`;

const Row = styled.div`
    display: flex;
    width: 100%;
    justify-content: space-around;

    ${mediaQuery.phone`
        flex-direction: ${p => (p.onlyRow ? "row" : "column")};
    `}
`;

const GenderWrapper = styled.div`
    display: flex;
    padding: 1rem;
    align-items: center;
    justify-content: space-between;
    width: 60%;
    min-width: 300px;
    max-width: 350px;

    ${mediaQuery.phone`
        flex-direction: column;
        align-items: baseline;
    `}
`;

const followerModalTabs = [
    {
        id: "following",
        name: "Подписки"
    },
    {
        id: "followers",
        name: "Подписчики"
    }
];

@withRouter
class ProfileForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentTab: followerModalTabs[0]
        };
    }

    changeTab = async tab => {
        await this.props.getSubscriptions();
        this.setState({currentTab: tab});
    };

    getUsersList = () => {
        const {followers, following, subscribeToUser, getSubscriptions} = this.props;
        const {currentTab} = this.state;

        const isFollowersTab = currentTab.id === "followers";
        const isFollowingTab = currentTab.id === "following";

        if (isFollowersTab) {
            if (!followers.length) return "У вас нет подписчиков";
            return followers.map(u => {
                const {id, firstName, lastName} = u;
                const isFollowing = !!following.find(item => item.id === id);
                return (
                    <UserItem key={id} onClick={() => this.props.history.push(getRoute(routes.profileById, {id}))}>
                        {`${firstName} ${lastName}`}
                        <Button
                            borderColor={"#050558"}
                            onClick={async e => {
                                e.stopPropagation();
                                await subscribeToUser(id);
                                await getSubscriptions();
                            }}>
                            {isFollowing ? "Отписаться" : "Подписаться"}
                        </Button>
                    </UserItem>
                );
            });
        }

        if (isFollowingTab) {
            if (!following.length) return "Вы ни на кого не подписаны";
            return following.map(u => {
                const {id, firstName, lastName} = u;
                return (
                    <UserItem key={id} onClick={() => this.props.history.push(getRoute(routes.profileById, {id}))}>
                        {`${firstName} ${lastName}`}
                        <Button
                            borderColor={"#050558"}
                            onClick={async e => {
                                e.stopPropagation();
                                await subscribeToUser(id);
                                await getSubscriptions();
                            }}>
                            Отписаться
                        </Button>
                    </UserItem>
                );
            });
        }
    };

    render() {
        const {
            fieldsValues,
            fieldsKeys,
            onChangeField,
            submitForm,
            resetForm,
            deleteProfile,
            errors,
            hasErrors,
            showModal,
            openFollowersModal,
            closeFollowersModal,
            isOpenFollowersModal,
            modalSubmit,
            modalClose
        } = this.props;

        const {currentTab} = this.state;

        const {isSent, waitResponse} = fieldsValues;
        return (
            <Form>
                <Followers width={25} height={25} onClick={() => openFollowersModal()} />
                <Row>
                    <Input
                        placeholder={"Имя*"}
                        value={fieldsValues.firstName || ""}
                        onChange={v => onChangeField(fieldsKeys.firstName, v)}
                        error={errors.firstName}
                    />
                    <Input
                        placeholder={"Фамилия*"}
                        value={fieldsValues.lastName || ""}
                        onChange={v => onChangeField(fieldsKeys.lastName, v)}
                        error={errors.lastName}
                    />
                    <Input
                        placeholder={"Отчество"}
                        value={fieldsValues.middleName || ""}
                        onChange={v => onChangeField(fieldsKeys.middleName, v)}
                    />
                </Row>

                <MuiPickersUtilsProvider utils={MomentUtils} locale={"ru"}>
                    <DatePicker
                        style={{padding: "0.5rem"}}
                        variant="inline"
                        format={"Дата рождения DD.MM.yyyy"}
                        inputVariant="outlined"
                        animateYearScrolling
                        value={fieldsValues.dateOfBirth}
                        onChange={v => onChangeField(fieldsKeys.dateOfBirth, v)}
                    />
                </MuiPickersUtilsProvider>

                <GenderWrapper>
                    <b>Пол: </b>
                    <Button
                        selected={fieldsValues.gender === "male"}
                        borderColor={"#050558"}
                        onClick={() => onChangeField(fieldsKeys.gender, "male")}>
                        Мужской
                    </Button>
                    <Button
                        selected={fieldsValues.gender === "female"}
                        borderColor={"#050558"}
                        onClick={() => onChangeField(fieldsKeys.gender, "female")}>
                        Женский
                    </Button>
                    <Button
                        selected={fieldsValues.gender === "other"}
                        borderColor={"#050558"}
                        onClick={() => onChangeField(fieldsKeys.gender, "other")}>
                        Другой
                    </Button>
                </GenderWrapper>

                <Input
                    placeholder={"Место работы"}
                    value={fieldsValues.workPlace || ""}
                    onChange={v => onChangeField(fieldsKeys.workPlace, v)}
                />

                <Row>
                    <Input
                        placeholder={"Номер телефона +7 (xxx) xxx-xx-xx"}
                        value={fieldsValues.phoneNumber || ""}
                        type="tel"
                        formatOptions={"+7 (###) ###-##-##"}
                        onChange={v => onChangeField(fieldsKeys.phoneNumber, v)}
                        error={errors.phoneNumber}
                    />
                    <Input
                        placeholder={"E-mail*"}
                        value={fieldsValues.email || ""}
                        type={fieldsKeys.email}
                        onChange={v => onChangeField(fieldsKeys.email, v)}
                        error={errors.email}
                    />
                </Row>

                <Input
                    placeholder={"Пароль*"}
                    type={fieldsKeys.password || ""}
                    onChange={v => onChangeField(fieldsKeys.password, v)}
                    error={errors.password}
                />
                <Input
                    placeholder={"Подтвердите пароль*"}
                    type={fieldsKeys.password || ""}
                    onChange={v => onChangeField(fieldsKeys.confirmPassword, v)}
                />

                <Row>
                    <Button style={{width: "100%"}} disabled={waitResponse} borderColor={"#050558"} onClick={() => submitForm()}>
                        Сохранить
                    </Button>
                    <Button
                        style={{width: "100%"}}
                        disabled={waitResponse}
                        type={"danger"}
                        borderColor={"#ff1818"}
                        onClick={() => resetForm()}>
                        Отменить
                    </Button>
                </Row>
                {isSent && !waitResponse && (
                    <Alert severity={hasErrors ? "error" : "success"}>
                        {hasErrors ? "Проверьте введённые данные" : "Изменения успешно сохранены!"}
                    </Alert>
                )}

                <Row>
                    <Button type={"danger"} style={{width: "100%"}} borderColor={"#ff1818"} onClick={() => deleteProfile()}>
                        Удалить профиль
                    </Button>
                </Row>

                <Modal show={isOpenFollowersModal} onHide={() => closeFollowersModal()}>
                    <Modal.Header>
                        <Tabs currentTab={currentTab} tabs={followerModalTabs} changeTab={tab => this.changeTab(tab)} />
                    </Modal.Header>
                    <Modal.Body>{this.getUsersList()}</Modal.Body>
                    <Modal.Footer>
                        <Button onClick={() => closeFollowersModal()}>Закрыть</Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={showModal}>
                    <Modal.Header onHide={() => modalClose()} closeButton>
                        <Modal.Title>Подтверждение удаления профиля</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Вы действительно хотите удалить профиль?</Modal.Body>
                    <Modal.Footer>
                        <Button type={"danger"} onClick={modalSubmit}>
                            Удалить
                        </Button>
                        <Button onClick={() => modalClose()}>Отмена</Button>
                    </Modal.Footer>
                </Modal>
            </Form>
        );
    }
}

export default ProfileForm;
