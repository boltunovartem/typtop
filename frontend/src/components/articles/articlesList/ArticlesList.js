import React, {Component} from "react";

import Input from "components/common/form/Input";
import Paginator from "components/common/Paginator";
import ArticleItem from "./ArticleItem";
import {withContextConsumer, withContextProvider} from "utils/contexts";
import ArticlesContext from "contexts/Articles";
import TagsContext from "contexts/Tags";
import FilterItem, {Wrapper} from "components/common/filters/FilterItem";
import {throttle} from "lodash";
import SelectSearch from "react-select-search";
import {Row} from "components/articles/articleCreate/styled";
import Loading from "containers/contents/Loading";
import {withRouter} from "react-router-dom";
import {
    FiltersList,
    List,
    PaginatorWrapper,
    NoData,
    Wrapper as FiltersAndSort,
    ArticlesWithPagination
} from "components/articles/articlesList/styled";

const articleFilters = props => {
    const factors = [
        {
            id: "date",
            name: "Дата",
            elem: "popup"
        },
        {
            id: "rating",
            name: "Рейтинг",
            elem: "slider",
            min: props.slider.min,
            max: props.slider.max
        },
        {
            id: "tags",
            name: "Тэги",
            elem: "dropdown",
            values: props.tags
        }
    ];

    if (props.currentUser && !props.withoutFavorites) {
        factors.push({
            id: "favorite",
            name: "Избранные",
            elem: "checkBox"
        });
    }

    return factors;
};

const sortOptions = [
    {name: "По возрастанию даты", value: "dateAsc"},
    {name: "По убыванию даты", value: "dateDesc"},
    {name: "По возрастанию рейтинга", value: "rateAsc"},
    {name: "По убыванию рейтинга", value: "rateDesc"}
];

@withRouter
@withContextProvider(TagsContext.Provider)
@withContextConsumer(TagsContext.Consumer)
@withContextConsumer(ArticlesContext.Consumer)
class ArticlesList extends Component {
    constructor(props) {
        super(props);
        const {owner} = props;
        this.loadFoundArticles = throttle(this.loadFoundArticlesAtMoment, 400);
        this.state = {
            offset: 0,
            search: "",
            limit: 6,
            current: 1,
            filters: {
                owner: owner ? owner.id : null,
                date: null,
                tags: [],
                rating: null,
                favorite: false
            },
            sort: {publicationDate: -1}
        };
    }

    componentDidMount() {
        const {search, limit, offset, filters, sort} = this.state;
        const {getArticles, getTags} = this.props;
        getTags();
        getArticles(search, limit, offset, filters, sort);
    }

    componentDidUpdate(prevProps, prevStates) {
        if (this.state.current !== prevStates.current) {
            const {search, limit, offset, filters, sort} = this.state;
            const {getArticles} = this.props;
            getArticles(search, limit, offset, filters, sort);
        }
    }

    componentWillUnmount() {
        this.props.clearArticles();
    }

    loadFoundArticlesAtMoment(search) {
        const {getArticles} = this.props;
        const {limit, offset, filters, sort} = this.state;
        getArticles(search, limit, offset, filters, sort);
    }

    async setFilter(field, value) {
        const {filters, search} = this.state;
        await this.setState({
            filters: {
                ...filters,
                [field]: value
            }
        });
        this.loadFoundArticles(search);
    }

    async setSorting(sort) {
        const {search} = this.state;
        switch (sort) {
            case "dateAsc":
                await this.setState({sort: {publicationDate: 1}});
                break;
            case "dateDesc":
                await this.setState({sort: {publicationDate: -1}});
                break;
            case "rateAsc":
                await this.setState({sort: {rate: 1}});
                break;
            case "rateDesc":
                await this.setState({sort: {rate: -1}});
                break;
            default:
                break;
        }
        await this.loadFoundArticles(search);
    }

    render() {
        const {history, articles, onChangeArticleRate, currentUser, tags, total, withoutFavorites, articlesMeta} = this.props;
        const {limit, current, filters} = this.state;

        if (!articlesMeta) {
            return <Loading />;
        }

        return (
            <FiltersAndSort>
                <Row>
                    <Input
                        style={{marginBottom: 0}}
                        placeholder={"Введите для поиска"}
                        onChange={v => {
                            this.loadFoundArticles(v);
                            this.setState({search: v});
                        }}
                    />
                    <Wrapper width={"20rem"} height={"5vh"}>
                        <SelectSearch
                            options={sortOptions}
                            value={sortOptions[1].value}
                            onChange={value => this.setSorting(value)}
                            placeholder={"Сортировка"}
                            printOptions={"on-focus"}
                        />
                    </Wrapper>
                </Row>
                <FiltersList>
                    {articleFilters({
                        slider: {
                            min: articlesMeta.rate.min,
                            max: articlesMeta.rate.max
                        },
                        currentUser,
                        tags,
                        withoutFavorites
                    }).map(u => (
                        <FilterItem
                            key={u.id}
                            onChange={value => this.setFilter(u.id, value)}
                            filters={filters}
                            tags={tags}
                            {...u}
                        />
                    ))}
                </FiltersList>
                <ArticlesWithPagination>
                    <List>
                        {articles.map(article => {
                            return (
                                <ArticleItem
                                    key={article.id}
                                    fields={article}
                                    onChangeArticleRate={onChangeArticleRate}
                                    history={history}
                                />
                            );
                        })}
                        {total < 1 && <NoData>По данному запросу не нашлось статей</NoData>}
                    </List>
                    <PaginatorWrapper>
                        <Paginator
                            pageSize={limit}
                            current={current}
                            total={total}
                            onChange={pageNum => {
                                this.setState({current: pageNum, offset: (pageNum - 1) * limit});
                            }}
                        />
                    </PaginatorWrapper>
                </ArticlesWithPagination>
            </FiltersAndSort>
        );
    }
}

export default ArticlesList;
