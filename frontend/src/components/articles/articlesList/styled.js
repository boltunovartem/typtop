import styled from "styled-components";

const Wrapper = styled.div`
    width: 100%;
    padding: 0rem;
    position: relative;
    height: -webkit-fill-available;
    height: -moz-available;
    height: 100%;
`;

const Column = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
`;

const Row = styled.div`
    height: 4rem;
    display: flex;
    align-items: center;
    justify-content: space-between;
`;

const FiltersList = styled.div`
    position: relative;
    height: 4rem;

    display: flex;

    align-items: center;
    justify-content: space-between;
`;

const List = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 100%;
    height: 100%;
`;

const PaginatorWrapper = styled.div`
    height: 4rem;
`;

const NoData = styled.div`
    display: flex;
    width: 100%;
    justify-content: center;
    font-size: 20px;
    margin: auto 0;
    color: #aaa;
`;

const ArticlesWithPagination = styled.div`
    position: relative;
    height: calc(100% - 12rem);
`;

export {PaginatorWrapper, ArticlesWithPagination, FiltersList, NoData, Wrapper, List, Column, Row};
