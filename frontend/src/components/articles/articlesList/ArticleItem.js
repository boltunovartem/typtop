import React, {Component} from "react";
import styled from "styled-components";
import routes, {getRoute} from "consts/routes";
import {Tag} from "components/articles/articlePage/styled";
import {Row} from "../articleCreate/styled";

const ArticleItemWrapper = styled.div`
    display: block;
    width: calc(50% - 2rem);
    padding: 1rem;
    margin: 1rem;
    cursor: pointer;
    box-shadow: 0px 1px 4px 0px #999;
    transition: 0.5s;
    transition-timing-function: cubic-bezier;

    :hover {
        transform: scale(1.015);
        box-shadow: 0px 8px 12px -6px #999;
    }
`;

const ArticleTitle = styled.button`
    border: none;
    background-color: inherit;
    font-size: 1.5rem;
    font-weight: bold;
    padding: 0;
    margin: 0.5rem 0;
    outline: 0;
`;

const Author = styled.div`
    font-weight: bold;
    font-size: 1.5rem;
`;

const RateWrapper = styled.div`
    position: absolute;
    right: -1rem;
    top: -1rem;
    padding: 0.2rem 0.5rem;
    background: rgba(0, 0, 0, 0.04);
    border-radius: 0 0 0 5px;
`;

const Rate = styled.div`
    position: relative;
    font-size: 1.5rem;
    color: #898989;
    padding-left: 1rem;
    ${({rate}) => {
        if (!rate) return "padding-left: 0;";
        return rate > 0 ? "color: #0A0;" : "color: #A00;";
    }}

    :before {
        ${({rate}) => {
            if (!rate) return "content: unset;";
            return rate > 0 ? "content: '+';" : "content: '-';";
        }}
        position: absolute;
        left: 0;
        top: 50%;
        transform: translateY(-50%);
        font-size: 2rem;
    }
`;

const DateTime = styled.div`
    color: gray;
    font-size: 0.85rem;
`;

const TagsBlock = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 100%;
`;

const Description = styled.div``;

class ArticleItem extends Component {
    render() {
        const {fields, history} = this.props;

        return (
            <ArticleItemWrapper onClick={() => history.push(getRoute(routes.article, {id: fields.id}))}>
                <Row style={{justifyContent: "space-between"}}>
                    <Author>
                        {fields.owner.firstName} {fields.owner.lastName}
                    </Author>
                    <RateWrapper>
                        <Rate rate={fields.rate}>{Math.abs(fields.rate)}</Rate>
                    </RateWrapper>
                </Row>
                <DateTime>{new Date(fields.publicationDate).toLocaleDateString()}</DateTime>
                <ArticleTitle>{fields.title}</ArticleTitle>
                <Description>{fields.description}</Description>
                <TagsBlock>
                    {fields.tags.map(tag => (
                        <Tag key={tag.id}>#{tag.name}</Tag>
                    ))}
                </TagsBlock>
            </ArticleItemWrapper>
        );
    }
}

export default ArticleItem;
