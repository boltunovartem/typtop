import styled from "styled-components";
import {Title} from "components/common/form/Title";
import {mediaQuery} from "components/common/styled/mediaQuery";

const ArticlePageWrapper = styled.div`
    position: relative;
    width: 1024px;
    box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.5);
`;

const ArticleWrapper = styled.div`
    .ck.ck-editor__main > .ck-editor__editable:not(.ck-focused) {
        border: none;
        outline: none;
    }
    .ck.ck-editor__main > .ck-editor__editable:not(.ck-editor__nested-editable).ck-focused {
        border: none;
        box-shadow: none;
        outline: none;
    }
    .ck.ck-editor__main {
        border: none;
        outline: none;
    }
    .ck-focused {
        border: none;
        outline: none;
    }
    .ck.ck-toolbar {
        display: none;
    }
`;

const Block = styled.div`
    padding: 0.3rem;
    margin: 1rem;
`;

const Author = styled(Block)`
    display: flex;
    justify-content: space-between;
    align-items: baseline;
`;

const ArticleTitle = styled(Title)`
    text-align: center;
`;

const RateButton = styled.div`
    text-align: center;
    font-size: 20px;
    margin-left: 1rem;
    margin-right: 1rem;
    height: 2rem;
    width: 3rem;
    border: none;
`;

const RateWrapper = styled.div`
    position: absolute;
    left: -5rem;
    display: flex;
    align-items: center;
    flex-direction: column;
    ${mediaQuery.phoneForRating`
        position: fixed;
        left: 100%;
        top: 50%;
        transform: translateX(-100%) translateY(-50%);
        background: #ebebeb;
        border-radius: 5px 0 0 5px; 
        z-index: 10000;
        width:  3rem;
        box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.5);
    `}
`;

const TextLink = styled.div`
    display: flex;
    width: 10rem;
    align-items: center;
    font-size: 18px;
    color: #4444aa;
    margin: 0.3rem;
    padding: 0.6rem;
    cursor: pointer;
`;

const TagsAndDateBlock = styled.div`
    display: flex;
    flex-direction: column;
    background: #ebebeb;
    padding: 0.5rem 5rem;
`;

const TagsBlock = styled.div`
    display: flex;
`;

const Tag = styled.div`
    background: gray;
    color: white;
    width: fit-content;
    padding: 0.5rem;
    border-radius: 5px;
    margin: 0.5rem 0.3rem;
`;

const InputWrapper = styled.div`
    margin: 0.5rem;
`;

const ButtonWrapper = styled.div`
    display: flex;
    width: 100%;
    margin: -1rem 0rem -0.2rem 0rem;
    justify-content: center;
`;

export {
    TagsAndDateBlock,
    ArticlePageWrapper,
    ArticleWrapper,
    ArticleTitle,
    RateButton,
    RateWrapper,
    TagsBlock,
    TextLink,
    Author,
    Title,
    Block,
    Tag,
    InputWrapper,
    ButtonWrapper
};
