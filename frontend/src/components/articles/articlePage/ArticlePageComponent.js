import React, {Component, createRef} from "react";
import Button from "components/common/form/Button";
import {CKEditor} from "@ckeditor/ckeditor5-react";
import ClassicEditor from "ckeditor5-build-classic-base64-upload-adapter";
import routes, {getRoute} from "consts/routes";
import {
    ArticleTitle,
    ArticlePageWrapper,
    ArticleWrapper,
    Author,
    Block,
    RateButton,
    RateWrapper,
    Tag,
    TagsAndDateBlock,
    TagsBlock,
    TextLink,
    InputWrapper,
    ButtonWrapper
} from "components/articles/articlePage/styled";
import {CommentsList} from "components/comments/CommentsList";
import Input from "components/common/form/Input";
import Arrow from "components/common/icons/Arrow";

class ArticlePageComponent extends Component {
    constructor(props) {
        super(props);

        this.createCommentRef = createRef();
        this.createCommentInputRef = createRef();
    }

    getAuthorLink = owner => {
        const text = `${owner.firstName} ${owner.lastName}`;
        return <TextLink onClick={() => this.props.history.push(getRoute(routes.profileById, {id: owner.id}))}>{text}</TextLink>;
    };

    renderTag = tag => {
        return <Tag key={tag.id}>#{tag.name}</Tag>;
    };

    onClickCommentAuthor = author => {
        this.props.history.push(getRoute(routes.profileById, {id: author.id}));
    };

    onClickEditComment = comment => {
        this.createCommentInputRef.getInnerRef().current.focus();
        this.createCommentRef.current.scrollIntoView({behavior: "smooth", block: "center"});

        const {onStartEditComment} = this.props;
        onStartEditComment(comment);
    };

    render() {
        const {
            currentUser,
            currentArticle,
            onChangeArticleRate,
            history,
            commentText,
            onChangeComment,
            onComment,
            errors,
            comments,
            commentsOffset,
            commentsLimit,
            currentCommentsPage,
            totalComments,
            onClickDeleteComment,
            isEditing,
            onCancelEditComment,
            onSubmitEditComment,
            onChangeCommentsPage
        } = this.props;
        const {title, content, rate, tags, owner, publicationDate, currentUserRate} = currentArticle;
        const isUserLogged = currentUser;
        return (
            <ArticlePageWrapper>
                <ArticleWrapper>
                    <RateWrapper>
                        {isUserLogged && (
                            <RateButton onClick={() => onChangeArticleRate(true)}>
                                <Arrow width={25} currentUserRate={currentUserRate} isUpArrow />
                            </RateButton>
                        )}
                        {rate}
                        {isUserLogged && (
                            <RateButton onClick={() => onChangeArticleRate(false)}>
                                <Arrow width={25} currentUserRate={currentUserRate} transform={"rotate(180)"} />
                            </RateButton>
                        )}
                    </RateWrapper>
                    <Author>
                        {this.getAuthorLink(owner)}
                        <ArticleTitle>{title}</ArticleTitle>
                        {currentUser && currentUser.id === owner.id ? (
                            <Button
                                borderColor={"#050558"}
                                style={{width: "10rem"}}
                                onClick={() =>
                                    history.push(
                                        getRoute(routes.updateArticle, {
                                            id: currentArticle.id
                                        })
                                    )
                                }>
                                Изменить
                            </Button>
                        ) : (
                            <div style={{width: "10rem"}} />
                        )}
                    </Author>
                    <Block>
                        <CKEditor
                            editor={ClassicEditor}
                            data={content}
                            onReady={editor => {
                                if (!editor) return;
                                editor.isReadOnly = true;
                                editor.editing.view.change(writer => {
                                    writer.setStyle("margin-bottom", "0.5rem", editor.editing.view.document.getRoot());
                                });
                            }}
                        />
                    </Block>

                    <TagsAndDateBlock>
                        <div style={{alignSelf: "center", fontSize: 18}}>
                            Дата публикации: {new Date(publicationDate).toLocaleDateString()}
                        </div>
                        <TagsBlock>{tags.map(tag => this.renderTag(tag))}</TagsBlock>
                    </TagsAndDateBlock>
                </ArticleWrapper>

                <InputWrapper ref={this.createCommentRef}>
                    <Input
                        placeholder={"Введите комментарий"}
                        value={commentText}
                        onChange={v => onChangeComment(v)}
                        error={errors.text}
                        ref={r => (this.createCommentInputRef = r)}
                        multiline
                    />
                </InputWrapper>
                {!isEditing && (
                    <ButtonWrapper>
                        <Button borderColor={"#050558"} onClick={() => onComment()}>
                            Отправить
                        </Button>
                    </ButtonWrapper>
                )}
                {isEditing && (
                    <ButtonWrapper>
                        <Button borderColor={"#ff1818"} type={"danger"} onClick={() => onCancelEditComment()}>
                            Отменить
                        </Button>
                        <Button borderColor={"#050558"} onClick={() => onSubmitEditComment()}>
                            Сохранить
                        </Button>
                    </ButtonWrapper>
                )}

                <CommentsList
                    comments={comments}
                    total={totalComments}
                    onClickAuthor={this.onClickCommentAuthor.bind(this)}
                    onClickDeleteComment={onClickDeleteComment}
                    onClickEditComment={this.onClickEditComment.bind(this)}
                    onChangePage={onChangeCommentsPage}
                    currentUser={currentUser}
                    offset={commentsOffset}
                    limit={commentsLimit}
                    current={currentCommentsPage}
                />
            </ArticlePageWrapper>
        );
    }
}

export default ArticlePageComponent;
