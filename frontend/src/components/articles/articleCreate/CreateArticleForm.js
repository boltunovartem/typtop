import Input from "components/common/form/Input";
import {CKEditor} from "@ckeditor/ckeditor5-react";
import ClassicEditor from "ckeditor5-build-classic-base64-upload-adapter";
import Chips from "react-chips";
import Button from "components/common/form/Button";
import React, {Component} from "react";
import {ChipsWrapper, CreateArticleWrapper, Error, Row} from "components/articles/articleCreate/styled";
import Modal from "react-bootstrap/Modal";

class CreateArticleForm extends Component {
    render() {
        const {
            title,
            currentArticle,
            description,
            tags,
            onChange,
            fieldsKeys,
            errors,
            submitArticle,
            isLoading,
            history,
            suggestions,
            showModal,
            modalSubmit,
            modalClose
        } = this.props;

        return (
            <CreateArticleWrapper>
                <Input
                    placeholder={"Название статьи"}
                    inputStyle={{backgroundColor: "#FFF", fontSize: 20, fontWeight: "bold", textAlign: "center"}}
                    onChange={title => onChange(fieldsKeys.title, title)}
                    error={errors.title}
                    value={title}
                />
                <Input
                    placeholder={"Краткое описание (До 200 символов)"}
                    inputStyle={{backgroundColor: "#FFF", fontSize: 18, textAlign: "center", resize: "none"}}
                    onChange={description => onChange(fieldsKeys.description, description)}
                    value={description}
                    maxLength={200}
                    error={errors.description}
                    multiline
                />
                <CKEditor
                    className={"ckeditor-elem"}
                    title={"Статья"}
                    editor={ClassicEditor}
                    data={(currentArticle && currentArticle.content) || ""}
                    config={{
                        language: "ru",
                        placeholder: "Содержимое статьи"
                    }}
                    onReady={editor => {
                        if (!editor) return;
                        editor.editing.view.change(writer => {
                            writer.setStyle("min-height", "400px", editor.editing.view.document.getRoot());
                            writer.setStyle("margin-bottom", "0.5rem", editor.editing.view.document.getRoot());
                        });
                    }}
                    onChange={(e, editor) => onChange(fieldsKeys.content, editor.getData())}
                />
                <Error>{errors.content}</Error>
                <ChipsWrapper>
                    <Chips
                        placeholder={"Укажите тэги"}
                        value={tags}
                        onChange={tags => onChange(fieldsKeys.tags, tags)}
                        suggestions={suggestions}
                    />
                </ChipsWrapper>
                {errors.tags && <Error>{errors.tags}</Error>}
                <Row>
                    <Button borderColor={"#050558"} onClick={submitArticle} disabled={isLoading}>
                        Сохранить
                    </Button>
                    <Button borderColor={"#ff1818"} type={"danger"} onClick={() => history.goBack()}>
                        Отмена
                    </Button>
                </Row>
                <Modal show={showModal}>
                    <Modal.Header onHide={() => modalClose()} closeButton>
                        <Modal.Title>Подтверждение удаления статьи</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Вы действительно хотите удалить статью?</Modal.Body>
                    <Modal.Footer>
                        <Button type={"danger"} onClick={modalSubmit}>
                            Удалить
                        </Button>
                        <Button onClick={() => modalClose()}>Отмена</Button>
                    </Modal.Footer>
                </Modal>
            </CreateArticleWrapper>
        );
    }
}

export default CreateArticleForm;
