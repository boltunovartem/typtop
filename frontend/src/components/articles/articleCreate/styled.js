import styled from "styled-components";
import {mediaQuery} from "components/common/styled/mediaQuery";

const CreateArticleWrapper = styled.div`
    width: 100%;
    .ck.ck-editor__editable:not(.ck-editor__nested-editable).ck-focused {
        border-color: #050558;
    }
`;

const Row = styled.div`
    position: relative;
    display: flex;
    width: 100%;
    justify-content: space-around;

    ${mediaQuery.phone`
        flex-direction: ${p => (p.onlyRow ? "row" : "column")};
    `}
`;

const ChipsWrapper = styled.div`
    width: 100%;

    ul {
        max-height: 10rem;
        overflow-x: hidden;
        overflow-y: auto;
    }
`;

const Error = styled.div`
    padding: 0 1rem;
    color: #c00;
    font-size: 0.9rem;
    margin-bottom: 1rem;
`;

export {Error, CreateArticleWrapper, Row, ChipsWrapper};
