import styled from "styled-components";

const List = styled.div`
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    width: 100%;
`;

const CommentItemWrapper = styled.div`
    display: block;
    width: 100% - 2rem;
    padding: 1rem;
    margin: 1rem;
    border-radius: 0.6rem;
    box-shadow: 0px 1px 4px 0px #999;
`;

const Row = styled.div`
    display: flex;
    width: 100%;
    justify-content: flex-start;
`;

const Author = styled.div`
    font-weight: bold;
    font-size: 1rem;
    cursor: pointer;
`;

const EmptySpace = styled.div`
    flex-grow: 2;
`;

const Text = styled.div`
    width: inherit;
    padding: 0.5rem 0rem;
    word-break: break-word;
    overflow-break: break-word;
    white-space: pre-wrap;
`;

const DateTime = styled.div`
    color: gray;
    font-size: 0.85rem;
`;

const IconWrapper = styled.div`
    cursor: pointer;
    margin: 0rem 0.4rem;
`;

export {List, CommentItemWrapper, Row, Author, Text, DateTime, IconWrapper, EmptySpace};
