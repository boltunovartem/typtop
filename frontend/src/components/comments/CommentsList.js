import React, {Component} from "react";
import {List} from "./styled";
import {CommentItem} from "./CommentItem";
import Paginator from "../common/Paginator";

class CommentsList extends Component {
    render() {
        const {
            comments,
            total,
            onClickAuthor,
            onClickDeleteComment,
            onClickEditComment,
            currentUser,
            onChangePage,
            limit,
            current
        } = this.props;

        return (
            <div>
                <List>
                    {comments.map(comment => {
                        return (
                            <CommentItem
                                key={comment.id}
                                fields={comment}
                                onClickAuthor={onClickAuthor}
                                onClickDeleteComment={onClickDeleteComment}
                                onClickEditComment={onClickEditComment}
                                showManageIcons={currentUser ? comment.owner.id === currentUser.id : false}
                            />
                        );
                    })}
                </List>
                <Paginator
                    pageSize={limit}
                    current={current}
                    total={total}
                    onChange={pageNum => {
                        onChangePage(pageNum);
                    }}
                />
            </div>
        );
    }
}

export {CommentsList};
