import React, {Component} from "react";
import {Author, CommentItemWrapper, DateTime, EmptySpace, IconWrapper, Row, Text} from "./styled";
import moment from "moment";
import Close from "components/common/icons/Close";
import Edit from "components/common/icons/Edit";

class CommentItem extends Component {
    formatDate = date => {
        return moment(date).format("DD.MM.YYYY HH:mm");
    };

    render() {
        const {fields, onClickAuthor, onClickDeleteComment, onClickEditComment, showManageIcons} = this.props;

        return (
            <CommentItemWrapper>
                <Row>
                    <Author
                        onClick={() => {
                            onClickAuthor(fields.owner);
                        }}>
                        {fields.owner.firstName} {fields.owner.lastName}
                    </Author>
                    <EmptySpace />

                    {showManageIcons && (
                        <IconWrapper>
                            <Edit
                                width={"1rem"}
                                height={"1rem"}
                                onClick={() => {
                                    onClickEditComment(fields);
                                }}
                            />
                        </IconWrapper>
                    )}
                    {showManageIcons && (
                        <IconWrapper>
                            <Close
                                width={"1rem"}
                                height={"1rem"}
                                onClick={() => {
                                    onClickDeleteComment(fields);
                                }}
                            />
                        </IconWrapper>
                    )}
                </Row>
                <Text>{fields.text}</Text>
                <DateTime>{this.formatDate(fields.publicationDate)}</DateTime>
            </CommentItemWrapper>
        );
    }
}

export {CommentItem};
