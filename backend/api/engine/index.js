const https = require("https");
const pem = require("pem");

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const express = require("express");
const Router = require("../router");

class Engine {
    constructor() {
        this.db = null;
        this.router = null;
        this.app = express();
    }

    async start() {
        await this.createRouter();
        this.runServer();
        return this.app;
    }

    async connectMongoDB() {
        try {
            await mongoose.connect(config.mongoUrl, {
                useNewUrlParser: true,
                family: 4,
                connectTimeoutMS: 5000,
                autoReconnect: false,
                useUnifiedTopology: true,
                useCreateIndex: true
            });
            this.db = mongoose.connection;
        } catch (e) {
            console.log(e);
            process.exit(0);
        }
    }

    async createRouter() {
        this.router = new Router(this.app);
        await this.router.configure();
    }

    runServer() {
        if (process.env.NODE_ENV === "test") {
            return;
        }
        if (config.withHttps) {
            pem.createCertificate({days: 1, selfSigned: true}, (err, keys) => {
                if (err) return console.log(err);
                https.createServer({key: keys.serviceKey, cert: keys.certificate}, this.app).listen(config.port);
            });
            return;
        }
        this.app.listen(config.port, () => console.log(`Start server on port ${config.port}`));
    }
}

module.exports = Engine;
