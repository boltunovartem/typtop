const RealEngine = require("../../engine");

describe("Test Case Engine", () => {
    test("Check create engine", async () => {
        function app() {
            return {
                use: jest.fn(),
                listen: jest.fn()
            }
        }
        jest.doMock('express', () => {
            return () => {
                return app();
            }
        })
        class Router {
            constructor(app) {
                this.app = app;
            }
        }
        class Engine {
            constructor() {
                this.app = app;
                this.db = null;
                this.router = new Router(this.app);
            }
        }

        const engine = new RealEngine();
        await engine.start();
        const testEngine = JSON.stringify(new Engine());

        expect(JSON.stringify(engine)).toEqual(testEngine);
    });
})