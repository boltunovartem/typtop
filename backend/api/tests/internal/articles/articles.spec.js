const {
    createArticle,
    updateArticle,
    deleteArticle,
    getArticleById,
    getArticles,
    getMeta,
    getCountOfArticles
} = require("../../../internal/articles/usecases");
const ArticleModel = require("../../../internal/articles/article.js");
const ratingsUsecases = require("../../../internal/ratings/usecases");
const commentsUsecases = require("../../../internal/comments/usecases");
const {ERROR_INVALID_USER} = require("../../../internal/articles/usecases");
const {ERROR_ARTICLE_NOT_FOUND} = require("../../../internal/articles/usecases");

const article1 = {
    _id: 1,
    content: "Тело статьи",
    description: "Описание статьи",
    tags: ["тег1", "тег2"],
    title: "Название статьи",
    owner: {
        _id: 1
    },
    rate: 2
};

const article2 = {
    _id: 2,
    content: "Тело статьи1",
    description: "Описание статьи1",
    tags: ["тег1", "тег2"],
    title: "Название статьи1",
    owner: {
        _id: 1
    },
    rate: 5
};

const ArticleMockMin = {
    find: jest.fn(() => ArticleMockMin),
    sort: jest.fn(() => ArticleMockMin),
    limit: jest.fn(() => [article1, article2])
};

const ArticleMockMax = {
    find: jest.fn(() => ArticleMockMax),
    sort: jest.fn(() => ArticleMockMax),
    limit: jest.fn(() => [article2, article1])
};

const ArticleMock = {
    find: jest.fn(() => ArticleMock),
    sort: jest.fn(() => ArticleMock),
    limit: jest.fn(() => [article1, article2]),
    skip: jest.fn(() => ArticleMock)
};

const ArticleMockUndef = {
    find: jest.fn(() => ArticleMockUndef),
    sort: jest.fn(() => ArticleMockUndef),
    limit: jest.fn(() => [])
};

describe("Articles", () => {
    // negative
    test("Check delete article by invalid id from db", async () => {
        const spyArticle = jest.spyOn(ArticleModel, "findById");
        spyArticle.mockReturnValue(undefined);

        try {
            await deleteArticle(1, {_id: 1});
        } catch (e) {
            expect(e).toEqual(ERROR_ARTICLE_NOT_FOUND);
        }
    });

    test("Check delete article with invalid user in db", async () => {
        const spyArticle = jest.spyOn(ArticleModel, "findById");
        spyArticle.mockReturnValue(article1);
        try {
            await deleteArticle(1, {_id: {equals: () => false}});
        } catch (e) {
            expect(e).toEqual(ERROR_INVALID_USER);
        }
    });

    // positive
    test("Check create article in db", async () => {
        const spyArticle = jest.spyOn(ArticleModel, "create");
        spyArticle.mockImplementation(() => {});
        await createArticle({}, {});
        expect(ArticleModel.create.mock.calls.length).toBe(1);
    });

    test("Check update article in db", async () => {
        const spyArticle = jest.spyOn(ArticleModel, "findByIdAndUpdate");
        spyArticle.mockImplementation(() => {});
        await updateArticle(1, {}, {});
        expect(ArticleModel.findByIdAndUpdate.mock.calls.length).toBe(1);
    });

    test("Check delete article in db", async () => {
        const spyArticle = jest.spyOn(ArticleModel, "findById");
        spyArticle.mockReturnValue(article1);
        const spyComments = jest.spyOn(commentsUsecases, "deleteAllArticleComments");
        spyComments.mockImplementation(() => {});
        const spyRates = jest.spyOn(ratingsUsecases, "deleteAllArticleRates");
        spyRates.mockImplementation(() => {});

        article1.delete = () => {};
        await deleteArticle(1, {_id: {equals: () => true}});

        expect(commentsUsecases.deleteAllArticleComments.mock.calls.length).toBe(1);
        expect(ratingsUsecases.deleteAllArticleRates.mock.calls.length).toBe(1);
    });

    test("Check get article by id from db", async () => {
        const spyArticle = jest.spyOn(ArticleModel, "findOne");
        spyArticle.mockImplementation(() => article1);
        const res = await getArticleById(1);
        expect(ArticleModel.findOne.mock.calls.length).toBe(1);
        expect(res).toEqual(article1);
    });

    test("Check get articles from db", async () => {
        const curUser = 1;
        const filters = {
            owner: curUser,
            date: {startDate: Date.now(), endDate: Date.now()},
            rating: 2
        };
        const search = "12345";
        const sort = {};
        const spyCountArticle = jest.spyOn(ArticleModel, "countDocuments");
        spyCountArticle.mockReturnValue(2);

        const spyArticle = jest.spyOn(ArticleModel, "find");
        spyArticle.mockReturnValue(ArticleMock);

        const res = await getArticles(curUser, search, 0, 2, filters, sort);

        expect(ArticleModel.countDocuments.mock.calls.length).toBe(1);
        expect(ArticleModel.find.mock.calls.length).toBe(1);
        expect(res).toEqual({articles: [article1, article2], total: 2});
        spyArticle.mockReset();
    });

    test("Check get meta from db", async () => {
        const spyArticle = jest.spyOn(ArticleModel, "find");
        spyArticle.mockReturnValueOnce(ArticleMockMin);
        spyArticle.mockReturnValueOnce(ArticleMockMax);
        const res = await getMeta();
        expect(ArticleModel.find.mock.calls.length).toBe(2);
        expect(res).toEqual({rate: {min: 2, max: 5}});
        spyArticle.mockReset();
    });

    test("Check get meta from db empty", async () => {
        const spyArticle = jest.spyOn(ArticleModel, "find");
        spyArticle.mockReturnValue(ArticleMockUndef);
        const res = await getMeta();
        expect(ArticleModel.find.mock.calls.length).toBe(2);
        expect(res).toEqual({rate: {min: 0, max: 0}});
    });
});
