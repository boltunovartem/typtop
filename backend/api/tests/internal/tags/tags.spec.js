const {getTags} = require("../../../internal/tags/usecases");
const TagModel = require("../../../internal/tags/tag.js");
const mongoose = require("mongoose");

const connectDB = async () => {
    await mongoose.connect(config.mongoUrl, {
        useNewUrlParser: true,
        family: 4,
        connectTimeoutMS: 5000,
        autoReconnect: false,
        useUnifiedTopology: true,
        useCreateIndex: true
    });
};

const tag = {
    id: 1,
    name: "tag1"
};

const tagToDTO = {
    name: "tag1"
};

const tags = [
    {
        id: 1,
        name: "tag1"
    },
    {
        id: 2,
        name: "tag2"
    }
];


describe("Tags", () => {
    let connection, tagToDtoModel;

    beforeAll(async () => {
        await connectDB();
        connection = mongoose.connection;
        tagToDtoModel = await TagModel.create(tagToDTO);
    });

    test("Check Get Tags", async () => {
        const spyTags = jest.spyOn(TagModel, "find");
        spyTags.mockImplementation(() => tags);
        const res = await getTags("tag1");

        expect(res).toContainEqual(tag);
    });

    test("Check tag to DTO", () => {
        const res = tagToDtoModel.toDTO();
        expect(res).toEqual({...tagToDTO, id: tagToDtoModel._id});
    });

    afterAll(async () => {
        await tagToDtoModel.delete();
        await connection.close();
    })
});
