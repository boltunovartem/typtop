const {firstLetterUppercase} = require("../../../internal/utils/strings")

describe("Test utils for strings", () => {
    test("firstLetterUppercase correct test", () => {
        const string = "hello";
        expect(firstLetterUppercase(string)).toEqual("Hello");
    })

    test("firstLetterUppercase negative test", () => {
        const string = null;
        expect(firstLetterUppercase(string)).toEqual(null);
    })
})