const {validateStruct} = require("../../../internal/utils/validator");

const requiredErrorMessage = "Введите название";
const minErrorMessage = "Название должно быть длиннее 2 символов";
const maxErrorMessage = "Название должно быть короче 50 символов";
const fieldValue = "Какое-то значение";

const body = {field: fieldValue};
const struct = {
    field: {
        required: {message: requiredErrorMessage},
        min: {value: 2, message: minErrorMessage},
        max: {value: 50, message: maxErrorMessage}
    }
};

describe("Validator", () => {
    test("Should validate correct body by fields", () => {
        expect(validateStruct(body, struct)).toEqual({errors: {}, hasErrors: false});
    });

    test("Should not validate body without struct", () => {
        expect(validateStruct(body, null)).toEqual({errors: {internal: "Invalid body"}, hasErrors: true});
    });

    test("Should validate null body", () => {
        expect(validateStruct(null, struct)).toEqual({errors: {field: requiredErrorMessage}, hasErrors: true});
    });

    test("Should not validate with invalid validation rules", () => {
        const body = {field: "Какое-то значение"};
        const invalidStruct = {
            field: {
                wrongRule: {message: "Unexpected error"}
            }
        };

        expect(validateStruct(body, invalidStruct)).toEqual({errors: {}, hasErrors: false});
    });

    test("Should validate empty body", () => {
        const body = {
            field: "njkn"
        };
        const struct = {
            field: {
                omitempty: "Необходимо, чтобы поле было пустое"
            }
        };

        expect(validateStruct(body, struct)).toEqual({errors: {}, hasErrors: false});
    });

    test("Should validate email", () => {
        const body = {
            email: "email@mail.com"
        };
        const struct = {
            email: {
                email: {message: "Требуется ввести электронную почту"}
            }
        };

        expect(validateStruct(body, struct)).toEqual({errors: {}, hasErrors: false});
    });

    test("Should not validate empty email", () => {
        const body = {
            email: ""
        };
        const struct = {
            email: {
                email: {message: "Требуется ввести электронную почту"}
            }
        };

        expect(validateStruct(body, struct)).toEqual({errors: {}, hasErrors: false});
    });

    test("Should not validate non-array fields if it's required", () => {
        const body = {
            field: "Какое-то значение"
        };
        const struct = {
            field: {
                isArray: {message: "Field is not array"}
            }
        };

        expect(validateStruct(body, struct)).toEqual({errors: {field: "Field is not array"}, hasErrors: true});
    });

    test("Should validate array fields if it's required", () => {
        const body = {
            field: ["1", "2"]
        };
        const struct = {
            field: {
                isArray: {message: "Field is not array"},
                arrayLengthMax: {max: 2, message: "Array does not match max array length"}
            }
        };

        expect(validateStruct(body, struct)).toEqual({errors: {}, hasErrors: false});
    });

    test("Should not validate array fields if its length is greater than max length", () => {
        const body = {
            field: ["1", "2", "3", "4"]
        };
        const struct = {
            field: {
                isArray: {message: "Field is not array"},
                arrayLengthMax: {max: 3, message: "Array does not match max array length"}
            }
        };

        expect(validateStruct(body, struct)).toEqual({
            errors: {field: "Array does not match max array length"},
            hasErrors: true
        });
    });
});
