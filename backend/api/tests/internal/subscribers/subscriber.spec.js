const SubscriberModel = require("../../../internal/subscribers/subsriber");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

const connectDB = async () => {
    await mongoose.connect(config.mongoUrl, {
        useNewUrlParser: true,
        family: 4,
        connectTimeoutMS: 5000,
        autoReconnect: false,
        useUnifiedTopology: true,
        useCreateIndex: true
    });
};

describe("Test subsciber model", () => {
    let connection;
    let subscription;
    const data = {
        owner: ObjectId(),
        subscribedTo: ObjectId()
    }

    beforeAll(async () => {
        await connectDB();
        connection = mongoose.connection;
        subscription = await SubscriberModel.create(data)
        subscription.populate = () => ({
            execPopulate: () => {}
        });
    });

    test("test DTO", async () => {
        const dto = subscription.toDTO();

        expect(dto).toEqual(data);
    });

    test("test toFollowingDTO", async () => {
        const toNameDTO = () => ({
            firstName: "Artem",
            lastName: "Boltunov"
        });

        subscription.subscribedTo.toNameDTO = toNameDTO;
        const dto = await subscription.toFollowingDTO();

        expect(dto).toEqual(toNameDTO());
    });

    test("test toFollowersDTO", async () => {
        const toNameDTO = () => ({
            firstName: "Artem",
            lastName: "Boltunov"
        });

        subscription.owner.toNameDTO = toNameDTO;
        const dto = await subscription.toFollowersDTO();

        expect(dto).toEqual(toNameDTO());
    });

    afterAll(async () => {
        await subscription.delete();
        await connection.close();
    })
})