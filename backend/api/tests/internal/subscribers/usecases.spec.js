const subscribersUsecases = require("../../../internal/subscribers/usecases");
const SubscriberModel = require("../../../internal/subscribers/subsriber");

describe("Test subscibers usecases", () => {
    test("getSubscription", async () => {
        const testSubscription = {
            owner: 2,
            subscribedTo: 1
        };

        const subscribedTo = 1, owner = 2;
        const spy = jest.spyOn(SubscriberModel, "findOne");
        spy.mockReturnValue(testSubscription);

        const subscription = await subscribersUsecases.getSubscription(subscribedTo, owner)
        expect(subscription).toEqual(testSubscription);
    })

    test("getFavorites", async () => {
        const testSubscriptions = [
            {
                owner: 2,
                subscribedTo: 1
            },
            {
                owner: 2,
                subscribedTo: 3
            },
            {
                owner: 2,
                subscribedTo: 4
            },
        ];

        const owner = 2;
        const spy = jest.spyOn(SubscriberModel, "find");
        spy.mockReturnValue(testSubscriptions);

        const subscription = await subscribersUsecases.getFavorites(owner)
        expect(subscription).toEqual(testSubscriptions);
    })
})