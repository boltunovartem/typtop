const ratingsUsecases = require("../../../internal/ratings/usecases");
const RatingModel = require("../../../internal/ratings/rating");

describe("Test ratings usecase", () => {

    test("Test change rating to positive", async () => {
        const article = {
            _id: 1,
            rating: 10
        }
        const rating = {
            article: 1,
            owner: 1,
            positive: true
        };
        const owner = {
            _id: 1
        }
        const isPositive = true;

        const spyCurrentRating = jest.spyOn(RatingModel, "findOne");
        spyCurrentRating.mockReturnValue(rating);
        const spyDeleteRating = jest.spyOn(RatingModel, "deleteOne");
        spyDeleteRating.mockReturnThis();
        const spyUpdateRating = jest.spyOn(RatingModel, "updateOne");
        spyUpdateRating.mockReturnThis();
        const spyCreateRating = jest.spyOn(RatingModel, "create");
        spyCreateRating.mockReturnThis();
        const spyCountPositive = jest.spyOn(RatingModel, "countDocuments");
        spyCountPositive.mockImplementation((data) => {
            return data.positive ? 19 : 10
        })

        const count = await ratingsUsecases.changeRating(article, owner, isPositive);
        expect(count).toEqual(article.rating - 1);
    })

});