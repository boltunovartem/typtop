const config = require("../config");
const HttpStatus = require("http-status-codes");

global.config = config;
global.HttpStatus = HttpStatus;

global.console = {
    log: jest.fn(),
    error: jest.fn(),
    warn: jest.fn(),
    trace: jest.fn()
};

global.ResponseClass = class {
    constructor() {
        this.statusCode = null;
        this.content = null;
        this.currentCookie = {};
    }

    send(data) {
        this.content = data;
        return this;
    }

    json(data) {
        return this.send(JSON.stringify(data));
    }

    status(code) {
        this.statusCode = code;
        return this;
    }

    cookie(key, value, expTime) {
        this.currentCookie[key] = {value, expTime};
        return this;
    }

    clearCookie(key) {
        delete this.currentCookie[key];
        return this;
    }
};
