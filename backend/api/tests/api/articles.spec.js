const {createArticle, updateArticle, deleteArticle, getArticleById, getArticles, changeRating} = require("../../api/articles");
const mongoose = require("mongoose");
const userUsecases = require("../../internal/users/usecases");
const tagsUsecases = require("../../internal/tags/usecases");
const articleUsecases = require("../../internal/articles/usecases");
const ratingUsecases = require("../../internal/ratings/usecases");

const connectDB = async () => {
    await mongoose.connect(config.mongoUrl, {
        id: 1,
        useNewUrlParser: true,
        family: 4,
        connectTimeoutMS: 5000,
        autoReconnect: false,
        useUnifiedTopology: true,
        useCreateIndex: true
    });
};

const article = {
    _id: 1,
    content: "Тело статьи",
    description: "Описание статьи",
    tags: ["тег1", "тег2"],
    title: "Название статьи"
};

const user = {
    _id: 1,
    firstName: "Artem",
    lastName: "Boltunov",
    userName: "blotunovartem",
    phoneNumber: "+7 (929) 821-40-30",
    email: "tema2000@gmail.com",
    password: "password"
};

const tags = [
    {
        _id: 1,
        name: "tag1"
    },
    {
        _id: 2,
        name: "tag2"
    }
];

const rating = {
    _id: 1,
    article: 1,
    owner: 1,
    positive: true
};

const articleDto = {
    id: article.id,
    owner: user.id,
    title: article.title,
    description: article.description,
    content: article.content,
    tags: tags.map(tag => tag.id),
    publicationDate: Date.now(),
    rate: rating.id
};

describe("Articles", () => {
    let connection;

    beforeAll(async () => {
        await connectDB();
        connection = mongoose.connection;
    });

    // negative
    // create
    test("Check create article with invalid data", async () => {
        const invalidArticleData = {};

        const res = new ResponseClass();
        const req = {
            body: invalidArticleData
        };
        const next = () => {};

        await createArticle(req, res, next);

        expect(res.statusCode).toEqual(400);
    });

    test("Check create article with invalid user", async () => {
        const res = new ResponseClass();
        const req = {
            body: article,
            currentUser: 1
        };
        const next = () => {};

        await createArticle(req, res, next);

        expect(res.statusCode).toEqual(500);
    });

    test("Check create article with invalid tags", async () => {
        const res = new ResponseClass();
        const req = {
            body: article
        };
        const next = () => {};

        const spy = jest.spyOn(userUsecases, "getUserById");
        spy.mockReturnValue(user);

        await createArticle(req, res, next);

        expect(res.statusCode).toEqual(500);
    });

    // update
    test("Check update article with invalid data", async () => {
        const invalidArticleData = {};

        const res = new ResponseClass();
        const req = {
            body: invalidArticleData
        };
        const next = () => {};

        await updateArticle(req, res, next);

        expect(res.statusCode).toEqual(400);
    });

    test("Check update article with invalid user", async () => {
        const res = new ResponseClass();
        const req = {
            body: article,
            currentUser: 1
        };
        const next = () => {};

        await updateArticle(req, res, next);

        expect(res.statusCode).toEqual(500);
    });

    test("Check update article with invalid tags", async () => {
        const res = new ResponseClass();
        const req = {
            body: article
        };
        const next = () => {};

        const spy = jest.spyOn(userUsecases, "getUserById");
        spy.mockReturnValue(user);

        await updateArticle(req, res, next);

        expect(res.statusCode).toEqual(500);
    });

    // delete
    test("Check delete article with invalid id", async () => {
        const res = new ResponseClass();
        const req = {
            params: 1
        };
        const next = () => {};

        await deleteArticle(req, res, next);

        expect(res.statusCode).toEqual(404);
    });

    test("Check delete article by not owner", async () => {
        const res = new ResponseClass();
        const req = {
            params: 1,
            currentUser: 1
        };
        const next = () => {};

        const spy = jest.spyOn(articleUsecases, "deleteArticle");
        spy.mockImplementation(() => {
            throw articleUsecases.ERROR_INVALID_USER;
        });
        await deleteArticle(req, res, next);

        expect(res.statusCode).toEqual(409);
    });

    test("Check delete article with unexpected internal error ", async () => {
        const res = new ResponseClass();
        const req = {
            params: 1,
            currentUser: 1
        };
        const next = () => {};

        const spy = jest.spyOn(articleUsecases, "deleteArticle");
        spy.mockImplementation(() => {
            throw new Error("Ooops!");
        });
        await deleteArticle(req, res, next);

        expect(res.statusCode).toEqual(500);
    });

    // getById
    test("Check get article by invalid id", async () => {
        const res = new ResponseClass();
        const req = {
            params: 1,
            cookies: {
                token: "12345667890"
            }
        };
        const next = () => {};

        const spyUser = jest.spyOn(userUsecases, "getUserByToken");
        spyUser.mockReturnValue(user);

        await getArticleById(req, res, next);

        expect(res.statusCode).toEqual(404);
    });

    test("Check get article by id with internal error", async () => {
        const res = new ResponseClass();
        const req = {};
        const next = () => {};

        await getArticleById(req, res, next);

        expect(res.statusCode).toEqual(500);
    });

    // getArticles
    test("Check get articles with internal error", async () => {
        const res = new ResponseClass();
        const req = {};
        const next = () => {};

        await getArticles(req, res, next);

        expect(res.statusCode).toEqual(500);
    });

    test("Check change rating with internal error", async () => {
        const res = new ResponseClass();
        const req = {
            params: 1,
            body: true
        };
        const next = () => {};

        await changeRating(req, res, next);

        expect(res.statusCode).toEqual(500);
    });

    // positive
    test("Check create article", async () => {
        const res = new ResponseClass();
        const req = {
            body: article
        };
        const next = () => {};

        const spyUser = jest.spyOn(userUsecases, "getUserById");
        spyUser.mockReturnValue(user);

        const spyTags = jest.spyOn(tagsUsecases, "checkTags");
        spyTags.mockReturnValue(tags);

        const spyArticle = jest.spyOn(articleUsecases, "createArticle");
        spyArticle.mockReturnValue(article);

        await createArticle(req, res, next);

        expect(res.statusCode).toEqual(200);
        expect(res.content.id).toEqual(article._id);
    });

    test("Check update article", async () => {
        const res = new ResponseClass();
        const req = {
            body: article,
            params: 1
        };
        const next = () => {};

        const spyUser = jest.spyOn(userUsecases, "getUserById");
        spyUser.mockReturnValue(user);

        const spyTags = jest.spyOn(tagsUsecases, "checkTags");
        spyTags.mockReturnValue(tags);

        const spyArticle = jest.spyOn(articleUsecases, "updateArticle");
        spyArticle.mockReturnValue(article);

        await updateArticle(req, res, next);

        expect(res.statusCode).toEqual(200);
        expect(res.content.id).toEqual(article._id);
    });

    test("Check delete article", async () => {
        const res = new ResponseClass();
        const req = {
            params: 1
        };
        const next = () => {};

        const spyArticle = jest.spyOn(articleUsecases, "deleteArticle");
        spyArticle.mockReturnValue(article);

        await deleteArticle(req, res, next);

        expect(res.statusCode).toEqual(200);
        expect(res.content).toEqual({});
    });

    test("Check get by id article by owner", async () => {
        const res = new ResponseClass();
        const req = {
            params: {
                articleId: 1
            },
            cookies: {
                token: "12345667890"
            }
        };
        const next = () => {};

        const spyUser = jest.spyOn(userUsecases, "getUserByToken");
        spyUser.mockReturnValue(user);

        article.toDTO = () => articleDto;

        const spyArticle = jest.spyOn(articleUsecases, "getArticleById");
        spyArticle.mockReturnValue(article);

        const spyRating = jest.spyOn(ratingUsecases, "getRateByUserAndArticle");
        spyRating.mockReturnValue(rating);

        await getArticleById(req, res, next);

        expect(res.statusCode).toEqual(200);
        expect(res.content).toEqual(articleDto);
    });
    test("Check get article by id by owner", async () => {
        const res = new ResponseClass();
        const req = {
            params: 1,
            cookies: {
                token: "12345667890"
            }
        };
        const next = () => {};

        const spyUser = jest.spyOn(userUsecases, "getUserByToken");
        spyUser.mockReturnValue(user);

        article.toDTO = () => articleDto;

        const spyArticle = jest.spyOn(articleUsecases, "getArticleById");
        spyArticle.mockReturnValue(article);

        const spyRating = jest.spyOn(ratingUsecases, "getRateByUserAndArticle");
        spyRating.mockReturnValue(rating);

        await getArticleById(req, res, next);

        expect(res.statusCode).toEqual(200);
        expect(res.content).toEqual(articleDto);
    });

    test("Check get articles", async () => {
        const res = new ResponseClass();
        const req = {
            cookies: {
                token: "12345667890"
            },
            query: {
                offset: 0,
                limit: 5,
                search: "статья",
                filters: 1,
                sort: 1
            }
        };
        const next = () => {};

        const spyUser = jest.spyOn(userUsecases, "getUserByToken");
        spyUser.mockReturnValue(user);

        article.toPreviewDTO = () => articleDto;

        const spyArticles = jest.spyOn(articleUsecases, "getArticles");
        spyArticles.mockReturnValue({articles: [article], total: 1});

        const spyMeta = jest.spyOn(articleUsecases, "getMeta");
        spyMeta.mockReturnValue(1);

        await getArticles(req, res, next);

        expect(res.statusCode).toEqual(200);
        expect(res.content).toEqual({list: [articleDto], total: 1, meta: 1});
    });

    test("Check change rating", async () => {
        const res = new ResponseClass();
        const req = {
            params: 1,
            body: true
        };
        const next = () => {};

        const spyUser = jest.spyOn(userUsecases, "getUserById");
        spyUser.mockReturnValue(user);

        article.save = jest.fn();

        const spyArticle = jest.spyOn(articleUsecases, "getArticleById");
        spyArticle.mockReturnValue(article);

        const spyChangeRating = jest.spyOn(ratingUsecases, "changeRating");
        spyChangeRating.mockReturnValue(rating);

        const spyRating = jest.spyOn(ratingUsecases, "getRateByUserAndArticle");
        spyRating.mockReturnValue(rating);

        await changeRating(req, res, next);

        expect(res.statusCode).toEqual(200);
        expect(res.content).toEqual({currentUserRate: "positive", rate: rating});
    });

    afterAll(() => {
        connection.close();
    });
});

describe("Articles: creation", async () => {
    let connection;
    const title = {
        valid: "Title",
        invalid: new Array(51).toString(),
        empty: null
    };
    const description = {
        valid: "Description",
        invalid: new Array(201).toString(),
        empty: null
    };
    const content = {
        filled: "Some description",
        empty: null
    };
    const tags = {
        from1to10: ["Tag1", "Tag2"],
        moreThan10: ["Tag1", "Tag2", "Tag3", "Tag4", "Tag5", "Tag6", "Tag7", "Tag8", "Tag9", "Tag10", "Tag11"],
        empty: null
    };

    beforeAll(async () => {
        await connectDB();
        connection = mongoose.connection;
    });

    test("1. title = Invalid, description = Invalid, content = Filled, Tags = >10", async () => {
        const req = {
            body: {
                title: title.invalid,
                description: description.invalid,
                content: content.filled,
                tags: tags.moreThan10
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await createArticle(req, res, next);

        expect(res.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    });

    test("2. title = Invalid, description = Valid, content = Empty, Tags = 1 to 10", async () => {
        const req = {
            body: {
                title: title.invalid,
                description: description.valid,
                content: content.empty,
                tags: tags.from1to10
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await createArticle(req, res, next);

        expect(res.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    });

    test("3. title = Invalid, description = Empty, content = Filled, Tags = Empty", async () => {
        const req = {
            body: {
                title: title.invalid,
                description: description.empty,
                content: content.filled,
                tags: tags.empty
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await createArticle(req, res, next);

        expect(res.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    });

    test("4. title = Valid, description = Valid, content = Filled, Tags = >10", async () => {
        const req = {
            body: {
                title: title.valid,
                description: description.valid,
                content: content.filled,
                tags: tags.moreThan10
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await createArticle(req, res, next);

        expect(res.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    });

    test("5. title = Valid, description = Empty, content = Filled, Tags = 1 to 10", async () => {
        const req = {
            body: {
                title: title.valid,
                description: description.empty,
                content: content.filled,
                tags: tags.from1to10
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await createArticle(req, res, next);

        expect(res.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    });

    test("6. title = Valid, description = Invalid, content = Empty, Tags = Empty", async () => {
        const req = {
            body: {
                title: title.valid,
                description: description.invalid,
                content: content.empty,
                tags: tags.empty
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await createArticle(req, res, next);

        expect(res.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    });

    test("7. title = Empty, description = Empty, content = Empty, Tags = >10", async () => {
        const req = {
            body: {
                title: title.empty,
                description: description.empty,
                content: content.empty,
                tags: tags.moreThan10
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await createArticle(req, res, next);

        expect(res.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    });

    test("8. title = Empty, description = Invalid, content = Filled, Tags = 1 to 10", async () => {
        const req = {
            body: {
                title: title.empty,
                description: description.invalid,
                content: content.filled,
                tags: tags.from1to10
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await createArticle(req, res, next);

        expect(res.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    });

    test("9. title = Empty, description = Valid, content = Filled, Tags = Empty", async () => {
        const req = {
            body: {
                title: title.empty,
                description: description.valid,
                content: content.filled,
                tags: tags.empty
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await createArticle(req, res, next);

        expect(res.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    });

    afterAll(async () => {
        await connection.close();
    });
});
