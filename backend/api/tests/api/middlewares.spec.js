const {AuthMiddleware} = require("../../api/middlewares");
const mongoose = require("mongoose");
const userUsecases = require("../../internal/users/usecases");

const connectDB = async () => {
    await mongoose.connect(config.mongoUrl, {
        useNewUrlParser: true,
        family: 4,
        connectTimeoutMS: 5000,
        autoReconnect: false,
        useUnifiedTopology: true,
        useCreateIndex: true
    });
};

const userInitial = {
    firstName: "Artem",
    lastName: "Boltunov",
    userName: "blotunovartem",
    phoneNumber: "+7 (929) 821-40-30",
    email: "tema2000@gmail.com",
    password: "password",
    status: "initial"
};

const userDeleted = {
    firstName: "Artem",
    lastName: "Boltunov",
    userName: "blotunovartem",
    phoneNumber: "+7 (929) 821-40-30",
    email: "tema2000@gmail.com",
    password: "password",
    status: "deleted"
};

describe("Middlewares", () => {
    let connection;

    beforeAll(async () => {
        await connectDB();
        connection = mongoose.connection;
    });

    test("Check AuthMiddleware return", async () => {
        const res = new ResponseClass();
        const req = {
            cookies: {
                token: "12345667890"
            }
        };
        const next = () => {};

        const spyUser = jest.spyOn(userUsecases, "getUserByToken");
        spyUser.mockReturnValue(userInitial);

        await AuthMiddleware(req, res, next);
        expect(res.statusCode).toEqual(null);
    });

    test("Check AuthMiddleware UNAUTHORIZED", async () => {
        const res = new ResponseClass();
        const req = {
            cookies: {
                token: "12345667890"
            }
        };
        const next = () => {};

        const spyUser = jest.spyOn(userUsecases, "getUserByToken");
        spyUser.mockReturnValue(userDeleted);

        await AuthMiddleware(req, res, next);
        expect(res.statusCode).toEqual(401);
    });

    afterAll(() => {
        connection.close();
    });
});
