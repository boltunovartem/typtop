const {getTags} = require("../../api/tags");
const mongoose = require("mongoose");
const tagsUsecases = require("../../internal/tags/usecases");

const connectDB = async () => {
    await mongoose.connect(config.mongoUrl, {
        useNewUrlParser: true,
        family: 4,
        connectTimeoutMS: 5000,
        autoReconnect: false,
        useUnifiedTopology: true,
        useCreateIndex: true
    });
};

const tags = [
    {
        id: 1,
        name: "tag1"
    },
    {
        id: 2,
        name: "tag2"
    }
];

const tagToDTO = {
    id: 3,
    name: "tag3"
};

describe("Tags", () => {
    let connection;

    beforeAll(async () => {
        await connectDB();
        connection = mongoose.connection;
    });

    test("Check get tags INTERNAL_SERVER_ERROR(500)", async () => {
        const res = new ResponseClass();
        const req = {
            query: ""
        };
        const next = () => {};

        const spyTags = jest.spyOn(tagsUsecases, "getTags");
        spyTags.mockReturnValue(tags);

        await getTags(req, res, next);
        expect(res.statusCode).toEqual(500);
    });

    test("Check get tags OK (200)", async () => {
        const res = new ResponseClass();
        const req = {
            query: ""
        };
        const next = () => {};

        tags[0].toDTO = () => tagToDTO;
        tags[1].toDTO = () => tagToDTO;

        const spyTags = jest.spyOn(tagsUsecases, "getTags");
        spyTags.mockReturnValue(tags);

        await getTags(req, res, next);
        expect(res.statusCode).toEqual(200);
    });

    afterAll(() => {
        connection.close();
    });
});
