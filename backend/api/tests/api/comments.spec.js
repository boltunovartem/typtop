const articleUsecases = require("../../internal/articles/usecases");
const userUsecases = require("../../internal/users/usecases");
const commentUsecases = require("../../internal/comments/usecases");
const ObjectId = mongoose.Types.ObjectId;
const {createComment, getComments, editComment, deleteComment} = require("../../api/comments.js");

const connectDB = async () => {
    await mongoose.connect(config.mongoUrl, {
        id: 1,
        useNewUrlParser: true,
        family: 4,
        connectTimeoutMS: 5000,
        autoReconnect: false,
        useUnifiedTopology: true,
        useCreateIndex: true
    });
};

const userData = {
    firstName: "Alena",
    lastName: "Mordvintseva",
    userName: "mordvintseva",
    phoneNumber: "+7 (929) 821-40-30",
    email: "mordvintseva@gmail.com",
    password: "password"
};

const article = {
    title: "Название",
    description: "Описание",
    content: "Содержимое"
};

const commentText = "Комментарий к статье";
const existingCommentText = "Существующий комментарий";

describe("Comments", () => {
    let connection;
    let currentUser;
    let currentArticle;
    let existingComment;

    beforeAll(async () => {
        await connectDB();
        connection = mongoose.connection;
    });

    beforeEach(async () => {
        currentUser = (await userUsecases.createUser(userData)).user;
        currentArticle = await articleUsecases.createArticle(article, currentUser._id);
        existingComment = await commentUsecases.createComment({text: existingCommentText}, currentUser, currentArticle);
    });

    test("Should create comment to existing article", async () => {
        const req = {
            body: {
                text: commentText,
                currentArticle: currentArticle
            },
            currentUser: currentUser
        };
        const res = new ResponseClass();
        const next = () => {};

        await createComment(req, res, next);

        expect(res.statusCode).toEqual(200);
    });

    test("Should not create comment with invalid structure", async () => {
        const req = {
            body: {
                commentText: commentText,
                currentArticle: currentArticle
            },
            currentUser: currentUser
        };
        const res = new ResponseClass();
        const next = () => {};

        await createComment(req, res, next);

        expect(res.statusCode).toEqual(400);
    });

    /*
    // никогда не проверяется ERROR_USER_NOT_FOUND & ERROR_ARTICLE_NOT_FOUND
    test("Should not create comment to existing article by invalid user", async () => {
        const req = {
            body: {
                text: commentText,
                currentArticle: currentArticle
            },
            currentUser: {
                id_: ObjectId("6046143371debd127a164daa")
            }
        }
        const res = new ResponseClass();
        const next = () => {};

        await createComment(req, res, next);

        expect(res.statusCode).toEqual(400);
    });
*/

    test("Should get all existing article comments", async () => {
        const req = {
            params: {
                articleId: currentArticle._id
            },
            query: {
                limit: 5,
                offset: 0
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await getComments(req, res, next);

        const comments = res.content.comments;

        expect(comments.length).toEqual(1);
    });

    test("Should edit existing comment", async () => {
        const newCommentText = "новый текст комментария";

        const req = {
            body: {
                text: newCommentText
            },
            params: {
                commentId: existingComment._id
            },
            currentUser: currentUser
        };
        const res = new ResponseClass();
        const next = () => {};

        await editComment(req, res, next);

        expect(res.statusCode).toEqual(200);
    });

    test("Should edit non-existent comment", async () => {
        const newCommentText = "новый текст комментария";

        const req = {
            body: {
                text: newCommentText
            },
            params: {
                commentId: ObjectId("5fd395d17a0afe57fafccaaa")
            },
            currentUser: currentUser
        };
        const res = new ResponseClass();
        const next = () => {};

        await editComment(req, res, next);

        expect(res.statusCode).toEqual(409);
    });

    test("Should edit comment if the user is not the owner", async () => {
        const newCommentText = "новый текст комментария";

        const req = {
            body: {
                text: newCommentText
            },
            params: {
                commentId: existingComment._id
            },
            currentUser: {
                _id: ObjectId("6045f0134f2dde19c591aaaa")
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await editComment(req, res, next);

        expect(res.statusCode).toEqual(409);
    });

    test("Should not edit existing comment with invalid new structure", async () => {
        const newCommentText = "новый текст комментария";

        const req = {
            body: {
                commentText: newCommentText
            },
            params: {
                commentId: existingComment._id
            },
            currentUser: currentUser
        };
        const res = new ResponseClass();
        const next = () => {};

        await editComment(req, res, next);

        expect(res.statusCode).toEqual(400);
    });

    test("Should delete existing comment by owner", async () => {
        const comment = await commentUsecases.createComment({text: existingCommentText}, currentUser, currentArticle);

        const req = {
            params: {
                commentId: comment._id
            },
            currentUser: currentUser
        };
        const res = new ResponseClass();
        const next = () => {};

        await deleteComment(req, res, next);

        expect(res.statusCode).toEqual(200);
    });

    test("Should not delete comment with invalid id", async () => {
        const req = {
            params: {
                commentId: ObjectId("5fd395d17a0afe57fafccaaa")
            },
            currentUser: currentUser
        };
        const res = new ResponseClass();
        const next = () => {};

        await deleteComment(req, res, next);

        expect(res.statusCode).toEqual(409);
    });

    test("Should not delete comment if user is not the owner", async () => {
        const req = {
            params: {
                commentId: existingComment._id
            },
            currentUser: {
                _id: ObjectId("6045f0134f2dde19c591aaaa")
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await deleteComment(req, res, next);

        expect(res.statusCode).toEqual(409);
    });

    afterEach(async () => {
        await commentUsecases.deleteComment(existingComment._id, currentUser);
        await articleUsecases.deleteArticle(currentArticle._id, currentUser);
        await currentUser.delete();
    });

    afterAll(() => {
        connection.close();
    });
});
