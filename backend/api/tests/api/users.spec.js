const {login, createUser, deleteUser} = require("../../api/users");
const mongoose = require("mongoose");
const userUsecases = require("../../internal/users/usecases");
const {getUserSubscriptions} = require("../../api/users");
const {subscribeToUser} = require("../../api/users");
const {getUserById} = require("../../api/users");
const {logout} = require("../../api/users");
const {updateUser} = require("../../api/users");
const {getMe} = require("../../api/users");

const connectDB = async () => {
    await mongoose.connect(config.mongoUrl, {
        useNewUrlParser: true,
        family: 4,
        connectTimeoutMS: 5000,
        autoReconnect: false,
        useUnifiedTopology: true,
        useCreateIndex: true
    });
};

describe("Users", () => {
    const existingUserData = {
        firstName: "Artem",
        lastName: "Boltunov",
        userName: "boltunov",
        phoneNumber: "+7 (929) 821-40-30",
        email: "tema@gmail.com",
        password: "password"
    };
    const newUserData = {
        firstName: "Alena",
        lastName: "Mordvintseva",
        userName: "a.mordvintseva",
        phoneNumber: "+7 (911) 123-45-67",
        email: "mordvintseva.a@gmail.com",
        password: "password"
    };
    let connection;
    let existingUser;

    beforeAll(async () => {
        await connectDB();
        connection = mongoose.connection;
    });

    beforeEach(async () => {
        existingUser = (await userUsecases.createUser(existingUserData)).user;
    });

    test("Check create user with empty data", async () => {
        const user = {};

        const res = new ResponseClass();
        const req = {
            body: user
        };
        const next = () => {};

        await createUser(req, res, next);

        expect(res.statusCode).toEqual(400);
    });

    test("Check create user if all data correct", async () => {
        const res = new ResponseClass();
        const req = {
            body: newUserData
        };
        const next = () => {};

        await createUser(req, res, next);

        expect(res.statusCode).toEqual(200);

        await deleteUserAfterTest(res.content.user);
    });

    test("Check create user if duplicate user", async () => {
        const res = new ResponseClass();
        const req = {
            body: existingUserData
        };
        const next = () => {};

        await createUser(req, res, next);

        expect(res.statusCode).toEqual(409);
    }, 1000);

    test("Check create user if duplicate email", async () => {
        let userData = {...newUserData, email: existingUserData.email};

        const res = new ResponseClass();
        const req = {
            body: userData
        };
        const next = () => {};

        await createUser(req, res, next);

        expect(res.statusCode).toEqual(409);
    }, 1000);

    test("Check create user if duplicate email", async () => {
        let userData = {...newUserData, email: existingUserData.email};

        const res = new ResponseClass();
        const req = {
            body: userData
        };
        const next = () => {};

        await createUser(req, res, next);

        expect(res.statusCode).toEqual(409);
    }, 1000);

    test("Check create user with invalid phone number", async () => {
        let userData = {...newUserData, phoneNumber: "123456789"};

        const res = new ResponseClass();
        const req = {
            body: userData
        };
        const next = () => {};

        await createUser(req, res, next);

        expect(res.statusCode).toEqual(400);
    }, 1000);

    test("Check login", async () => {
        const res = new ResponseClass();
        const next = jest.fn();

        const req = {
            body: {
                userName: "boltunov",
                password: "password"
            }
        };
        await login(req, res, next);

        expect(res.statusCode).toEqual(HttpStatus.OK);
    });

    test("Check login with invalid credentials", async () => {
        const res = new ResponseClass();
        const next = jest.fn();

        const req = {
            body: {
                userName: "somename",
                password: "pass"
            }
        };
        await login(req, res, next);

        expect(res.statusCode).toEqual(400);
    });

    test("check get me function", async () => {
        const res = new ResponseClass();
        const next = jest.fn();

        const req = {
            currentUser: existingUser
        };
        await getMe(req, res, next);

        expect(res.content.user.id).toEqual(existingUser._id);
    });

    test("check update existing user", async () => {
        const res = new ResponseClass();
        const next = jest.fn();

        const req = {
            body: {...existingUserData, firstName: newUserData.firstName},
            currentUser: existingUser
        };

        await updateUser(req, res, next);

        expect(res.content.user.firstName).toEqual(newUserData.firstName);
    });

    test("check update with invalid data", async () => {
        const res = new ResponseClass();
        const next = jest.fn();

        const req = {
            body: {...existingUserData, firstName: ""},
            currentUser: existingUser
        };

        await updateUser(req, res, next);

        expect(res.statusCode).toEqual(400);
    });

    test("check update with duplicate email", async () => {
        const res = new ResponseClass();
        const next = jest.fn();
        let req = {
            body: newUserData
        };

        await createUser(req, res, next);
        const user = res.content.user;

        req = {
            body: {...newUserData, email: existingUserData.email},
            currentUser: {...user}
        };

        await updateUser(req, res, next);

        expect(res.statusCode).toEqual(409);

        await deleteUserAfterTest(user);
    });

    test("check update with invalid phone number", async () => {
        const res = new ResponseClass();
        const next = jest.fn();

        req = {
            body: {...existingUserData, phoneNumber: "123456789"},
            currentUser: existingUser
        };

        await updateUser(req, res, next);

        expect(res.statusCode).toEqual(400);
    });

    test("check logout", async () => {
        const res = new ResponseClass();
        const next = jest.fn();

        req = {
            currentUser: existingUser
        };

        await logout(req, res, next);

        expect(res.statusCode).toEqual(200);
    });

    test("check get user by id with valid data if current user is not logged in", async () => {
        const res = new ResponseClass();
        const next = jest.fn();

        const req = {
            params: {
                profileId: existingUser.id
            }
        };

        await getUserById(req, res, next);

        expect(res.statusCode).toEqual(200);
    });

    test("check subscription", async () => {
        const res = new ResponseClass();
        const next = jest.fn();
        let req = {
            body: newUserData
        };

        await createUser(req, res, next);
        const user = res.content.user;

        req = {
            currentUser: user,
            body: {
                userId: existingUser.id
            }
        };

        await subscribeToUser(req, res, next);

        expect(res.statusCode).toEqual(200);

        await subscribeToUser(req, res, next); // unsubscribe
        await deleteUserAfterTest(user);
    });

    test("check self subscription", async () => {
        const res = new ResponseClass();
        const next = jest.fn();
        let req = {
            body: newUserData
        };

        await createUser(req, res, next);
        const user = res.content.user;

        req = {
            currentUser: user,
            body: {
                userId: user.id
            }
        };

        await subscribeToUser(req, res, next);

        expect(res.statusCode).toEqual(409);

        await deleteUserAfterTest(user);
    });

    test("check get user subscriptions", async () => {
        const res = new ResponseClass();
        const next = jest.fn();
        let req = {
            currentUser: existingUser,
            params: {
                userId: existingUser.id
            }
        };

        await getUserSubscriptions(req, res, next);

        expect(res.statusCode).toEqual(200);
    });

    test("Check delete user", async () => {
        const res = new ResponseClass();
        const next = jest.fn();
        let req = {
            body: newUserData
        };

        await createUser(req, res, next);
        const user = res.content.user;

        req = {
            currentUser: user
        };
        const userModel = await userUsecases.getUserById(user);
        await deleteUser(req, res, next);

        expect(res.statusCode).toEqual(200);
        expect(res.cookie.token).toEqual(undefined);

        await userModel.delete();
    });

    const deleteUserAfterTest = async user => {
        const userModel = await userUsecases.getUserById({id: user.id || user._id});
        await userModel.delete();
    };

    afterEach(async () => {
        await existingUser.delete();
    });

    afterAll(async () => {
        await connection.close();
    });
});

describe("Users registration: password constraints", async () => {
    const newUserData = {
        firstName: "Alena",
        lastName: "Mordvintseva",
        userName: "testuser1",
        phoneNumber: "+7 (911) 123-45-67",
        email: "bo1x@gmail.com"
    };
    const passwordLengthMin = 8;
    const passwordLengthMax = 200;
    let newUser;
    let connection;

    beforeAll(async () => {
        await connectDB();
        connection = mongoose.connection;
    });

    beforeEach(() => {
        newUser = null;
    });

    test("Should create user if password's length is less than 200", async () => {
        //generate random password
        const passwordLength = passwordLengthMax - 1;
        const password = new Array(passwordLength + 1).toString();
        const user = {...newUserData, password: password};

        const res = new ResponseClass();
        const req = {
            body: user
        };
        const next = () => {};

        await createUser(req, res, next);
        newUser = res.content.user;

        expect(res.statusCode).toEqual(200);
    });

    test("Should create user if password's length is equal to 200", async () => {
        //generate random password
        const passwordLength = passwordLengthMax;
        const password = new Array(passwordLength + 1).toString();
        const user = {...newUserData, password: password};

        const res = new ResponseClass();
        const req = {
            body: user
        };
        const next = () => {};

        await createUser(req, res, next);
        newUser = res.content.user;

        expect(res.statusCode).toEqual(200);
    });

    test("Should not create user if password's length is more than 200", async () => {
        //generate random password
        const passwordLength = passwordLengthMax + 1;
        const password = new Array(passwordLength + 1).toString();

        // Check length of test password
        // ****************
        expect(password.length).toEqual(201);
        // ****************

        const user = {...newUserData, password: password};

        const res = new ResponseClass();
        const req = {
            body: user
        };
        const next = () => {};

        await createUser(req, res, next);
        newUser = res.content.user;

        expect(res.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    });

    test("Should create user if password's length is more than 8", async () => {
        //generate random password
        const passwordLength = passwordLengthMin + 1;
        const password = new Array(passwordLength + 1).toString();

        // Check length of test password
        // ****************
        expect(password.length).toEqual(passwordLengthMin + 1);
        // ****************

        const user = {...newUserData, password: password};

        const res = new ResponseClass();
        const req = {
            body: user
        };
        const next = () => {};

        await createUser(req, res, next);
        newUser = res.content.user;

        expect(res.statusCode).toEqual(HttpStatus.OK);
    });

    test("Should create user if password's length is equal to 8", async () => {
        //generate random password
        const passwordLength = passwordLengthMin;
        const password = new Array(passwordLength + 1).toString();

        // Check length of test password
        // ****************
        expect(password.length).toEqual(passwordLengthMin);
        // ****************

        const user = {...newUserData, password: password};

        const res = new ResponseClass();
        const req = {
            body: user
        };
        const next = () => {};

        await createUser(req, res, next);
        newUser = res.content.user;

        expect(res.statusCode).toEqual(HttpStatus.OK);
    });

    test("Should not create user if password's length is less to 8", async () => {
        //generate random password
        const passwordLength = passwordLengthMin - 1;
        const password = new Array(passwordLength + 1).toString();

        // Check length of test password
        // ****************
        expect(password.length).toEqual(passwordLengthMin - 1);
        // ****************

        const user = {...newUserData, password: password};

        const res = new ResponseClass();
        const req = {
            body: user
        };
        const next = () => {};

        await createUser(req, res, next);
        newUser = res.content.user;

        expect(res.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    });

    afterEach(async () => {
        if (!newUser) return;

        const userModel = await userUsecases.getUserById({id: newUser.id || newUser._id});
        await userModel.delete();
    });

    afterAll(async () => {
        await connection.close();
    });
});

describe("User authorization", async () => {
    let connection;
    const existingUserData = {
        firstName: "Artem",
        lastName: "Boltunov",
        userName: "boltunov",
        phoneNumber: "+7 (929) 821-40-30",
        email: "tema@gmail.com",
        password: "password"
    };
    let existingUser;

    beforeAll(async () => {
        await connectDB();
        connection = mongoose.connection;
    });

    beforeEach(async () => {
        existingUser = (await userUsecases.createUser(existingUserData)).user;
    });

    test("Should login with valid credentials", async () => {
        const password = existingUserData.password;
        const userName = existingUserData.userName;

        const req = {
            body: {
                userName: userName,
                password: password
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await login(req, res, next);

        expect(res.statusCode).toEqual(HttpStatus.OK);
    });

    test("Should not login with invalid password", async () => {
        const password =
            existingUserData.password +
            Math.random()
                .toString(16)
                .substr(2, 1);
        const userName = existingUserData.userName;

        const req = {
            body: {
                userName: userName,
                password: password
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await login(req, res, next);

        expect(res.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    });

    test("Should not login with invalid userName", async () => {
        const password = existingUserData.password;
        const userName =
            existingUserData.userName +
            Math.random()
                .toString(16)
                .substr(2, 1);

        const req = {
            body: {
                userName: userName,
                password: password
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await login(req, res, next);

        expect(res.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    });

    test("Should not login with invalid both password and userName", async () => {
        const password =
            existingUserData.password +
            Math.random()
                .toString(16)
                .substr(2, 1);
        const userName =
            existingUserData.userName +
            Math.random()
                .toString(16)
                .substr(2, 1);

        const req = {
            body: {
                userName: userName,
                password: password
            }
        };
        const res = new ResponseClass();
        const next = () => {};

        await login(req, res, next);

        expect(res.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    });

    afterEach(async () => {
        await existingUser.delete();
    });

    afterAll(async () => {
        await connection.close();
    });
});
