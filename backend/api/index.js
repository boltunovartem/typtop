const config = require("./config");
global.config = config;

const HttpStatus = require("http-status-codes");
global.HttpStatus = HttpStatus;

const {defaultLogger} = require("./internal/logger");
global.logger = defaultLogger;

const Engine = require("./engine");

(async () => {
    const engine = new Engine();

    console.log("Connect to mongodb");
    await engine.connectMongoDB();
    await engine.start();
})();
