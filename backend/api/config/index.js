module.exports = {
    port: process.env.PORT || 8889,
    withHttps: process.env.WITH_HTTPS || false,
    allowedOrigin: process.env.ALLOWED_ORIGIN || "http://localhost:1234",
    mongoUrl: process.env.MONGO_URL || "mongodb://127.0.0.1:27017/typtop",
    apiRoot: "/api/v1",
    frontendUrl: process.env.FRONTEND_URL || "http://localhost:1234",
    localUrl: process.env.LOCAL_URL || "http://localhost:8889",
    filePath: process.env.FILE_PATH || "/etc/typtop_content",
    fileUrl: process.env.FILE_URL || "http://localhost:8889/static",
    rateLimiterIpWhitelist: ["127.0.0.1"],
    defaultRateLimiter: {
        windowMs: 1000,
        max: 5000,
        skipFailedRequests: true,
        message: "Too many request from this IP"
    },
    registerLimiter: {
        windowMs: 60 * 1000,
        max: 2000,
        message: "Too many accounts created from this IP, please try again after an hour"
    },
    loginLimiter: {
        windowMs: 60 * 1000,
        max: 2000,
        message: "Too many login from this IP, please try again after an hour"
    },
    security: {
        frameguard: process.env.ENABLE_FRAME_GUARD === "true"
    },
    jwtSecret: "8*b$x9k7uZR-Nd-?GS#dy&EjK*Pz#ya+$mmrH$?Rp6aP!tH$M5fTj&Q$bMb2L=Gc"
};
