const sendInternalServerError = (res, e, next) => {
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({error: "InternalServerError"});
    return next(e);
};

module.exports = {sendInternalServerError};
