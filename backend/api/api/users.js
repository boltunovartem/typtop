const usecases = require("../internal/users/usecases");
const {validateStruct} = require("../internal/utils/validator");
const {sendInternalServerError} = require("../api/common");
const subUsecases = require("../internal/subscribers/usecases");

const createUser = async (req, res, next) => {
    try {
        const createUserForm = {
            firstName: {
                required: {message: "Введите имя"},
                min: {value: 2, message: "Имя должно быть длиннее 2 символов"},
                max: {value: 50, message: "Имя должно быть короче 50 символов"}
            },
            lastName: {
                required: {message: "Введите фамилию"},
                min: {value: 2, message: "Фамилия должна быть длиннее 2 символов"},
                max: {value: 50, message: "Фамилия должна быть короче 50 символов"}
            },
            userName: {
                required: {message: "Введите логин"},
                min: {value: 2, message: "Логин должен быть длиннее 2 символов"},
                max: {value: 50, message: "Логин должен быть короче 50 символов"}
            },
            phoneNumber: {
                required: {message: "Ведите номер телефона"}
            },
            email: {
                required: {message: "Введите адрес электронной почты"},
                email: {
                    message: "Некорректный адрес электронной почты"
                }
            },
            password: {
                required: {message: "Введите пароль"},
                min: {value: 8, message: "Пароль должен быть длиннее 8 символов"},
                max: {value: 200, message: "Пароль должен быть короче 200 символов"}
            }
        };
        const {errors, hasErrors} = validateStruct(req.body, createUserForm);
        if (hasErrors) {
            res.status(HttpStatus.BAD_REQUEST);
            return res.send(errors);
        }

        const {user, token} = await usecases.createUser(req.body);
        res.cookie("token", token, {maxAge: 7 * 24 * 60 * 60 * 1000, httpOnly: true});
        res.status(HttpStatus.OK);
        res.send({user: user.toDTO()});
    } catch (e) {
        if (e === usecases.ERROR_DUPLICATE_USER) {
            res.status(HttpStatus.CONFLICT);
            return res.send({userName: "Пользователь с таким логином существует"});
        }
        if (e === usecases.ERROR_DUPLICATE_EMAIL) {
            res.status(HttpStatus.CONFLICT);
            return res.send({email: "Пользователь с таким адресом эл. почты существует"});
        }
        if (e === usecases.ERROR_NOT_SUPPORTED_PHONE_NUMBER) {
            res.status(HttpStatus.BAD_REQUEST);
            return res.send({phoneNumber: "Некорректный номер телефона"});
        }
        sendInternalServerError(res, e, next);
    }
};

const login = async (req, res, next) => {
    try {
        const {user, token} = await usecases.login(req.body);
        res.cookie("token", token, {maxAge: 7 * 24 * 60 * 60 * 1000, httpOnly: true});
        res.status(HttpStatus.OK);
        res.send({user: user.toDTO()});
    } catch (e) {
        if (e === usecases.ERROR_INVALID_CREDENTIALS) {
            res.status(HttpStatus.BAD_REQUEST);
            return res.send({error: "Неправильно указан логин и/или пароль"});
        }
        sendInternalServerError(res, e, next);
    }
};

const getMe = async (req, res, next) => {
    try {
        const {currentUser} = req;
        res.send({user: currentUser.toDTO()});
    } catch (e) {
        sendInternalServerError(res, e, next);
    }
};

const updateUser = async (req, res, next) => {
    try {
        const updateUserForm = {
            firstName: {
                required: {message: "Введите имя"},
                min: {value: 2, message: "Имя должно быть длиннее 2 символов"},
                max: {value: 50, message: "Имя должно быть короче 50 символов"}
            },
            lastName: {
                required: {message: "Введите фамилию"},
                min: {value: 2, message: "Фамилия должна быть длиннее 2 символов"},
                max: {value: 50, message: "Фамилия должна быть короче 50 символов"}
            },
            phoneNumber: {
                required: {message: "Ведите номер телефона"}
            },
            email: {
                required: {message: "Введите адрес электронной почты"},
                email: {
                    message: "Некорректный адрес электронной почты"
                }
            },
            password: {
                omitempty: true,
                required: {message: "Введите пароль"},
                min: {value: 8, message: "Пароль должен быть длиннее 8 символов"},
                max: {value: 200, message: "Пароль должен быть короче 200 символов"}
            }
        };
        const {errors, hasErrors} = validateStruct(req.body, updateUserForm);
        if (hasErrors) {
            res.status(HttpStatus.BAD_REQUEST);
            return res.send(errors);
        }

        const {user} = await usecases.updateUser(req.body, req.currentUser);
        res.status(HttpStatus.OK);
        res.send({user: user.toDTO()});
    } catch (e) {
        if (e === usecases.ERROR_DUPLICATE_EMAIL) {
            res.status(HttpStatus.CONFLICT);
            return res.send({email: "Пользователь с таким адресом эл. почты существует"});
        }
        if (e === usecases.ERROR_NOT_SUPPORTED_PHONE_NUMBER) {
            res.status(HttpStatus.BAD_REQUEST);
            return res.send({phoneNumber: "Некорректный номер телефона"});
        }
        sendInternalServerError(res, e, next);
    }
};

const deleteUser = async (req, res, next) => {
    try {
        await usecases.deleteUser(req.currentUser);
        res.clearCookie("token");
        res.status(HttpStatus.OK);
        res.send();
    } catch (e) {
        sendInternalServerError(res, e, next);
    }
};

const logout = async (req, res, next) => {
    try {
        res.clearCookie("token");
        res.status(HttpStatus.OK);
        res.send({});
    } catch (e) {
        sendInternalServerError(res, e, next);
    }
};

const getUserById = async (req, res, next) => {
    try {
        const getUserValidation = {
            profileId: {
                required: {message: "Чтобы получить пользователя укажите ID"},
                mongoId: {message: "Неверный формат идентификатора пользователя"}
            }
        };

        const {errors, hasErrors} = validateStruct(req.params, getUserValidation);
        if (hasErrors) {
            res.status(HttpStatus.BAD_REQUEST);
            return res.send(errors);
        }

        const user = await usecases.getUserById({id: req.params.profileId});

        const token = req.cookies && req.cookies.token;
        const currentUser = await usecases.getUserByToken(token);

        const dto = await user.toDTO();
        dto.isSubscribed = currentUser && !!(await subUsecases.getSubscription(user, currentUser));

        res.status(HttpStatus.OK);
        res.send({user: dto});
    } catch (e) {
        if (e === usecases.ERROR_USER_NOT_FOUND) {
            res.status(HttpStatus.NOT_FOUND);
            return res.send({error: "Пользователь не найден"});
        }
        sendInternalServerError(res, e, next);
    }
};

const subscribeToUser = async (req, res, next) => {
    try {
        const owner = await usecases.getUserById(req.currentUser);
        const subscribedTo = await usecases.getUserById({id: req.body.userId});

        res.status(HttpStatus.OK);
        res.send({isSubscribed: !!(await subUsecases.subscribeToUser(subscribedTo, owner))});
    } catch (e) {
        if (e === usecases.ERROR_USER_NOT_FOUND) {
            res.status(HttpStatus.NOT_FOUND);
            return res.send({error: "Пользователя не существует"});
        }
        if (e === subUsecases.ERROR_INVALID_USER) {
            res.status(HttpStatus.CONFLICT);
            return res.send({error: "Вы не можете подписаться сами на себя"});
        }
        sendInternalServerError(res, e, next);
    }
};

const getUserSubscriptions = async (req, res, next) => {
    try {
        const currentUser = req.currentUser;
        const user = await usecases.getUserById({id: req.params.userId});
        const {followers, following} = await subUsecases.getSubscriptionsByUser(currentUser, user);

        res.status(HttpStatus.OK);
        res.send({followers, following});
    } catch (e) {
        sendInternalServerError(res, e, next);
    }
};

module.exports = {
    createUser,
    login,
    logout,
    getMe,
    deleteUser,
    updateUser,
    getUserById,
    subscribeToUser,
    getUserSubscriptions
};
