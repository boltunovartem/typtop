const rateLimit = require("express-rate-limit");
const MongoStore = require("rate-limit-mongo");

const skipWhitelist = req => !!config.rateLimiterIpWhitelist.find(ip => req.ip.includes(ip));

const defaultLimiter = rateLimit({
    store: new MongoStore({
        uri: config.mongoUrl,
        expireTimeMs: config.defaultRateLimiter.windowMs,
        collectionName: "expressRateRecords"
    }),
    handler: (req, res) => res.status(HttpStatus.TOO_MANY_REQUESTS).json({error: config.defaultRateLimiter.message}),
    skip: skipWhitelist,
    ...config.defaultRateLimiter
});

const registerLimiter = rateLimit({
    store: new MongoStore({
        uri: config.mongoUrl,
        expireTimeMs: config.registerLimiter.windowMs,
        collectionName: "expressRegistrationRateRecords"
    }),
    handler: (req, res) => res.status(HttpStatus.TOO_MANY_REQUESTS).json({error: config.registerLimiter.message}),
    skip: skipWhitelist,
    ...config.registerLimiter
});

const loginLimiter = rateLimit({
    store: new MongoStore({
        uri: config.mongoUrl,
        expireTimeMs: config.loginLimiter.windowMs,
        collectionName: "expressLoginRateRecords"
    }),
    handler: (req, res) => res.status(HttpStatus.TOO_MANY_REQUESTS).json({error: config.loginLimiter.message}),
    skip: skipWhitelist,
    ...config.loginLimiter
});

module.exports = {defaultLimiter, loginLimiter, registerLimiter};
