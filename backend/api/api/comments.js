const commentsUsecases = require("../internal/comments/usecases");
const userUsecases = require("../internal/users/usecases");
const articleUsecases = require("../internal/articles/usecases");
const {validateStruct} = require("../internal/utils/validator");
const {sendInternalServerError} = require("./common");

const commentForm = {
    text: {
        required: {message: "Введите текст комментария"},
        min: {value: 1, message: "Длина комментария должна быть больше 1 символа"},
        max: {value: 200, message: "Длина коммментария должна быть меньше 200 символов"}
    }
};

const createComment = async (req, res, next) => {
    try {
        const {errors, hasErrors} = validateStruct(req.body, commentForm);

        if (hasErrors) {
            res.status(HttpStatus.BAD_REQUEST);
            return res.send(errors);
        }

        const currentArticle = await articleUsecases.getArticleById(req.body.currentArticle.id);
        const comment = await commentsUsecases.createComment(req.body, req.currentUser, currentArticle);

        res.status(HttpStatus.OK);
        res.send({comment: await comment.toDTO()});
    } catch (e) {
        if (e === userUsecases.ERROR_USER_NOT_FOUND) {
            res.status(HttpStatus.BAD_REQUEST);
            return res.send({error: "Неправильно указан пользователь"});
        }
        if (e === articleUsecases.ERROR_ARTICLE_NOT_FOUND) {
            res.status(HttpStatus.BAD_REQUEST);
            return res.send({error: "Неправильно указана статья"});
        }
        sendInternalServerError(res, e, next);
    }
};

const getComments = async (req, res, next) => {
    try {
        const {offset, limit} = req.query;
        const {articleId} = req.params;

        const currentArticle = await articleUsecases.getArticleById(articleId);
        const {comments, total} = await commentsUsecases.getComments(currentArticle, offset, limit);

        res.status(HttpStatus.OK);
        res.send({comments, total});
    } catch (e) {
        sendInternalServerError(res, e, next);
    }
};

const editComment = async (req, res, next) => {
    try {
        const {errors, hasErrors} = validateStruct(req.body, commentForm);

        if (hasErrors) {
            res.status(HttpStatus.BAD_REQUEST);
            return res.send(errors);
        }

        await commentsUsecases.editComment(req.params.commentId, req.body.text, req.currentUser);

        res.status(HttpStatus.OK);
        res.send({});
    } catch (e) {
        if (e === commentsUsecases.ERROR_INVALID_COMMENT_ID) {
            res.status(HttpStatus.CONFLICT);
            return res.send({comment: "Некорректный идентификатор комментария"});
        }
        if (e === commentsUsecases.ERROR_INVALID_USER) {
            res.status(HttpStatus.CONFLICT);
            return res.send({user: "Редактировать комментарий может только его владелец"});
        }
        sendInternalServerError(res, e, next);
    }
};

const deleteComment = async (req, res, next) => {
    try {
        await commentsUsecases.deleteComment(req.params.commentId, req.currentUser);

        res.status(HttpStatus.OK);
        res.send({});
    } catch (e) {
        if (e === commentsUsecases.ERROR_INVALID_COMMENT_ID) {
            res.status(HttpStatus.CONFLICT);
            return res.send({comment: "Некорректный идентификатор комментария"});
        }
        if (e === commentsUsecases.ERROR_INVALID_USER) {
            res.status(HttpStatus.CONFLICT);
            return res.send({user: "Удалить комментарий может только его владелец"});
        }
        sendInternalServerError(res, e, next);
    }
};

module.exports = {
    createComment,
    editComment,
    deleteComment,
    getComments
};
