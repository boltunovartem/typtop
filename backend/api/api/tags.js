const usecases = require("../internal/tags/usecases");
const {sendInternalServerError} = require("./common");

const getTags = async (req, res, next) => {
    try {
        const {search} = req.query;
        const tags = await usecases.getTags(search);
        const dtos = tags.map(tag => tag.toDTO());

        res.status(HttpStatus.OK);
        res.send({tags: dtos});
    } catch (e) {
        sendInternalServerError(res, e, next);
    }
};

module.exports = {
    getTags
};
