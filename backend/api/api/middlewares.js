const userStatuses = require("../internal/users/consts/userStatuses");
const userUsecases = require("../internal/users/usecases");

const isUserUnauthorized = user => {
    return !user || user.status !== userStatuses.initial;
};

const sendUnauthorizedError = res => {
    res.status(HttpStatus.UNAUTHORIZED);
    return res.send({error: "Unauthorized"});
};

const AuthMiddleware = async (req, res, next) => {
    const token = req.cookies && req.cookies.token;
    const user = await userUsecases.getUserByToken(token);
    if (isUserUnauthorized(user)) {
        return sendUnauthorizedError(res);
    }
    req.currentUser = user;
    return next();
};

module.exports = {
    AuthMiddleware
};
