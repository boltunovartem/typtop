const usecases = require("../internal/articles/usecases");
const userUsecases = require("../internal/users/usecases");
const tagsUsecases = require("../internal/tags/usecases");
const ratingUsecases = require("../internal/ratings/usecases");
const {validateStruct} = require("../internal/utils/validator");
const {sendInternalServerError} = require("./common");

const articleForm = {
    title: {
        required: {message: "Введите название"},
        min: {value: 2, message: "Название должно быть длиннее 2 символов"},
        max: {value: 50, message: "Название должно быть короче 50 символов"}
    },
    description: {
        required: {message: "Введите краткое описание"},
        min: {value: 2, message: "Краткое описание должно быть длиннее 2 символов"},
        max: {value: 200, message: "Краткое описание должно быть короче 200 символов"}
    },
    content: {
        required: {message: "Напишите статью"}
    },
    tags: {
        isArray: {message: "Неверный формат тегов"},
        arrayLengthMax: {max: 10, message: "Нельзя добавить больше 10 тегов"}
    }
};

const createArticle = async (req, res, next) => {
    try {
        const {errors, hasErrors} = validateStruct(req.body, articleForm);
        if (hasErrors) {
            res.status(HttpStatus.BAD_REQUEST);
            return res.send(errors);
        }
        const currentUser = await userUsecases.getUserById(req.currentUser);
        req.body.tags = await tagsUsecases.checkTags(req.body.tags);
        const article = await usecases.createArticle(req.body, currentUser);

        res.status(HttpStatus.OK);
        res.send({id: article._id});
    } catch (e) {
        sendInternalServerError(res, e, next);
    }
};

const updateArticle = async (req, res, next) => {
    try {
        const {errors, hasErrors} = validateStruct(req.body, articleForm);
        if (hasErrors) {
            res.status(HttpStatus.BAD_REQUEST);
            return res.send(errors);
        }

        const currentUser = await userUsecases.getUserById(req.currentUser);
        req.body.tags = await tagsUsecases.checkTags(req.body.tags);
        const {articleId} = req.params;
        const article = await usecases.updateArticle(articleId, req.body, currentUser);

        res.status(HttpStatus.OK);
        res.send({id: article._id});
    } catch (e) {
        sendInternalServerError(res, e, next);
    }
};

const deleteArticle = async (req, res, next) => {
    try {
        const {articleId} = req.params;
        await usecases.deleteArticle(articleId, req.currentUser);

        res.status(HttpStatus.OK);
        res.send({});
    } catch (e) {
        if (e === usecases.ERROR_ARTICLE_NOT_FOUND) {
            res.status(HttpStatus.NOT_FOUND);
            return res.send({error: "Статья не найдена"});
        }
        if (e === usecases.ERROR_INVALID_USER) {
            res.status(HttpStatus.CONFLICT);
            return res.send({user: "Удалять статью может только ее владелец"});
        }
        sendInternalServerError(res, e, next);
    }
};

const getArticleById = async (req, res, next) => {
    try {
        const {articleId} = req.params;
        const token = req.cookies && req.cookies.token;
        const currentUser = token && (await userUsecases.getUserByToken(token));
        const article = await usecases.getArticleById(articleId);
        const dto = await article.toDTO();

        if (currentUser) {
            const currentUserRate = await ratingUsecases.getRateByUserAndArticle(currentUser, article);
            if (currentUserRate) dto.currentUserRate = currentUserRate.positive ? "positive" : "negative";
        }

        res.status(HttpStatus.OK);
        res.send(dto);
    } catch (e) {
        if (e === usecases.ERROR_ARTICLE_NOT_FOUND) {
            res.status(HttpStatus.NOT_FOUND);
            return res.send({error: "Статья не найдена"});
        }
        sendInternalServerError(res, e, next);
    }
};

const getArticles = async (req, res, next) => {
    try {
        const {offset, limit, search, filters, sort} = req.query;
        const token = req.cookies && req.cookies.token;
        const currentUser = await userUsecases.getUserByToken(token);
        const {articles, total} = await usecases.getArticles(
            currentUser,
            search,
            offset,
            limit,
            JSON.parse(filters),
            JSON.parse(sort)
        );
        const meta = await usecases.getMeta();
        const dtos = [];
        for (const article of articles) {
            dtos.push(await article.toPreviewDTO());
        }
        res.status(HttpStatus.OK);
        res.send({list: dtos, total, meta});
    } catch (e) {
        sendInternalServerError(res, e, next);
    }
};

const changeRating = async (req, res, next) => {
    try {
        const {articleId} = req.params;
        const {isPositive} = req.body;
        const article = await usecases.getArticleById(articleId);
        const currentUser = await userUsecases.getUserById(req.currentUser);
        const rate = await ratingUsecases.changeRating(article, currentUser, isPositive);
        const response = {rate};
        article.rate = rate;
        article.save();

        const currentUserRateModel = await ratingUsecases.getRateByUserAndArticle(currentUser, article);
        if (currentUserRateModel) response.currentUserRate = currentUserRateModel.positive ? "positive" : "negative";

        res.status(HttpStatus.OK);
        res.send(response);
    } catch (e) {
        sendInternalServerError(res, e, next);
    }
};

module.exports = {
    createArticle,
    updateArticle,
    deleteArticle,
    getArticleById,
    getArticles,
    changeRating
};
