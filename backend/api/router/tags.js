const express = require("express");
const TagsApi = require("../api/tags");

module.exports = app => {
    const tagsRouter = express.Router({mergeParams: true});

    tagsRouter.route("/").get(TagsApi.getTags);

    app.use(`${config.apiRoot}/tags`, tagsRouter);
};
