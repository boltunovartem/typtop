const express = require("express");
const CommentApi = require("../api/comments");
const middlewares = require("../api/middlewares");

module.exports = app => {
    const commentsRouter = express.Router({mergeParams: true});

    commentsRouter.route("/:articleId").get(CommentApi.getComments);

    commentsRouter.use(middlewares.AuthMiddleware);

    commentsRouter.route("/").post(CommentApi.createComment);

    commentsRouter
        .route("/:commentId")
        .put(CommentApi.editComment)
        .delete(CommentApi.deleteComment);

    app.use(`${config.apiRoot}/comments`, commentsRouter);
};
