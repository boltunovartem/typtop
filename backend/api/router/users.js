const express = require("express");
const middlewares = require("../api/middlewares");
const {registerLimiter, loginLimiter} = require("../api/rateLimiters");
const UserApi = require("../api/users");

module.exports = app => {
    const userRouter = express.Router({mergeParams: true});
    userRouter.route("/register").post(registerLimiter, UserApi.createUser);
    userRouter.route("/login").post(loginLimiter, UserApi.login);
    userRouter.route("/:profileId").get(UserApi.getUserById);

    userRouter.use(middlewares.AuthMiddleware);
    userRouter
        .route("/")
        .get(UserApi.getMe)
        .put(UserApi.updateUser)
        .delete(UserApi.deleteUser);
    userRouter.route("/logout").post(UserApi.logout);

    userRouter.route("/subscribe").post(UserApi.subscribeToUser);
    userRouter.route("/:userId/subscriptions").get(UserApi.getUserSubscriptions);

    app.use(`${config.apiRoot}/users`, userRouter);
};
