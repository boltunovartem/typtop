const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const helmet = require("helmet");
const addUserRoute = require("./users");
const addArticlesRoute = require("./articles");
const addCommentsRoute = require("./comments");
const addTagRoute = require("./tags");
const {expressRequestLogger, expressErrorLogger} = require("../internal/logger");
const {defaultLimiter} = require("../api/rateLimiters");

class Router {
    constructor(app) {
        this.app = app;
        this.app.set("trust proxy", () => true);
    }

    async configure() {
        this.applyDefaultMiddlewares();
        this.app.use("/api/", defaultLimiter);

        addUserRoute(this.app);
        addArticlesRoute(this.app);
        addCommentsRoute(this.app);
        addTagRoute(this.app);

        this.addStaticRoute();
        this.addDefaultRoute();
    }

    applyDefaultMiddlewares() {
        if (config.allowedOrigin === "*") {
            this.app.use(cors());
        } else {
            this.app.use(cors({origin: config.allowedOrigin, credentials: true}));
        }
        this.app.use(cookieParser());
        this.app.use(bodyParser.json({limit: "400mb"}));
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(expressRequestLogger);
        this.app.use(helmet(config.security));
    }

    addDefaultRoute() {
        this.app.use(expressErrorLogger);
        this.app.use((req, res) => res.sendStatus(HttpStatus.NOT_FOUND));
    }

    addStaticRoute() {
        this.app.use("/static", express.static(config.filePath));
    }
}

module.exports = Router;
