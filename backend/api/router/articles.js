const express = require("express");
const middlewares = require("../api/middlewares");
const ArticlesApi = require("../api/articles");

module.exports = app => {
    const articlesRouter = express.Router({mergeParams: true});

    articlesRouter.route("/").get(ArticlesApi.getArticles);
    articlesRouter.route("/:articleId").get(ArticlesApi.getArticleById);

    articlesRouter.use(middlewares.AuthMiddleware);
    articlesRouter.route("/").post(ArticlesApi.createArticle);
    articlesRouter.route("/:articleId").put(ArticlesApi.updateArticle);
    articlesRouter.route("/:articleId").delete(ArticlesApi.deleteArticle);

    articlesRouter.route("/:articleId/rating").post(ArticlesApi.changeRating);

    app.use(`${config.apiRoot}/articles`, articlesRouter);
};
