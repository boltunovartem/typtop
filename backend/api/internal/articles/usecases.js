const ArticleModel = require("./article");
const tagsUsecases = require("../tags/usecases");
const subscribersUsecases = require("../subscribers/usecases");
const ratingsUsecases = require("../ratings/usecases");
const commentsUsecases = require("../comments/usecases");

const ERROR_ARTICLE_NOT_FOUND = new Error("ERROR_ARTICLE_NOT_FOUND");
const ERROR_INVALID_USER = new Error("ERROR_INVALID_USER");

const createArticle = async (data, currentUser) => {
    const {title, description, content, tags} = data;
    return await ArticleModel.create({
        owner: currentUser,
        title,
        description,
        content,
        tags
    });
};

const updateArticle = async (id, data, currentUser) => {
    const {title, description, content, tags} = data;
    return await ArticleModel.findByIdAndUpdate(id, {
        owner: currentUser,
        title,
        description,
        content,
        tags
    });
};

const deleteArticle = async (id, currentUser) => {
    const article = await ArticleModel.findById(id);
    if (!article) throw ERROR_ARTICLE_NOT_FOUND;
    if (!currentUser._id.equals(article.owner._id)) throw ERROR_INVALID_USER;
    await commentsUsecases.deleteAllArticleComments(id);
    await ratingsUsecases.deleteAllArticleRates(id);
    await article.delete();
};

const getArticleById = async id => {
    const article = await ArticleModel.findOne({_id: id});
    if (!article) throw ERROR_ARTICLE_NOT_FOUND;

    return article;
};

const getArticles = async (currentUser, search, offset, limit, filters, sort) => {
    let query = {};
    if (search) {
        query = {
            $or: [{title: {$regex: search}}, {description: {$regex: search}}, {content: {$regex: search}}]
        };
    }
    const {owner, date, rating, favorite, tags} = filters;
    if (date) {
        const {startDate, endDate} = date;
        if (startDate) query.publicationDate = {$gte: startDate};
        if (endDate) query.publicationDate = {$lte: endDate};
    }
    if (rating) {
        query.$and = [
            {
                rate: {$gte: rating[0]}
            },
            {
                rate: {$lte: rating[1]}
            }
        ];
    }
    if (tags && tags.length > 0) {
        const tagsModels = await tagsUsecases.checkTags(tags);
        query.tags = {$in: tagsModels};
    }
    if (favorite) {
        const subscriptions = await subscribersUsecases.getFavorites(currentUser);
        const users = subscriptions.map(s => s.subscribedTo);
        query.owner = {$in: users};
    }
    if (owner) {
        query.owner = owner;
    }

    const total = await getCountOfArticles(query);
    const articles = await ArticleModel.find(query, {content: 0})
        .sort(sort || {publicationDate: -1})
        .skip(parseInt(offset))
        .limit(parseInt(limit));

    return {articles, total};
};

const getCountOfArticles = async query => {
    return ArticleModel.countDocuments(query);
};

const getMeta = async () => {
    const articleMinRate = (await ArticleModel.find()
        .sort({rate: 1})
        .limit(1))[0];
    const articleMaxRate = (await ArticleModel.find()
        .sort({rate: -1})
        .limit(1))[0];
    const rate = {
        min: articleMinRate ? articleMinRate.rate : 0,
        max: articleMaxRate ? articleMaxRate.rate : 0
    };

    return {rate};
};

module.exports = {
    getArticleById,
    createArticle,
    updateArticle,
    deleteArticle,
    getArticles,
    getMeta,
    getCountOfArticles,
    ERROR_ARTICLE_NOT_FOUND,
    ERROR_INVALID_USER
};
