const mongoose = require("mongoose");
const ObjectId = mongoose.Schema.Types.ObjectId;

const Article = mongoose.Schema({
    owner: {type: ObjectId, ref: "User", required: true},
    title: {type: String, required: true},
    description: {type: String, required: true},
    content: {type: String, required: true},
    tags: [{type: ObjectId, ref: "Tag"}],
    publicationDate: {type: Date, default: Date.now},
    rate: {type: Number, default: 0}
});

Article.methods.toDTO = async function() {
    await this.populate("owner").execPopulate();
    await this.populate("tags").execPopulate();

    return {
        id: this._id,
        owner: this.owner.toNameDTO(),
        title: this.title,
        description: this.description,
        content: this.content,
        tags: this.tags.map(tag => tag.toDTO()),
        publicationDate: this.publicationDate,
        rate: this.rate
    };
};

Article.methods.toPreviewDTO = async function() {
    await this.populate("owner").execPopulate();
    await this.populate("tags").execPopulate();

    return {
        id: this._id,
        owner: this.owner.toNameDTO(),
        title: this.title,
        description: this.description,
        tags: this.tags.map(tag => tag.toDTO()),
        publicationDate: this.publicationDate,
        rate: this.rate
    };
};

mongoose.set("useFindAndModify", false);
module.exports = mongoose.model("Article", Article);
