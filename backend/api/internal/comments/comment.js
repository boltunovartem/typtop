const mongoose = require("mongoose");
const ObjectId = mongoose.Schema.Types.ObjectId;

const Comment = mongoose.Schema({
    article: {type: ObjectId, ref: "Article"},
    owner: {type: ObjectId, ref: "User", required: true},
    text: {type: String, required: true, "min-maxlength": 200},
    publicationDate: {type: Date, default: Date.now}
});

Comment.methods.toDTO = async function() {
    await this.populate("owner").execPopulate();

    return {
        id: this._id,
        owner: this.owner.toNameDTO(),
        text: this.text,
        publicationDate: this.publicationDate
    };
};

module.exports = mongoose.model("Comment", Comment);
