mongoose = require("mongoose");
const CommentModel = require("./comment");

const ERROR_INVALID_COMMENT_ID = new Error("ERROR_INVALID_COMMENT_ID");
const ERROR_INVALID_USER = new Error("ERROR_INVALID_USER");

async function createComment(data, currentUser, currentArticle) {
    const {text} = data;
    return CommentModel.create({
        article: currentArticle,
        owner: currentUser,
        text
    });
}

const editComment = async (id, text, currentUser) => {
    let comment = await CommentModel.findById(id);

    if (!comment) throw ERROR_INVALID_COMMENT_ID;

    if (!currentUser._id.equals(comment.owner._id)) throw ERROR_INVALID_USER;

    comment.text = text;
    await comment.save();
};

const deleteComment = async (id, currentUser) => {
    let comment = await CommentModel.findById(id);

    if (!comment) throw ERROR_INVALID_COMMENT_ID;

    if (!currentUser._id.equals(comment.owner._id)) throw ERROR_INVALID_USER;

    await comment.delete();
};

const getComments = async (currentArticle, offset, limit) => {
    const comments = await CommentModel.find({article: currentArticle})
        .sort({publicationDate: -1})
        .skip(parseInt(offset))
        .limit(parseInt(limit));

    const commentsDTO = await Promise.all(
        comments.map(async model => {
            return model.toDTO();
        })
    );

    const total = await CommentModel.countDocuments({article: currentArticle});

    return {
        comments: commentsDTO,
        total: total
    };
};

const deleteAllArticleComments = async articleId => {
    await CommentModel.deleteMany({article: articleId});
};

module.exports = {
    createComment,
    editComment,
    deleteComment,
    getComments,
    deleteAllArticleComments,
    ERROR_INVALID_COMMENT_ID,
    ERROR_INVALID_USER
};
