const mongoose = require("mongoose");
const ObjectId = mongoose.Schema.Types.ObjectId;

const Subscriber = mongoose.Schema({
    owner: {type: ObjectId, ref: "User", required: true},
    subscribedTo: {type: ObjectId, ref: "User", required: true}
});

Subscriber.methods.toDTO = function() {
    return {
        owner: this.owner,
        subscribedTo: this.subscribedTo
    };
};

Subscriber.methods.toFollowersDTO = async function() {
    await this.populate("owner").execPopulate();

    return this.owner.toNameDTO();
};

Subscriber.methods.toFollowingDTO = async function() {
    await this.populate("subscribedTo").execPopulate();

    return this.subscribedTo.toNameDTO();
};

module.exports = mongoose.model("Subscriber", Subscriber);
