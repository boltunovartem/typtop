const SubscriberModel = require("./subsriber");

const ERROR_INVALID_USER = new Error("ERROR_INVALID_USER");

async function subscribeToUser(subscribedTo, owner) {
    if (owner.id === subscribedTo.id) {
        throw ERROR_INVALID_USER;
    }
    const subscription = await SubscriberModel.findOne({owner: owner, subscribedTo: subscribedTo});
    if (subscription) {
        SubscriberModel.deleteOne({_id: subscription.id}, function(error) {
            if (error) throw new Error("Error occurred during deletion: " + error);
        });
        return null;
    } else {
        return await SubscriberModel.create({owner: owner, subscribedTo: subscribedTo});
    }
}

async function getSubscription(subscribedTo, owner) {
    return SubscriberModel.findOne({owner: owner, subscribedTo: subscribedTo});
}

async function getFavorites(owner) {
    return SubscriberModel.find({owner});
}

async function getSubscriptionsByUser(currentUser, user) {
    const followersModels = await SubscriberModel.find({subscribedTo: user});
    const followingModels = await SubscriberModel.find({owner: user});

    const followersDTO = await Promise.all(followersModels.map(async u => await u.toFollowersDTO()));
    const followingDTO = await Promise.all(followingModels.map(async u => await u.toFollowingDTO()));

    return {followers: followersDTO, following: followingDTO};
}

module.exports = {
    subscribeToUser,
    getSubscription,
    getFavorites,
    getSubscriptionsByUser,
    ERROR_INVALID_USER
};
