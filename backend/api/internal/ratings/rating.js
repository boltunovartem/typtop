const mongoose = require("mongoose");
const ObjectId = mongoose.Schema.Types.ObjectId;

const Rating = mongoose.Schema({
    article: {type: ObjectId, ref: "Article", required: true},
    owner: {type: ObjectId, ref: "User", required: true},
    positive: {type: Boolean, required: true}
});

Rating.methods.toDTO = function() {
    return {
        id: this._id,
        article: this.article,
        owner: this.owner,
        positive: this.positive
    };
};

module.exports = mongoose.model("Rating", Rating);
