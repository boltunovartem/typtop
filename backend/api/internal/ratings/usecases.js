const RatingModel = require("./rating");

const changeRating = async (article, owner, isPositive) => {
    const currentUserRate = await RatingModel.findOne({article, owner});
    if (currentUserRate) {
        if (currentUserRate.positive === isPositive) {
            await RatingModel.deleteOne({_id: currentUserRate._id});
        } else {
            await RatingModel.updateOne({_id: currentUserRate._id}, {positive: isPositive});
        }
    } else {
        await RatingModel.create({article, owner, positive: isPositive});
    }
    const countPositive = await RatingModel.countDocuments({article, positive: true});
    const countNegative = await RatingModel.countDocuments({article, positive: false});
    return countPositive - countNegative;
};

const deleteAllArticleRates = async articleId => {
    await RatingModel.deleteMany({article: articleId});
};

const getRateByUserAndArticle = (owner, article) => {
    return RatingModel.findOne({owner, article});
};

module.exports = {
    changeRating,
    getRateByUserAndArticle,
    deleteAllArticleRates
};
