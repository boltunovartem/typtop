// +7 (xxx) xxx-xx-xx
const regExpPhone = /^((\+7)+( )+(\()+([0-9]){3}(\))+( )+([0-9]){3}(-)+([0-9]){2}(-)+([0-9]){2})$/;

const isCorrectPhone = phoneNumber => {
    return regExpPhone.test(phoneNumber);
};

module.exports = {
    isCorrectPhone
};
