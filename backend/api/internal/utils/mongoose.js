function isValidationError(e) {
    return e.name === "ValidationError";
}

function isFieldDuplicationError(fieldName, e) {
    if (!isValidationError(e)) {
        return false;
    }
    const error = e.errors[fieldName];
    if (!error) {
        return false;
    }
    return error.kind === "unique";
}

module.exports = {isFieldDuplicationError};
