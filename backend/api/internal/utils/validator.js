const validator = require("validator");

function validateStruct(body, struct) {
    body = body || {};
    const errors = {};
    if (!struct) {
        errors.internal = "Invalid body";
        return {errors, hasErrors: true};
    }
    for (let field in struct) {
        if (struct[field]) {
            const error = validateField(body[field], struct[field]);
            if (error) errors[field] = error;
        }
    }
    return {errors, hasErrors: Object.keys(errors).length !== 0};
}

function validateField(input, rules) {
    if (!input && rules.omitempty) return;
    for (let key in rules) {
        const rule = rules[key];
        if (rule) {
            switch (key) {
                case "omitempty":
                    break;
                case "required":
                    if (input === null || input === undefined || input === "") return rule.message;
                    break;
                case "min":
                    if ((input + "").length < rule.value) return rule.message;
                    break;
                case "max":
                    if ((input + "").length > rule.value) return rule.message;
                    break;
                case "email":
                    if (!validator.isEmail(input) && !!input) return rule.message;
                    break;
                case "mongoId":
                    if (!validator.isMongoId(input)) return rule.message;
                    break;
                case "isArray":
                    if (!Array.isArray(input)) return rule.message;
                    break;
                case "arrayLengthMax":
                    if (input.length > rule.max) return rule.message;
                    break;
                default:
                    console.log("Invalid validation rule");
            }
        }
    }
}

module.exports = {
    validateStruct
};
