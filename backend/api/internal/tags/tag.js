const mongoose = require("mongoose");

const Tag = mongoose.Schema({
    name: {type: String, unique: true, required: true}
});

Tag.methods.toDTO = function() {
    return {
        id: this._id,
        name: this.name
    };
};

module.exports = mongoose.model("Tag", Tag);
