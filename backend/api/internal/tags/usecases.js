const TagModel = require("./tag");

const checkTags = async tags => {
    const result = [];

    for (const tag of tags) {
        const query = {name: tag};
        const options = {upsert: true, new: true};
        const tagFromModel = await TagModel.findOneAndUpdate(query, {}, options);

        result.push(tagFromModel);
    }

    return result;
};

const getTags = async search => {
    return TagModel.find({
        name: {$regex: search}
    });
};

module.exports = {
    checkTags,
    getTags
};
