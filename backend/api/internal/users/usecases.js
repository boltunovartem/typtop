const jwt = require("jsonwebtoken");

const UserModel = require("./user");
const userStatuses = require("./consts/userStatuses");
const strings = require("../utils/strings");
const uuid = require("uuid");
const {isFieldDuplicationError} = require("../utils/mongoose");
const {isCorrectPhone} = require("../utils/phoneNumber");

/* TODO: Use after adding ability for save user picture
 * const PicStorage = require("../utils/picStorage");
 * const pictureOptions = require("./consts/userPictureOptions");
 */

const ERROR_DUPLICATE_USER = new Error("ERROR_DUPLICATE_USER");
const ERROR_DUPLICATE_EMAIL = new Error("ERROR_DUPLICATE_EMAIL");
const ERROR_NOT_SUPPORTED_PHONE_NUMBER = new Error("ERROR_NOT_SUPPORTED_PHONE_NUMBER");
const ERROR_INVALID_CREDENTIALS = new Error("ERROR_INVALID_CREDENTIALS");
const ERROR_USER_NOT_FOUND = new Error("ERROR_USER_NOT_FOUND");

async function createUser(data) {
    try {
        const {phoneNumber, password, userName, ...rest} = data;

        if (!isCorrectPhone(phoneNumber)) {
            throw ERROR_NOT_SUPPORTED_PHONE_NUMBER;
        }

        const userData = {
            userName: userName.toLowerCase(),
            phoneNumber,
            password,
            ...rest
        };

        let user = new UserModel(userData);
        user.setupDefaults();
        user.setPassword(password);
        await user.save();

        return {user: user, token: generateUserToken(user)};
    } catch (e) {
        if (isFieldDuplicationError("userName", e)) {
            throw ERROR_DUPLICATE_USER;
        }
        if (isFieldDuplicationError("email", e)) {
            throw ERROR_DUPLICATE_EMAIL;
        }
        throw e;
    }
}

async function getUserByToken(token) {
    if (!token) {
        return null;
    }

    try {
        const {id, salt} = jwt.verify(token, config.jwtSecret);
        return await UserModel.findOne({_id: id, authTokenSalt: salt});
    } catch (e) {
        return null;
    }
}

async function login({userName, password}) {
    const user = await UserModel.findOne({userName});
    if (!user || user.status === userStatuses.deleted) {
        throw ERROR_INVALID_CREDENTIALS;
    }

    const isValidPassword = user.isValidPassword(password);
    if (!isValidPassword) {
        throw ERROR_INVALID_CREDENTIALS;
    }

    return {user: user, token: generateUserToken(user)};
}

function generateUserToken(user) {
    return jwt.sign({id: user.id, salt: user.authTokenSalt}, config.jwtSecret);
}

async function getUserById({id}) {
    const user = await UserModel.findOne({_id: id, status: userStatuses.initial});

    if (!user) throw ERROR_USER_NOT_FOUND;

    return user;
}

async function updateUser(data, currentUser) {
    try {
        const {phoneNumber, password, ...userData} = data;

        if (!isCorrectPhone(phoneNumber)) {
            throw ERROR_NOT_SUPPORTED_PHONE_NUMBER;
        }

        userData.phoneNumber = phoneNumber;

        let user = await getUserById(currentUser);

        const editedFields = getEditedFields(user, userData);

        for (const field of editedFields) {
            switch (field) {
                case "firstName":
                    user.firstName = strings.firstLetterUppercase(userData.firstName);
                    break;
                case "lastName":
                    user.lastName = strings.firstLetterUppercase(userData.lastName);
                    break;
                case "email":
                    const isUniqEmail = await checkUniq(field, userData[field]);
                    if (!isUniqEmail) throw ERROR_DUPLICATE_EMAIL;
                    user.email = userData.email;
                    break;
                default:
                    user[field] = userData[field];
                    break;
            }
        }

        if (password) {
            user.setPassword(password);
        }

        await user.save();

        return {user: user};
    } catch (e) {
        if (isFieldDuplicationError("email", e)) {
            throw ERROR_DUPLICATE_EMAIL;
        }
        throw e;
    }
}

async function deleteUser(currentUser) {
    let user = await getUserById(currentUser);
    user.userName = user.userName + "-" + uuid.v4();
    user.email = user.email + "-" + uuid.v4();
    user.status = userStatuses.deleted;
    await user.save();
}

function getEditedFields(user, data) {
    const fields = Object.keys(data);
    const result = [];

    for (const field of fields) {
        if (user[field] !== data[field]) {
            result.push(field);
        }
    }

    return result;
}

async function checkUniq(field, value) {
    return !UserModel.findOne({[field]: value});
}

module.exports = {
    createUser,
    login,
    getUserByToken,
    getUserById,
    updateUser,
    deleteUser,
    ERROR_DUPLICATE_USER,
    ERROR_DUPLICATE_EMAIL,
    ERROR_NOT_SUPPORTED_PHONE_NUMBER,
    ERROR_INVALID_CREDENTIALS,
    ERROR_USER_NOT_FOUND
};
