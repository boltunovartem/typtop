const crypto = require("crypto");
const uuidv4 = require("uuid").v4;

const mongoose = require("mongoose");

const uniqueValidator = require("mongoose-unique-validator");
const strings = require("../utils/strings");
const userStatuses = require("./consts/userStatuses");

const User = mongoose.Schema({
    firstName: {type: String},
    lastName: {type: String},
    middleName: {type: String, default: ""},
    gender: {type: String, default: "other"},
    dateOfBirth: {type: Date},
    workPlace: {type: String, default: ""},
    phoneNumber: {type: String, index: true, sparse: true},
    email: {type: String, index: true, unique: true, sparse: true},
    userName: {type: String, index: true, unique: true},
    password: {type: String},
    authTokenSalt: {type: String},
    createdAt: {type: Date, default: Date.now},
    status: {type: String, default: userStatuses.initial}
});

User.methods.setupDefaults = function() {
    this.firstName = strings.firstLetterUppercase(this.firstName);
    this.lastName = strings.firstLetterUppercase(this.lastName);
    this.authTokenSalt = uuidv4();
};

User.methods.setPassword = function(password) {
    this.password = this.encryptPassword(password);
};

User.methods.isValidPassword = function(password) {
    return this.password === this.encryptPassword(password);
};

User.methods.encryptPassword = function(password) {
    const hmac = crypto.createHmac("sha256", this.createdAt.toISOString());
    hmac.update(password);
    return hmac.digest("hex");
};

User.methods.toDTO = function() {
    return {
        id: this._id,
        firstName: this.firstName,
        lastName: this.lastName,
        middleName: this.middleName,
        gender: this.gender,
        dateOfBirth: this.dateOfBirth,
        phoneNumber: this.phoneNumber,
        workPlace: this.workPlace,
        email: this.email,
        userName: this.userName
    };
};

User.methods.toNameDTO = function() {
    return {
        id: this._id,
        firstName: this.firstName,
        lastName: this.lastName
    };
};
User.plugin(uniqueValidator);
module.exports = mongoose.model("User", User);
