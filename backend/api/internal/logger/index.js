const winston = require("winston");
const expressWinston = require("express-winston");

const transportConsole = new winston.transports.Console();

const defaultLogger = winston.createLogger({
    transports: [new winston.transports.Console()],
    level: "info",
    format: winston.format.json()
});

const expressRequestLogger = expressWinston.logger({
    transports: [transportConsole],
    format: winston.format.json(),
    msg: "HTTP {{req.url}} {{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.ip}}",
    expressFormat: false,
    colorize: false
});

const expressErrorLogger = expressWinston.errorLogger({
    transports: [transportConsole],
    format: winston.format.json()
});

module.exports = {
    defaultLogger,
    expressRequestLogger,
    expressErrorLogger
};
