[![Coverage report](https://gitlab.com/boltunov.artem/typtop/badges/testing/coverage.svg)](https://boltunovartem.gitlab.io/typtop/lcov-report/)

# TypTop - Typical Topics. Site for reading and publishing articles.

## Website - <a href="https://typtop.ru">https:\/\/typtop.ru</a>

## Structure

- **/backend/api** - Node.js API

  - **index.js** - base file for init project
  - **engine/index.js** - server engine for connect to DB and start listener API server
  - **config/index.js** - all data about connections, allowed addresses, env and other
  - **routes/** - init default, static route and init endpoint for API
  - **api/** - handlers API endpoints, validations, middlewares and using usecases for work with API
  - **internal/** - all usecases, Database models, utils and consts for work with API

- **/frontend** - React.js Application
  - **/scripts/** and /config/ - directories with scripts for Webpack compiling project and build web-application.
  - **/public/** - directory, where you can see index.html - base file of site, and site icon.
  - **src/** - directory of source code
    - **index.js** - base file React.js. It's needed for linking out application with root DOM element in index.html
    - **controllers/** - realizations all controllers used in project. For example NetworkController - object for work with network.
    - **containers/** - realizations containers (smart components). Containers can to work with data, network, memory and other.
    - **components/** - realizations simple components. Components needed for rendering HTML, using css with different conditions and filling site by data/
    - **contexts/** - all contexts for app. Context needed for work with data. All Container and Components can use context for get, change and remove data. But the best of practics it's changing data only from containers. Simple components shoudn't change and remove context data.
  - **/proxy** - used for controlling traffic and routing it into API or into frontend.
